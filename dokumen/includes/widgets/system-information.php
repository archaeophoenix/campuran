<?php
	if ( isset( $_GET['ajax_call'] ) ) {
		require_once('../../sys.includes.php');
	}
?>
<div class="widget widget_system_info">
	<h4><?php _e('Informasi Sistem','cftp_admin'); ?></h4>
	<div class="widget_int">
		<h3><?php _e('Software','cftp_admin'); ?></h3>
		<dl class="dl-horizontal">
			<dt><?php _e('Versi','cftp_admin'); ?></dt>
			<dd>
				<?php echo CURRENT_VERSION; ?> <?php
					if (defined('VERSION_NEW_NUMBER')) {
						echo ' - <strong>'; _e('New version available','cftp_admin'); echo ':</strong> <a href="'. VERSION_NEW_URL . '">' . VERSION_NEW_NUMBER . '</a>';
					}
				?>
			</dd>

			<dt><?php _e('Ukuran Maksimum Unggah','cftp_admin'); ?></dt>
			<dd><?php echo MAX_FILESIZE; ?> mb.</dd>

			<dt><?php _e('Template','cftp_admin'); ?></dt>
			<dd><?php echo ucfirst(TEMPLATE_USE); ?> <a href="<?php echo BASE_URI; ?>templates.php">[<?php _e('Ubah','cftp_admin'); ?>]</a></dd>

			<?php
				/** Get the data to show on the bars graphic */
				$statement = $dbh->query("SELECT distinct id FROM " . TABLE_FILES );
				$total_files = $statement->rowCount();
			
				$statement = $dbh->query("SELECT distinct id FROM " . TABLE_USERS . " WHERE level = '0'");
				$total_clients = $statement->rowCount();
			
				$statement = $dbh->query("SELECT distinct id FROM " . TABLE_GROUPS);
				$total_groups = $statement->rowCount();
			
				$statement = $dbh->query("SELECT distinct id FROM " . TABLE_USERS . " WHERE level != '0'");
				$total_users = $statement->rowCount();

				$statement = $dbh->query("SELECT distinct id FROM " . TABLE_CATEGORIES);
				$total_categories = $statement->rowCount();
			?>
		</dl>

		<h3><?php _e('Data','cftp_admin'); ?></h3>
		<dl class="dl-horizontal">
			<dt><?php _e('Dokumen','cftp_admin'); ?></dt>
			<dd><?php echo $total_files; ?></dd>

			<dt><?php _e('Klien','cftp_admin'); ?></dt>
			<dd><?php echo $total_clients; ?></dd>

			<dt><?php _e('Sistem Pengguna','cftp_admin'); ?></dt>
			<dd><?php echo $total_users; ?></dd>

			<dt><?php _e('Grup','cftp_admin'); ?></dt>
			<dd><?php echo $total_groups; ?></dd>

			<dt><?php _e('Kategori','cftp_admin'); ?></dt>
			<dd><?php echo $total_categories; ?></dd>

			<?php
				/**
				 * Hidden so it doesn't get shared by accident in any bug report
				<dt><?php _e('Direktori Utama','cftp_admin'); ?></dt>
				<dd><?php echo ROOT_DIR; ?></dd>

				<dt><?php _e('Folder Unggahan','cftp_admin'); ?></dt>
				<dd><?php echo UPLOADED_FILES_FOLDER; ?></dd>
				*/
			?>
		</dl>
		
		<h3><?php _e('Sistem','cftp_admin'); ?></h3>
		<dl class="dl-horizontal">
			<dt><?php _e('Server','cftp_admin'); ?></dt>
			<dd><?php echo $_SERVER["SERVER_SOFTWARE"]; ?>

			<dt><?php _e('Versi PHP','cftp_admin'); ?></dt>
			<dd><?php echo PHP_VERSION; ?></dd>

			<dt><?php _e('Batas Memori','cftp_admin'); ?></dt>
			<dd><?php echo ini_get('memory_limit'); ?></dd>

			<dt><?php _e('Waktu Eksekusi Maksimum','cftp_admin'); ?></dt>
			<dd><?php echo ini_get('max_execution_time'); ?></dd>

			<dt><?php _e('Ukuran Maksimum Posting','cftp_admin'); ?></dt>
			<dd><?php echo ini_get('post_max_size'); ?></dd>
		</dl>
		
		<h3><?php _e('Database','cftp_admin'); ?></h3>
		<dl class="dl-horizontal">
			<dt><?php _e('Driver','cftp_admin'); ?></dt>
			<dd><?php echo $dbh->getAttribute(PDO::ATTR_DRIVER_NAME); ?></dd>

			<dt><?php _e('Versi','cftp_admin'); ?></dt>
			<dd><?php echo $dbh->query('select version()')->fetchColumn(); ?></dd>
		</dl>
	</div>
</div>