<?php
/**
 * Options page and form.
 *
 * @package ProjectSend
 * @subpackage Options
 */
$load_scripts	= array(
						'jquery_tags_input',
						'spinedit',
					);

$allowed_levels = array(9);
require_once('sys.includes.php');

$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : $_POST['section'];

switch ( $section ) {
	case 'general':
		$section_title	= __('Opsi Umum','cftp_admin');
		$checkboxes		= array(
								'footer_custom_enable',
								'files_descriptions_use_ckeditor',
								'use_browser_lang',
							);
		break;
	case 'clients':
		$section_title	= __('Klien','cftp_admin');
		$checkboxes		= array(
								'clients_can_register',
								'clients_auto_approve',
								'clients_can_upload',
								'clients_can_delete_own_files',
								'clients_can_set_expiration_date',
							);
		break;
	case 'privacy':
		$section_title	= __('Privasi','cftp_admin');
		$checkboxes		= array(
								'privacy_noindex_site',
								'enable_landing_for_all_files',
								'public_listing_page_enable',
								'public_listing_logged_only',
								'public_listing_show_all_files',
								'public_listing_use_download_link',
							);
		break;
	case 'email':
		$section_title	= __('Notifikasi E-mail','cftp_admin');
		$checkboxes		= array(
								'mail_copy_user_upload',
								'mail_copy_client_upload',
								'mail_copy_main_user',
							);
		break;
	case 'security':
		$section_title	= __('Keamanan','cftp_admin');
		$checkboxes		= array(
								'pass_require_upper',
								'pass_require_lower',
								'pass_require_number',
								'pass_require_special',
								'recaptcha_enabled',
							);
		break;
	case 'thumbnails':
		$section_title	= __('Thumbnails','cftp_admin');
		$checkboxes		= array(
								'thumbnails_use_absolute',
							);
		break;
	case 'branding':
		$section_title	= __('Branding','cftp_admin');
		$checkboxes		= array(
							);
		break;
	case 'social_login':
		$section_title	= __('Login Sosial','cftp_admin');
		$checkboxes		= array(
							);
		break;
	default:
		$location = BASE_URI . 'options.php?section=general';
		header("Location: $location");
		die();
		break;
}

//$page_title = __('Opsi Sistem','cftp_admin');
$page_title = $section_title;

$active_nav = 'options';
include('header.php');

$logo_file_info = generate_logo_url();

/** Form sent */
if ($_POST) {
	/**
	 * Escape all the posted values on a single function.
	 * Defined on functions.php
	 */
	/** Values that can be empty */
	$allowed_empty_values	= array(
                                'footer_custom_content',
								'mail_copy_addresses',
								'mail_smtp_host',
								'mail_smtp_port',
								'mail_smtp_user',
								'mail_smtp_pass',
							);

	if ( empty( $_POST['google_signin_enabled'] ) ) {
		$allowed_empty_values[] = 'google_client_id';
		$allowed_empty_values[] = 'google_client_secret';
	}
	if ( empty( $_POST['recaptcha_enabled'] ) ) {
		$allowed_empty_values[] = 'recaptcha_site_key';
		$allowed_empty_values[] = 'recaptcha_secret_key';
	}

	foreach ($checkboxes as $checkbox) {
		$_POST[$checkbox] = (empty($_POST[$checkbox]) || !isset($_POST[$checkbox])) ? 0 : 1;
	}

	$keys = array_keys($_POST);

	$options_total = count($keys);
	$options_filled = 0;
	$query_state = '0';

	/**
	 * Check if all the options are filled.
	 */
	for ($i = 0; $i < $options_total; $i++) {
		if (!in_array($keys[$i], $allowed_empty_values)) {
			if (empty($_POST[$keys[$i]]) && $_POST[$keys[$i]] != '0') {
				$query_state = '3';
			}
			else {
				$options_filled++;
			}
		}
	}

	/** If every option is completed, continue */
	if ($query_state == '0') {
		$updated = 0;
		for ($j = 0; $j < $options_total; $j++) {
			$save = $dbh->prepare( "UPDATE " . TABLE_OPTIONS . " SET value=:value WHERE name=:name" );
			$save->bindParam(':value', $_POST[$keys[$j]]);
			$save->bindParam(':name', $keys[$j]);
			$save->execute();

			if ($save) {
				$updated++;
			}
		}
		if ($updated > 0){
			$query_state = '1';
		}
		else {
			$query_state = '2';
		}
	}

	/** If uploading a logo on the branding page */
	$file_logo = $_FILES['select_logo'];
	if ( !empty( $file_logo ) ) {
		$logo = option_file_upload( $file_logo, 'image', 'logo_filename', 29 );
		$file_status = $logo['status'];
	}

	/** Redirect so the options are reflected immediatly */
	while (ob_get_level()) ob_end_clean();
	$section_redirect = html_output($_POST['section']);
	$location = BASE_URI . 'options.php?section=' . $section_redirect;

	if ( !empty( $query_state ) ) {
		$location .= '&status=' . $query_state;
	}

	if ( !empty( $file_status ) ) {
		$location .= '&file_status=' . $file_status;
	}
	header("Location: $location");
	die();
}

/**
 * Replace | with , to use the tags system when showing
 * the allowed filetypes on the form. This value comes from
 * site.options.php
*/
$allowed_file_types = str_replace('|',',',$allowed_file_types);
/** Explode, sort, and implode the values to list them alphabetically */
$allowed_file_types = explode(',',$allowed_file_types);
sort($allowed_file_types);

/** If .php files are allowed, set the flag for the warning message */
if ( in_array( 'php', $allowed_file_types ) ) {
	$php_allowed_warning = true;
}

$allowed_file_types = implode(',',$allowed_file_types);

?>

<div class="col-xs-12 col-sm-12 col-lg-6">
	<?php
		if (isset($_GET['status'])) {
			switch ($_GET['status']) {
				case '1':
					$msg = __('Opsi berhasil diperbarui.','cftp_admin');
					echo system_message('ok',$msg);
					break;
				case '2':
					$msg = __('Ada kesalahan. Silakan coba lagi.','cftp_admin');
					echo system_message('error',$msg);
					break;
				case '3':
					$msg = __('Beberapa bidang tidak selesai. Opsi tidak dapat disimpan.','cftp_admin');
					echo system_message('error',$msg);
					$show_options_form = 1;
					break;
			}
		}

		/** Logo uploading status */
		if (isset($_GET['file_status'])) {
			switch ($_GET['file_status']) {
				case '1':
					break;
				case '2':
					$msg = __('Dokumen tidak dapat dipindahkan ke folder yang sesuai.','cftp_admin');
					$msg .= __("Ini kemungkinan besar merupakan masalah izin. Jika demikian, maka dapat diperbaiki melalui FTP dengan mengatur nilai chmod dari",'cftp_admin');
					$msg .= ' '.LOGO_FOLDER.' ';
					$msg .= __('direktori ke 755, atau 777 sebagai sumber terakhir.','cftp_admin');
					$msg .= __("Jika ini tidak menyelesaikan masalah, coba berikan nilai yang sama ke direktori di atas yang itu sampai berhasil.",'cftp_admin');
					echo system_message('error',$msg);
					break;
				case '3':
					$msg = __('Dokumen yang Anda pilih bukan format yang diizinkan.','cftp_admin');
					echo system_message('error',$msg);
					break;
				case '4':
					$msg = __('Terjadi kesalahan saat mengunggah dokumen. Silakan coba lagi.','cftp_admin');
					echo system_message('error',$msg);
					break;
			}
		}
	?>

	<div class="white-box">
		<div class="white-box-interior">

			<script type="text/javascript">
				$(document).ready(function() {
					$('#notifications_max_tries').spinedit({
						minimum: 1,
						maximum: 100,
						step: 1,
						value: <?php echo NOTIFICATIONS_MAX_TRIES; ?>,
						numberOfDecimals: 0
					});

					$('#notifications_max_days').spinedit({
						minimum: 0,
						maximum: 365,
						step: 1,
						value: <?php echo NOTIFICATIONS_MAX_DAYS; ?>,
						numberOfDecimals: 0
					});

					$('#allowed_file_types').tagsInput({
						'width'			: '95%',
						'height'		: 'auto',
						'defaultText'	: '',
					});

					$("form").submit(function() {
						clean_form(this);

						is_complete_all_options(this,'<?php _e('Silakan lengkapi semua bidang.','cftp_admin'); ?>');

						// show the errors or continue if everything is ok
						if (show_form_errors() == false) { alert('<?php _e('Silakan lengkapi semua bidang.','cftp_admin'); ?>'); return false; }
					});
				});
			</script>

			<form action="options.php" name="optionsform" method="post" enctype="multipart/form-data" class="form-horizontal">
				<input type="hidden" name="section" value="<?php echo $section; ?>">

					<?php
						switch ( $section ) {
							case 'general':
					?>
								<h3><?php _e('Umum','cftp_admin'); ?></h3>
								<p><?php _e('Informasi dasar untuk ditampilkan di sekitar situs. Format waktu dan nilai zona memengaruhi cara klien melihat tanggal pada daftar dokumen mereka.','cftp_admin'); ?></p>

								<div class="form-group">
									<label for="this_install_title" class="col-sm-4 control-label"><?php _e('Nama Situs','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="this_install_title" id="this_install_title" class="form-control" value="<?php echo html_output(THIS_INSTALL_SET_TITLE); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="timezone" class="col-sm-4 control-label"><?php _e('Zona Waktu','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<?php
											/**
											 * Generates a select field.
											 * Code is stored on a separate file since it's pretty long.
											 */
											include_once('includes/timezones.php');
										?>
									</div>
								</div>

								<div class="form-group">
									<label for="timeformat" class="col-sm-4 control-label"><?php _e('Format Waktu','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="timeformat" id="timeformat" value="<?php echo TIMEFORMAT_USE; ?>" />
										<p class="field_note"><?php _e('Sebagai contoh, d/m/Y h:i:s akan menghasilkan sesuatu seperti','cftp_admin'); ?> <strong><?php echo date('d/m/Y h:i:s'); ?></strong>.
										<?php _e('Untuk daftar lengkap nilai yang tersedia, kunjungi','cftp_admin'); ?> <a href="http://php.net/manual/en/function.date.php" target="_blank"><?php _e('halaman ini','cftp_admin'); ?></a>.</p>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="footer_custom_enable">
											<input type="checkbox" value="1" name="footer_custom_enable" id="footer_custom_enable" class="checkbox_options" <?php echo (FOOTER_CUSTOM_ENABLE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Gunakan teks footer khusus",'cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="footer_custom_content" class="col-sm-4 control-label"><?php _e('Isi Footer','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="footer_custom_content" id="footer_custom_content" class="form-control empty" value="<?php echo html_output(FOOTER_CUSTOM_CONTENT); ?>" />
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Editor','cftp_admin'); ?></h3>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="files_descriptions_use_ckeditor">
											<input type="checkbox" value="1" name="files_descriptions_use_ckeditor" id="files_descriptions_use_ckeditor" class="checkbox_options" <?php echo (DESCRIPTIONS_USE_CKEDITOR == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Gunakan editor visual pada deskripsi dokumen",'cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Bahasa','cftp_admin'); ?></h3>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="use_browser_lang">
											<input type="checkbox" value="1" name="use_browser_lang" id="use_browser_lang" class="checkbox_options" <?php echo (USE_BROWSER_LANG == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Deteksi bahasa browser pengguna",'cftp_admin'); ?>
											<p class="field_note"><?php _e("Jika tersedia, akan menggantikan yang default dari dokumen konfigurasi sistem. Mempengaruhi semua pengguna dan klien.",'cftp_admin'); ?></p>
										</label>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Lokasi Sistem','cftp_admin'); ?></h3>
								<p class="text-warning"><?php _e('Opsi-opsi ini harus diubah hanya jika Anda memindahkan sistem ke tempat lain. Perubahan di sini dapat menyebabkan ProjectSend berhenti bekerja.','cftp_admin'); ?></p>

								<div class="form-group">
									<label for="base_uri" class="col-sm-4 control-label"><?php _e('Sistem URI','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" class="form-control" name="base_uri" id="base_uri" value="<?php echo BASE_URI; ?>" />
									</div>
								</div>
					<?php
							break;
							case 'clients':
					?>
								<h3><?php _e('Registrasi Baru','cftp_admin'); ?></h3>
								<p><?php _e('Hanya digunakan pada pendaftaran mandiri. Opsi ini tidak akan berlaku untuk klien yang terdaftar oleh administrator sistem.','cftp_admin'); ?></p>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="clients_can_register">
											<input type="checkbox" value="1" name="clients_can_register" id="clients_can_register" class="checkbox_options" <?php echo (CLIENTS_CAN_REGISTER == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Klien Daftar Sendiri','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="clients_auto_approve">
											<input type="checkbox" value="1" name="clients_auto_approve" id="clients_auto_approve" class="checkbox_options" <?php echo (CLIENTS_AUTO_APPROVE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Otomatis menyetujui akun baru','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="clients_auto_group" class="col-sm-4 control-label"><?php _e('Tambahkan klien ke grup ini:','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<select class="form-control" name="clients_auto_group" id="clients_auto_group">
											<option value="0"><?php _e('Tidak ada (tidak mengaktifkan fitur ini)','cftp_admin'); ?></option>
											<?php
												/** Fill the groups array that will be used on the form */
												$get_groups		= new GroupActions;
												$arguments		= array();
												$groups 		= $get_groups->get_groups($arguments);

												foreach ( $groups as $group ) {
											?>
														<option value="<?php echo filter_var($group["id"],FILTER_VALIDATE_INT); ?>"
															<?php
																if (CLIENTS_AUTO_GROUP == $group["id"]) {
																	echo 'selected="selected"';
																}
															?>
															><?php echo html_output($group["name"]); ?>
														</option>
													<?php
												}
											?>
										</select>
										<p class="field_note"><?php _e('Klien baru akan secara otomatis ditugaskan ke grup yang telah Anda pilih.','cftp_admin'); ?></p>
									</div>
								</div>

								<div class="form-group">
									<label for="clients_can_select_group" class="col-sm-4 control-label"><?php _e('Grup tempat klien dapat meminta keanggotaan untuk:','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<select class="form-control" name="clients_can_select_group" id="clients_can_select_group">
											<?php
												$pub_groups_options = array(
																			'none'		=> __("Tidak Ada",'cftp_admin'),
																			'public'	=> __("Grup Publik",'cftp_admin'),
																			'all'		=> __("Semua Grup",'cftp_admin'),
																		);
												foreach ( $pub_groups_options as $value => $label ) {
											?>
													<option value="<?php echo $value; ?>" <?php if (CLIENTS_CAN_SELECT_GROUP == $value) { echo 'selected="selected"'; } ?>><?php echo $label; ?></option>
											<?php
												}
											?>
										</select>
										<p class="field_note"><?php _e('Ketika klien mendaftarkan akun baru, sebuah opsi akan disajikan untuk meminta menjadi anggota kelompok tertentu.','cftp_admin'); ?></p>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Dokumen','cftp_admin'); ?></h3>
								<?php
									/*<p><?php _e('Opsi terkait dengan dokumen yang diunggah klien sendiri.','cftp_admin'); ?></p>
									*/
								?>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="clients_can_upload">
											<input type="checkbox" value="1" name="clients_can_upload" id="clients_can_upload" class="checkbox_options" <?php echo (CLIENTS_CAN_UPLOAD == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Klien Bisa Unggah Dokumen','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="clients_can_delete_own_files">
											<input type="checkbox" value="1" name="clients_can_delete_own_files" id="clients_can_delete_own_files" class="checkbox_options" <?php echo (CLIENTS_CAN_DELETE_OWN_FILES == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Klien dapat menghapus dokumen yang diunggah sendiri','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="clients_can_set_expiration_date">
											<input type="checkbox" value="1" name="clients_can_set_expiration_date" id="clients_can_set_expiration_date" class="checkbox_options" <?php echo (CLIENTS_CAN_SET_EXPIRATION_DATE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Klien dapat mengatur Tanggal kedaluwarsa','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="expired_files_hide" class="col-sm-4 control-label"><?php _e('Kapan Dokumen kedaluwarsa:','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<select class="form-control" name="expired_files_hide" id="expired_files_hide">
											<option value="1" <?php echo (EXPIRED_FILES_HIDE == '1') ? 'selected="selected"' : ''; ?>><?php _e("Jangan tampilkan di daftar dokumen",'cftp_admin'); ?></option>
											<option value="0" <?php echo (EXPIRED_FILES_HIDE == '0') ? 'selected="selected"' : ''; ?>><?php _e("Tunjukkan saja, tetapi cegah unduhan.",'cftp_admin'); ?></option>
										</select>
										<p class="field_note"><?php _e('Ini hanya memengaruhi klien. Di sisi admin, Anda masih bisa mendapatkan dokumen.','cftp_admin'); ?></p>
									</div>
								</div>
					<?php
							break;
							case 'privacy':
					?>
								<h3><?php _e('Privasi','cftp_admin'); ?></h3>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="privacy_noindex_site">
											<input type="checkbox" value="1" name="privacy_noindex_site" id="privacy_noindex_site" class="checkbox_options" <?php echo (PRIVACY_NOINDEX_SITE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Cegah mesin pencari mengindeks situs ini",'cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="enable_landing_for_all_files">
											<input type="checkbox" value="1" name="enable_landing_for_all_files" id="enable_landing_for_all_files" class="checkbox_options" <?php echo (ENABLE_LANDING_FOR_ALL_FILES == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Aktifkan halaman informasi untuk dokumen pribadi",'cftp_admin'); ?>
											<p class="field_note"><?php _e("Jika diaktifkan, halaman pendaratan informasi dokumen akan tersedia bahkan untuk dokumen yang tidak ditandai sebagai pribadi. Mengunduhnya akan tetap dibatasi.",'cftp_admin'); ?></p>
										</label>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Grup publik dan dokumen daftar halaman','cftp_admin'); ?></h3>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="public_listing_page_enable">
											<input type="checkbox" value="1" name="public_listing_page_enable" id="public_listing_page_enable" class="checkbox_options" <?php echo (PUBLIC_LISTING_ENABLE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Aktifkan Halaman','cftp_admin'); ?>
										</label>
										<p class="field_note"><?php _e('Url untuk halaman daftar adalah','cftp_admin'); ?><br>
										<a href="<?php echo PUBLIC_LANDING_URI; ?>" target="_blank">
											<?php echo PUBLIC_LANDING_URI; ?>
										</a></p>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="public_listing_logged_only">
											<input type="checkbox" value="1" name="public_listing_logged_only" id="public_listing_logged_only" class="checkbox_options" <?php echo (PUBLIC_LISTING_LOGGED_ONLY == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Hanya untuk klien yang masuk','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="public_listing_show_all_files">
											<input type="checkbox" value="1" name="public_listing_show_all_files" id="public_listing_show_all_files" class="checkbox_options" <?php echo (PUBLIC_LISTING_SHOW_ALL_FILES == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Grup dalam memperlihatkan semua dokumen, termasuk yang tidak ditandai sebagai publik.','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="public_listing_use_download_link">
											<input type="checkbox" value="1" name="public_listing_use_download_link" id="public_listing_use_download_link" class="checkbox_options" <?php echo (PUBLIC_LISTING_USE_DOWNLOAD_LINK == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Pada dokumen publik, perlihatkan tautan unduhan.','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="options_divide"></div>

					<?php
							break;
							case 'email':
					?>
								<h3><?php _e('Informasi "Dari"','cftp_admin'); ?></h3>

								<div class="form-group">
									<label for="admin_email_address" class="col-sm-4 control-label"><?php _e('E-mail','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="admin_email_address" id="admin_email_address" class="form-control" value="<?php echo html_output(ADMIN_EMAIL_ADDRESS); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="mail_from_name" class="col-sm-4 control-label"><?php _e('Nama','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="mail_from_name" id="mail_from_name" class="form-control" value="<?php echo html_output(MAIL_FROM_NAME); ?>" />
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Kirim Salinan','cftp_admin'); ?></h3>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="mail_copy_user_upload">
											<input type="checkbox" value="1" name="mail_copy_user_upload" id="mail_copy_user_upload" <?php echo (COPY_MAIL_ON_USER_UPLOADS == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Ketika seorang pengguna sistem mengunggah dokumen','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="mail_copy_client_upload">
											<input type="checkbox" value="1" name="mail_copy_client_upload" id="mail_copy_client_upload" <?php echo (COPY_MAIL_ON_CLIENT_UPLOADS == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Ketika seorang klien mengunggah dokumen','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="options_nested_note">
									<p><?php _e('Tetapkan di sini siapa yang akan menerima salinan email ini. Ini dikirim sebagai BCC sehingga tidak ada penerima yang akan melihat alamat lainnya.','cftp_admin'); ?></p>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="mail_copy_main_user">
											<input type="checkbox" value="1" name="mail_copy_main_user" class="mail_copy_main_user" <?php echo (COPY_MAIL_MAIN_USER == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Alamat disediakan di atas (di "Dari")','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="mail_copy_addresses" class="col-sm-4 control-label"><?php _e('Juga ke alamat ini','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="mail_copy_addresses" id="mail_copy_addresses" class="mail_data empty form-control" value="<?php echo html_output(COPY_MAIL_ADDRESSES); ?>" />
										<p class="field_note"><?php _e('Pisahkan alamat email dengan koma.','cftp_admin'); ?></p>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Kedaluwarsa','cftp_admin'); ?></h3>

								<div class="form-group">
									<label for="notifications_max_tries" class="col-sm-4 control-label"><?php _e('Upaya pengiriman maksimum','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="notifications_max_tries" id="notifications_max_tries" class="form-control" value="<?php echo NOTIFICATIONS_MAX_TRIES; ?>" />
										<p class="field_note"><?php _e('Tentukan berapa kali sistem akan mencoba mengirim setiap notifikasi.','cftp_admin'); ?></p>
									</div>
								</div>

								<div class="form-group">
									<label for="notifications_max_days" class="col-sm-4 control-label"><?php _e('Hari sebelum kedaluwarsa','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="notifications_max_days" id="notifications_max_days" class="form-control" value="<?php echo NOTIFICATIONS_MAX_DAYS; ?>" />
										<p class="field_note"><?php _e('Pemberitahuan yang lebih tua dari ini tidak akan dikirim.','cftp_admin'); ?><br /><strong><?php _e('Set to 0 to disable.','cftp_admin'); ?></strong></p>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Opsi pengiriman email','cftp_admin'); ?></h3>
								<p><?php _e('Di sini Anda dapat memilih sistem surat mana yang akan digunakan saat mengirim pemberitahuan. Jika Anda memiliki akun email yang valid, SMTP adalah opsi yang disarankan.','cftp_admin'); ?></p>

								<div class="form-group">
									<label for="mail_system_use" class="col-sm-4 control-label"><?php _e('Pengirim','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<select class="form-control" name="mail_system_use" id="mail_system_use">
											<option value="mail" <?php echo (MAIL_SYSTEM == 'mail') ? 'selected="selected"' : ''; ?>>PHP Mail (basic)</option>
											<option value="smtp" <?php echo (MAIL_SYSTEM == 'smtp') ? 'selected="selected"' : ''; ?>>SMTP</option>
											<option value="gmail" <?php echo (MAIL_SYSTEM == 'gmail') ? 'selected="selected"' : ''; ?>>Gmail</option>
											<option value="sendmail" <?php echo (MAIL_SYSTEM == 'sendmail') ? 'selected="selected"' : ''; ?>>Sendmail</option>
										</select>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Opsi bersama SMTP & Gmail','cftp_admin'); ?></h3>
								<p><?php _e('Anda harus memasukkan nama pengguna Anda (biasanya alamat email Anda) dan kata sandi jika Anda telah memilih SMTP atau Gmail sebagai pengirim email Anda.','cftp_admin'); ?></p>

								<div class="form-group">
									<label for="mail_smtp_user" class="col-sm-4 control-label"><?php _e('Nama Pengguna','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="mail_smtp_user" id="mail_smtp_user" class="mail_data empty form-control" value="<?php echo html_output(SMTP_USER); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="mail_smtp_pass" class="col-sm-4 control-label"><?php _e('Kata Sandi','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="password" name="mail_smtp_pass" id="mail_smtp_pass" class="mail_data empty form-control" value="<?php echo html_output(SMTP_PASS); ?>" />
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Opsi SMTP ','cftp_admin'); ?></h3>
								<p><?php _e('Jika Anda memilih SMTP sebagai mailer Anda, silakan lengkapi opsi ini.','cftp_admin'); ?></p>

								<div class="form-group">
									<label for="mail_smtp_host" class="col-sm-4 control-label"><?php _e('Host','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="mail_smtp_host" id="mail_smtp_host" class="mail_data empty form-control" value="<?php echo html_output(SMTP_HOST); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="mail_smtp_port" class="col-sm-4 control-label"><?php _e('Port','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="mail_smtp_port" id="mail_smtp_port" class="mail_data empty form-control" value="<?php echo html_output(SMTP_PORT); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="mail_smtp_auth" class="col-sm-4 control-label"><?php _e('Authentication','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<select class="form-control" name="mail_smtp_auth" id="mail_smtp_auth">
											<option value="none" <?php echo (SMTP_AUTH == 'none') ? 'selected="selected"' : ''; ?>><?php _e('Tidak Ada','cftp_admin'); ?></option>
											<option value="ssl" <?php echo (SMTP_AUTH == 'ssl') ? 'selected="selected"' : ''; ?>>SSL</option>
											<option value="tls" <?php echo (SMTP_AUTH == 'tls') ? 'selected="selected"' : ''; ?>>TLS</option>
										</select>
									</div>
								</div>
					<?php
							break;
							case 'security':
					?>
								<h3><?php _e('Ekstensi dokumen yang diizinkan','cftp_admin'); ?></h3>
								<p><?php _e('Hati-hati saat mengubah opsi ini. Mereka dapat mempengaruhi tidak hanya sistem tetapi seluruh server diinstal.','cftp_admin'); ?><br />
								<strong><?php _e('Penting','cftp_admin'); ?></strong>: <?php _e('Pisahkan jenis dokumen yang dibolehkan dengan koma.','cftp_admin'); ?></p>

							   <div class="form-group">
								   <label for="file_types_limit_to" class="col-sm-4 control-label"><?php _e('Batasi jenis dokumen yang diunggah ke','cftp_admin'); ?></label>
								   <div class="col-sm-8">
										<select class="form-control" name="file_types_limit_to" id="file_types_limit_to">
											<option value="noone" <?php echo (FILE_TYPES_LIMIT_TO == 'noone') ? 'selected="selected"' : ''; ?>><?php _e('Tidak Seorang pun','cftp_admin'); ?></option>
											<option value="all" <?php echo (FILE_TYPES_LIMIT_TO == 'all') ? 'selected="selected"' : ''; ?>><?php _e('Semua Orang','cftp_admin'); ?></option>
											<option value="clients" <?php echo (FILE_TYPES_LIMIT_TO == 'clients') ? 'selected="selected"' : ''; ?>><?php _e('Hanya Klien','cftp_admin'); ?></option>
										</select>
								   </div>
								</div>

							   <div class="form-group">
									<input name="allowed_file_types" id="allowed_file_types" value="<?php echo $allowed_file_types; ?>" />
								</div>

								<?php
									if ( isset( $php_allowed_warning ) && $php_allowed_warning == true ) {
										$msg = __('Peringatan: ekstensi php diizinkan. Ini adalah masalah keamanan yang serius. Jika Anda tidak yakin Anda membutuhkannya, harap hapus dari daftar.','cftp_admin');
										echo system_message('error',$msg);
									}
								?>

								<div class="options_divide"></div>

								<h3><?php _e('Kata Sandi','cftp_admin'); ?></h3>
								<p><?php _e('Saat mengatur kata sandi untuk akun, paling tidak memerlukan:','cftp_admin'); ?></p>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="pass_require_upper">
											<input type="checkbox" value="1" name="pass_require_upper" id="pass_require_upper" class="checkbox_options" <?php echo (PASS_REQ_UPPER == 1) ? 'checked="checked"' : ''; ?> /> <?php echo $validation_req_upper; ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="pass_require_lower">
											<input type="checkbox" value="1" name="pass_require_lower" id="pass_require_lower" class="checkbox_options" <?php echo (PASS_REQ_LOWER == 1) ? 'checked="checked"' : ''; ?> /> <?php echo $validation_req_lower; ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="pass_require_number">
											<input type="checkbox" value="1" name="pass_require_number" id="pass_require_number" class="checkbox_options" <?php echo (PASS_REQ_NUMBER == 1) ? 'checked="checked"' : ''; ?> /> <?php echo $validation_req_number; ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="pass_require_special">
											<input type="checkbox" value="1" name="pass_require_special" id="pass_require_special" class="checkbox_options" <?php echo (PASS_REQ_SPECIAL == 1) ? 'checked="checked"' : ''; ?> /> <?php echo $validation_req_special; ?>
										</label>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('reCAPTCHA','cftp_admin'); ?></h3>
								<p><?php _e('Membantu mencegah SPAM pada formulir pendaftaran Anda.','cftp_admin'); ?></p>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="recaptcha_enabled">
											<input type="checkbox" value="1" name="recaptcha_enabled" id="recaptcha_enabled" class="checkbox_options" <?php echo (RECAPTCHA_ENABLED == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Gunakan reCAPTCHA','cftp_admin'); ?>
										</label>
									</div>
								</div>

								<div class="form-group">
									<label for="recaptcha_site_key" class="col-sm-4 control-label"><?php _e('Site key','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="recaptcha_site_key" id="recaptcha_site_key" class="form-control empty" value="<?php echo html_output(RECAPTCHA_SITE_KEY); ?>" />
									</div>
								</div>

								<div class="form-group">
									<label for="recaptcha_secret_key" class="col-sm-4 control-label"><?php _e('Secret key','cftp_admin'); ?></label>
									<div class="col-sm-8">
										<input type="text" name="recaptcha_secret_key" id="recaptcha_secret_key" class="form-control empty" value="<?php echo html_output(RECAPTCHA_SECRET_KEY); ?>" />
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<a href="<?php echo LINK_DOC_RECAPTCHA; ?>" class="external_link" target="_blank"><?php _e('Bagaimana cara saya mendapatkan kredensial ini?','cftp_admin'); ?></a>
									</div>
								</div>
					<?php
							break;
							case 'thumbnails':
					?>
								<h3><?php _e('Thumbnails','cftp_admin'); ?></h3>
								<p><?php _e("Thumbnail digunakan pada daftar dokumen. Disarankan untuk tetap kecil, kecuali Anda menggunakan sistem untuk hanya mengunggah gambar, dan akan mengubah template klien default.",'cftp_admin'); ?></p>

								<div class="options_column">
									<div class="options_col_left">
										<div class="form-group">
											<label for="max_thumbnail_width" class="col-sm-6 control-label"><?php _e('Lebar Maks.','cftp_admin'); ?></label>
											<div class="col-sm-6">
												<input type="text" name="max_thumbnail_width" id="max_thumbnail_width" class="form-control" value="<?php echo html_output(THUMBS_MAX_WIDTH); ?>" />
											</div>
										</div>

										<div class="form-group">
											<label for="max_thumbnail_height" class="col-sm-6 control-label"><?php _e('Tinggi Maks.','cftp_admin'); ?></label>
											<div class="col-sm-6">
												<input type="text" name="max_thumbnail_height" id="max_thumbnail_height" class="form-control" value="<?php echo html_output(THUMBS_MAX_HEIGHT); ?>" />
											</div>
										</div>
									</div>
									<div class="options_col_right">
										<div class="form-group">
											<label for="thumbnail_default_quality" class="col-sm-6 control-label"><?php _e('Kualitas JPG','cftp_admin'); ?></label>
											<div class="col-sm-6">
												<input type="text" name="thumbnail_default_quality" id="thumbnail_default_quality" class="form-control" value="<?php echo html_output(THUMBS_QUALITY); ?>" />
											</div>
										</div>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e("Jejak Dokumen", 'cftp_admin'); ?></h3>
								<p><?php _e("Jika thumbnail tidak ditampilkan (logo perusahaan dan pratinjau dokumen Anda pada halaman branding dan daftar dokumen klien) coba atur opsi ini ON. Jika masih tidak berfungsi, masalah izin folder mungkin menjadi penyebabnya.",'cftp_admin'); ?></p>

								<div class="form-group">
									<div class="col-sm-8 col-sm-offset-4">
										<label for="thumbnails_use_absolute">
											<input type="checkbox" value="1" name="thumbnails_use_absolute" id="thumbnails_use_absolute" <?php echo (THUMBS_USE_ABSOLUTE == 1) ? 'checked="checked"' : ''; ?> /> <?php _e("Gunakan jalur dokumen absolut",'cftp_admin'); ?>
										</label>
									</div>
								</div>
					<?php
							break;
							case 'branding':
					?>
								<h3><?php _e('Logo Sekarang','cftp_admin'); ?></h3>
								<p><?php _e('Gunakan halaman ini untuk mengunggah logo perusahaan Anda, atau memperbarui logo yang saat ini ditetapkan. Gambar ini akan ditampilkan kepada klien Anda ketika mereka mengakses daftar dokumen mereka.','cftp_admin'); ?></p>

								<input type="hidden" name="MAX_FILE_SIZE" value="1000000000">

								<div id="current_logo">
									<div id="current_logo_img">
										<?php
											if ($logo_file_info['exists'] === true) {
										?>
												<img src="<?php echo $logo_file_info['url']; ?>" alt="<?php _e('Logo Placeholder','cftp_admin'); ?>" />
										<?php
											}
										?>
									</div>
									<p class="preview_logo_note">
										<?php _e('Pratinjau ini menggunakan lebar maksimum 300px.','cftp_admin'); ?>
									</p>
								</div>

								<div id="form_upload_logo">
									<div class="form-group">
										<label class="col-sm-4 control-label"><?php _e('Pilih gambar untuk diunggah','cftp_admin'); ?></label>
										<div class="col-sm-8">
											<input type="file" name="select_logo" class="empty" />
										</div>
									</div>
								</div>

								<div class="options_divide"></div>

								<h3><?php _e('Pengaturan Ukuran','cftp_admin'); ?></h3>
								<p><?php _e("Templat penampil dokumen dapat menggunakan pengaturan ini saat menampilkan gambar.",'cftp_admin'); ?></p>

								<div class="form-group">
									<label for="max_logo_width" class="col-sm-4 control-label"><?php _e('Lebar Maks.','cftp_admin'); ?></label>
									<div class="col-sm-3">
										<div class="input-group">
											<input type="text" name="max_logo_width" id="max_logo_width" class="form-control" value="<?php echo html_output(LOGO_MAX_WIDTH); ?>" />
											<span class="input-group-addon">px</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label for="max_logo_height" class="col-sm-4 control-label"><?php _e('Tinggi Maks.','cftp_admin'); ?></label>
									<div class="col-sm-3">
										<div class="input-group">
											<input type="text" name="max_logo_height" id="max_logo_height" class="form-control" value="<?php echo html_output(LOGO_MAX_HEIGHT); ?>" />
											<span class="input-group-addon">px</span>
										</div>
									</div>
								</div>
					<?php
							break;
							case 'social_login':
					?>
								<h3><?php _e('Google','cftp_admin'); ?></h3>

								<div class="options_column">
									<div class="form-group">
										<label for="google_signin_enabled" class="col-sm-4 control-label"><?php _e('Aktifkan','cftp_admin'); ?></label>
										<div class="col-sm-8">
											<select name="google_signin_enabled" id="google_signin_enabled" class="form-control">
												<option value="1" <?php echo (GOOGLE_SIGNIN_ENABLED == '1') ? 'selected="selected"' : ''; ?>><?php _e('Ya','cftp_admin'); ?></option>
												<option value="0" <?php echo (GOOGLE_SIGNIN_ENABLED == '0') ? 'selected="selected"' : ''; ?>><?php _e('Tidak','cftp_admin'); ?></option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="google_client_id" class="col-sm-4 control-label"><?php _e('ID Klien','cftp_admin'); ?></label>
										<div class="col-sm-8">
											<input type="text" name="google_client_id" id="google_client_id" class="form-control empty" value="<?php echo html_output(GOOGLE_CLIENT_ID); ?>" />
										</div>
									</div>
									<div class="form-group">
										<label for="google_client_secret" class="col-sm-4 control-label"><?php _e('Rahasia Klien','cftp_admin'); ?></label>
										<div class="col-sm-8">
											<input type="text" name="google_client_secret" id="google_client_secret" class="form-control empty" value="<?php echo html_output(GOOGLE_CLIENT_SECRET); ?>" />
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-8 col-sm-offset-4">
											<a href="<?php echo LINK_DOC_GOOGLE_SIGN_IN; ?>" class="external_link" target="_blank"><?php _e('Bagaimana cara saya mendapatkan kredensial ini?','cftp_admin'); ?></a>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">
											<?php _e('Callback URI','cftp_admin'); ?>
										</div>
										<div class="col-sm-8">
											<span class="format_url"><?php echo BASE_URI . 'sociallogin/google/callback.php'; ?></span>
										</div>
									</div>
								</div>
					<?php
							break;
						}
					?>

				<div class="options_divide"></div>

				<div class="after_form_buttons">
					<button type="submit" class="btn btn-wide btn-primary empty"><?php _e('Simpan','cftp_admin'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	include('footer.php');
