<?php
/**
 * Allows the administrator to customize the emails
 * sent by the system.
 *
 * @package ProjectSend
 * @subpackage Options
 */
$allowed_levels = array(9);
require_once('sys.includes.php');

$section = ( !empty( $_GET['section'] ) ) ? $_GET['section'] : $_POST['section'];

switch ( $section ) {
	case 'header_footer':
		$section_title	= __('Header / Footer','cftp_admin');
		$checkboxes		= array(
								'email_header_footer_customize',
							);
		break;
	case 'new_files_for_client':
		$section_title	= __('Dokumen Baru Oleh Pengguna','cftp_admin');
		$checkboxes		= array(
								'email_new_file_by_user_customize',
								'email_new_file_by_user_subject_customize',
							);
		break;
	case 'new_file_by_client':
		$section_title	= __('Dokumen Baru Oleh Klien','cftp_admin');
		$checkboxes		= array(
								'email_new_file_by_client_customize',
								'email_new_file_by_client_subject_customize',
							);
		break;
	case 'new_client':
		$section_title	= __('Klien Baru (selamat datang)','cftp_admin');
		$checkboxes		= array(
								'email_new_client_by_user_customize',
								'email_new_client_by_user_subject_customize',
							);
		break;
	case 'new_client_self':
		$section_title	= __('Klien Baru (daftar sendiri)','cftp_admin');
		$checkboxes		= array(
								'email_new_client_by_self_customize',
								'email_new_client_by_self_subject_customize',
							);
		break;
	case 'client_approve':
		$section_title	= __('Akun Klien Disetujui','cftp_admin');
		$checkboxes		= array(
								'email_account_approve_subject_customize',
								'email_account_approve_customize',
							);
		break;
	case 'client_deny':
		$section_title	= __('Akun Klien Ditolak','cftp_admin');
		$checkboxes		= array(
								'email_account_deny_subject_customize',
								'email_account_deny_customize',
							);
		break;
	case 'new_user':
		$section_title	= __('Klien Pengguna (selamat datang)','cftp_admin');
		$checkboxes		= array(
								'email_new_user_customize',
								'email_new_user_subject_customize',
							);
		break;
	case 'password_reset':
		$section_title	= __('Atur Kata Sandi','cftp_admin');
		$checkboxes		= array(
								'email_pass_reset_customize',
								'email_pass_reset_subject_customize',
							);
		break;
	case 'client_edited':
		$section_title	= __('Perbarui Keanggotaan Klien','cftp_admin');
		$checkboxes		= array(
								'email_client_edited_subject_customize',
								'email_client_edited_customize',
							);
		break;
	default:
		$location = BASE_URI . 'email-templates.php?section=header_footer';
		header("Location: $location");
		die();
		break;
}

$page_title = $section_title;

$active_nav = 'emails';
include('header.php');

if ($_POST) {
	foreach ($checkboxes as $checkbox) {
		$_POST[$checkbox] = (empty($_POST[$checkbox]) || !isset($_POST[$checkbox])) ? 0 : 1;
	}

	/**
	 * Escape all the posted values on a single function.
	 * Defined on functions.php
	 */
	$keys = array_keys($_POST);

	$options_total = count($keys);

	$updated = 0;
	for ($j = 0; $j < $options_total; $j++) {
		$save = $dbh->prepare( "UPDATE " . TABLE_OPTIONS . " SET value=:value WHERE name=:name" );
		$save->bindParam(':value', $_POST[$keys[$j]]);
		$save->bindParam(':name', $keys[$j]);
		$save->execute();

		$updated++;
	}
	if ($updated > 0){
		$query_state = '1';
	}
	else {
		$query_state = '2';
	}

	/** Redirect so the options are reflected immediatly */
	while (ob_get_level()) ob_end_clean();
	$section_redirect = html_output($_POST['section']);
	$location = BASE_URI . 'email-templates.php?section=' . $section_redirect;

	if ( !empty( $query_state ) ) {
		$location .= '&status=' . $query_state;
	}

	header("Location: $location");
	die();
}
?>
<div class="col-xs-12 col-sm-12 col-lg-6">
	<?php
		if (isset($_GET['status'])) {
			switch ($_GET['status']) {
				case '1':
					$msg = __('Opsi berhasil diperbarui.','cftp_admin');
					echo system_message('ok',$msg);
					break;
				case '2':
					$msg = __('Ada kesalahan. Silakan coba lagi.','cftp_admin');
					echo system_message('error',$msg);
					break;
			}
		}

	?>

	<div class="white-box">
		<div class="white-box-interior">
			<?php
				$href_string = ' ' . __('(untuk digunakan sebagai href pada tag tautan)','cftp_admin');
		
				$options_groups = array(
										'new_files_for_client'	=> array(
																		'subject_checkbox'	=> 'email_new_file_by_user_subject_customize',
																		'subject'			=> 'email_new_file_by_user_subject',
																		'body_checkbox'		=> 'email_new_file_by_user_customize',
																		'body_textarea'		=> 'email_new_file_by_user_text',
																		'description'		=> __('Email ini akan dikirim ke klien setiap kali file baru telah diberikan ke akunnya.','cftp_admin'),
																		'subject_check'		=> EMAILS_FILE_BY_USER_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_FILE_BY_USER_SUBJECT,
																		'body_check'		=> EMAILS_FILE_BY_USER_USE_CUSTOM,
																		'body_text'			=> EMAILS_FILE_BY_USER_TEXT,
																		'tags'				=> array(
																										'%FILES%'		=> __('Memperlihatkan daftar file','cftp_admin'),
																										'%URI%'			=> __('Tautan masuk','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_NEW_FILE_BY_USER,
																	),
										'new_file_by_client'	=> array(
																		'subject_checkbox'	=> 'email_new_file_by_client_subject_customize',
																		'subject'			=> 'email_new_file_by_client_subject',
																		'body_checkbox'		=> 'email_new_file_by_client_customize',
																		'body_textarea'		=> 'email_new_file_by_client_text',
																		'description'		=> __('Email ini akan dikirim ke administrator sistem setiap kali klien mengunggah file baru.','cftp_admin'),
																		'subject_check'		=> EMAILS_FILE_BY_CLIENT_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_FILE_BY_CLIENT_SUBJECT,
																		'body_check'		=> EMAILS_FILE_BY_CLIENT_USE_CUSTOM,
																		'body_text'			=> EMAILS_FILE_BY_CLIENT_TEXT,
																		'tags'				=> array(
																										'%FILES%'		=> __('Memperlihatkan daftar file','cftp_admin'),
																										'%URI%'			=> __('Tautan masuk','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_NEW_FILE_BY_CLIENT,
																	),
										'new_client'			=> array(
																		'subject_checkbox'	=> 'email_new_client_by_user_subject_customize',
																		'subject'			=> 'email_new_client_by_user_subject',
																		'body_checkbox'		=> 'email_new_client_by_user_customize',
																		'body_textarea'		=> 'email_new_client_by_user_text',
																		'description'		=> __('Email ini akan dikirim ke klien baru setelah administrator membuat akun barunya. Akan lebih baik untuk memasukkan detail login (nama pengguna dan kata sandi).','cftp_admin'),
																		'subject_check'		=> EMAILS_CLIENT_BY_USER_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_CLIENT_BY_USER_SUBJECT,
																		'body_check'		=> EMAILS_CLIENT_BY_USER_USE_CUSTOM,
																		'body_text'			=> EMAILS_CLIENT_BY_USER_TEXT,
																		'tags'				=> array(
																										'%USERNAME%'	=> __('Nama pengguna baru untuk akun ini','cftp_admin'),
																										'%PASSWORD%'	=> __('Kata sandi baru untuk akun ini','cftp_admin'),
																										'%URI%'			=> __('Tautan masuk','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_NEW_CLIENT,
																	),
										'new_client_self'		=> array(
																		'subject_checkbox'	=> 'email_new_client_by_self_subject_customize',
																		'subject'			=> 'email_new_client_by_self_subject',
																		'body_checkbox'		=> 'email_new_client_by_self_customize',
																		'body_textarea'		=> 'email_new_client_by_self_text',
																		'description'		=> __('Email ini akan dikirim ke administrator sistem setelah klien baru telah membuat akun baru untuk dirinya sendiri.','cftp_admin'),
																		'subject_check'		=> EMAILS_CLIENT_BY_SELF_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_CLIENT_BY_SELF_SUBJECT,
																		'body_check'		=> EMAILS_CLIENT_BY_SELF_USE_CUSTOM,
																		'body_text'			=> EMAILS_CLIENT_BY_SELF_TEXT,
																		'tags'				=> array(
																										'%FULLNAME%'		=> __('Nama lengkap klien yang terdaftar','cftp_admin'),
																										'%USERNAME%'		=> __('Nama pengguna baru untuk akun ini','cftp_admin'),
																										'%URI%'				=> __('Tautan masuk','cftp_admin') . $href_string,
																										'%GROUPS_REQUESTS%' => __('Daftar grup tempat klien meminta keanggotaan','cftp_admin'),
																									),
																		'default_text'		=> EMAIL_TEMPLATE_NEW_CLIENT_SELF,
																	),
										'client_approve'		=> array(
																		'subject_checkbox'	=> 'email_account_approve_subject_customize',
																		'subject'			=> 'email_account_approve_subject',
																		'body_checkbox'		=> 'email_account_approve_customize',
																		'body_textarea'		=> 'email_account_approve_text',
																		'description'		=> __('Email ini akan dikirim ke klien yang meminta akun jika disetujui. Permintaan keanggotaan grup juga disebutkan di email, dipisahkan oleh status persetujuannya.','cftp_admin'),
																		'subject_check'		=> EMAILS_ACCOUNT_APPROVE_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_ACCOUNT_APPROVE_SUBJECT,
																		'body_check'		=> EMAILS_ACCOUNT_APPROVE_USE_CUSTOM,
																		'body_text'			=> EMAILS_ACCOUNT_APPROVE_TEXT,
																		'tags'				=> array(
																										'%GROUPS_APPROVED%'	=> __('Daftar keanggotaan grup yang disetujui','cftp_admin'),
																										'%GROUPS_DENIED%'	=> __('Daftar keanggotaan grup yang ditolak','cftp_admin'),
																										'%URI%'				=> __('Tautan masuk','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_ACCOUNT_APPROVE,
																	),
										'client_deny'		=> array(
																		'subject_checkbox'	=> 'email_account_deny_subject_customize',
																		'subject'			=> 'email_account_deny_subject',
																		'body_checkbox'		=> 'email_account_deny_customize',
																		'body_textarea'		=> 'email_account_deny_text',
																		'description'		=> __('Email ini akan dikirim ke klien yang meminta akun jika ditolak. Semua permintaan keanggotaan grup untuk akun ini ditolak secara otomatis.','cftp_admin'),
																		'subject_check'		=> EMAILS_ACCOUNT_DENY_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_ACCOUNT_DENY_SUBJECT,
																		'body_check'		=> EMAILS_ACCOUNT_DENY_USE_CUSTOM,
																		'body_text'			=> EMAILS_ACCOUNT_DENY_TEXT,
																		'tags'				=> array(
																									),
																		'default_text'		=> EMAIL_TEMPLATE_ACCOUNT_DENY,
																	),
										'new_user'				=> array(
																		'subject_checkbox'	=> 'email_new_user_subject_customize',
																		'subject'			=> 'email_new_user_subject',
																		'body_checkbox'		=> 'email_new_user_customize',
																		'body_textarea'		=> 'email_new_user_text',
																		'description'		=> __('Email ini akan dikirim ke pengguna sistem baru setelah administrator membuat akun barunya. Akan lebih baik untuk memasukkan detail login (nama pengguna dan kata sandi).','cftp_admin'),
																		'subject_check'		=> EMAILS_NEW_USER_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_NEW_USER_SUBJECT,
																		'body_check'		=> EMAILS_NEW_USER_USE_CUSTOM,
																		'body_text'			=> EMAILS_NEW_USER_TEXT,
																		'tags'				=> array(
																										'%USERNAME%'	=> __('Nama pengguna baru untuk akun ini','cftp_admin'),
																										'%PASSWORD%'	=> __('Kata sandi baru untuk akun ini','cftp_admin'),
																										'%URI%'			=> __('Tautan masuk','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_NEW_USER,
																	),
										'password_reset'		=> array(
																		'subject_checkbox'	=> 'email_pass_reset_subject_customize',
																		'subject'			=> 'email_pass_reset_subject',
																		'body_checkbox'		=> 'email_pass_reset_customize',
																		'body_textarea'		=> 'email_pass_reset_text',
																		'description'		=> __('Email ini akan dikirim ke pengguna atau klien ketika mereka mencoba mengatur ulang kata sandi mereka.','cftp_admin'),
																		'subject_check'		=> EMAILS_PASS_RESET_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_PASS_RESET_SUBJECT,
																		'body_check'		=> EMAILS_PASS_RESET_USE_CUSTOM,
																		'body_text'			=> EMAILS_PASS_RESET_TEXT,
																		'tags'				=> array(
																										'%USERNAME%'	=> __('Nama pengguna untuk akun ini','cftp_admin'),
																										'%TOKEN%'		=> __('String teks unik untuk permintaan ini. Harus disertakan di suatu tempat.','cftp_admin'),
																										'%URI%'			=> __('Tautan untuk melanjutkan proses','cftp_admin') . $href_string,
																									),
																		'default_text'		=> EMAIL_TEMPLATE_PASSWORD_RESET,
																	),
										'client_edited'		=> array(
																		'subject_checkbox'	=> 'email_client_edited_subject_customize',
																		'subject'			=> 'email_client_edited_subject',
																		'body_checkbox'		=> 'email_client_edited_customize',
																		'body_textarea'		=> 'email_client_edited_text',
																		'description'		=> __('Email ini akan dikirim ke administrator sistem ketika klien mengedit akunnya dan mengubah permintaan keanggotaan grup publik.','cftp_admin'),
																		'subject_check'		=> EMAILS_CLIENT_EDITED_USE_SUBJECT_CUSTOM,
																		'subject_text'		=> EMAILS_CLIENT_EDITED_SUBJECT,
																		'body_check'		=> EMAILS_CLIENT_EDITED_USE_CUSTOM,
																		'body_text'			=> EMAILS_CLIENT_EDITED_TEXT,
																		'tags'				=> array(
																										'%FULLNAME%'		=> __('Nama lengkap klien yang terdaftar','cftp_admin'),
																										'%USERNAME%'		=> __('Nama pengguna baru untuk akun ini','cftp_admin'),
																										'%URI%'				=> __('Tautan masuk ','cftp_admin') . $href_string,
																										'%GROUPS_REQUESTS%' => __('Daftar grup tempat klien meminta keanggotaan','cftp_admin'),
																									),
																		'default_text'		=> EMAIL_TEMPLATE_CLIENT_EDITED,
																	),
									);
			?>


			<form action="email-templates.php" name="templatesform" method="post" enctype="multipart/form-data" class="form-horizontal">
				<input type="hidden" name="section" value="<?php echo $section; ?>">
	
				<?php
					/** Header and footer options */
					if ( $section == 'header_footer' ) {
				?>
						<p class="text-warning"><?php _e('Di sini Anda mengatur header dan footer dari setiap email, atau menggunakan yang default yang tersedia dengan sistem. Gunakan ini untuk menyesuaikan setiap bagian dan termasuk, misalnya, logo dan markup Anda sendiri. ','cftp_admin'); ?></p>
						<p class="text-warning"><?php _e("Jangan lupa juga sertakan - dan tutup sesuai - tag HTML struktural dasar (DOCTYPE, HTML, HEADER, BODY).",'cftp_admin'); ?></p>

						<div class="options_divide"></div>

						<div class="form-group">
							<div class="col-sm-12">
								<label for="email_header_footer_customize">
									<input type="checkbox" value="1" id="email_header_footer_customize" name="email_header_footer_customize" <?php echo (EMAILS_HEADER_FOOTER_CUSTOM == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Gunakan header / footer khusus','cftp_admin'); ?>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<label for="email_header_text"><?php _e('Header','cftp_admin'); ?></label>
								<textarea name="email_header_text" id="email_header_text" class="form-control textarea_high"><?php echo EMAILS_HEADER_TEXT; ?></textarea>
								<p class="field_note"><?php _e('Anda dapat menggunakan tag HTML di sini.','cftp_admin'); ?></p>
							</div>
						</div>

						<div class="preview_button">
							<button type="button" class="btn btn-default load_default" data-textarea="email_header_text" data-file="<?php echo EMAIL_TEMPLATE_HEADER; ?>"><?php _e('Ganti dengan standar','cftp_admin'); ?></button>
						</div>
						
						<hr />

						<div class="form-group">
							<div class="col-sm-12">
								<label for="email_footer_text"><?php _e('Footer','cftp_admin'); ?></label>
								<textarea name="email_footer_text" id="email_footer_text" class="form-control textarea_high"><?php echo EMAILS_FOOTER_TEXT; ?></textarea>
								<p class="field_note"><?php _e('Anda dapat menggunakan tag HTML di sini.','cftp_admin'); ?></p>
							</div>
						</div>

						<div class="preview_button">
							<button type="button" class="btn btn-default load_default" data-textarea="email_footer_text" data-file="<?php echo EMAIL_TEMPLATE_FOOTER; ?>"><?php _e('Ganti dengan bawaan ','cftp_admin'); ?></button>
						</div>
				<?php
					}

					/** All other templates */
					if ( array_key_exists( $section, $options_groups ) ) {
						$group = $options_groups[$section];
				?>
						<p class="text-warning"><?php echo $group['description']; ?></p>

						<div class="options_divide"></div>

						<div class="form-group">
							<div class="col-sm-12">
								<label for="<?php echo $group['subject_checkbox']; ?>">
									<input type="checkbox" value="1" name="<?php echo $group['subject_checkbox']; ?>" id="<?php echo $group['subject_checkbox']; ?>" class="checkbox_options" <?php echo ($group['subject_check'] == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Gunakan subjek khusus','cftp_admin'); ?>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<input type="text" name="<?php echo $group['subject']; ?>" id="<?php echo $group['subject']; ?>" class="form-control" placeholder="<?php _e('Tambahkan subjek khusus Anda','cftp_admin'); ?>" value="<?php echo $group['subject_text']; ?>" />
							</div>
						</div>	

						<div class="options_divide"></div>

						<div class="form-group">
							<div class="col-sm-12">
								<label for="<?php echo $group['body_checkbox']; ?>">
									<input type="checkbox" value="1" name="<?php echo $group['body_checkbox']; ?>" id="<?php echo $group['body_checkbox']; ?>" class="checkbox_options" <?php echo ($group['body_check'] == 1) ? 'checked="checked"' : ''; ?> /> <?php _e('Gunakan template khusus','cftp_admin'); ?>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<label for="<?php echo $group['body_textarea']; ?>"><?php _e('Template Teks','cftp_admin'); ?></label>
								<textarea name="<?php echo $group['body_textarea']; ?>" id="<?php echo $group['body_textarea']; ?>"  class="form-control textarea_high"><?php echo $group['body_text']; ?></textarea>
								<p class="field_note"><?php _e('Anda dapat menggunakan tag HTML di sini.','cftp_admin'); ?></p>
							</div>
						</div>	

						<?php
							if (!empty($group['tags'])) {
						?>
								<p><strong><?php _e("Tag-tag berikut dapat digunakan pada badan surel ini.",'cftp_admin'); ?></strong></p>
								<ul>
									<?php
										foreach ($group['tags'] as $tag => $description) {
									?>
											<li><i class="icon-ok"></i> <strong><?php echo $tag; ?></strong>: <?php echo $description; ?></li>
									<?php
										}
									?>
								</ul>
						<?php
							}
						?>

						<hr />
						<div class="preview_button">
							<button type="button" class="btn btn-default load_default" data-textarea="<?php echo $group['body_textarea']; ?>" data-file="<?php echo $group['default_text']; ?>"><?php _e('Ganti dengan standar','cftp_admin'); ?></button>
							<button type="button" data-preview="<?php echo $section; ?>" class="btn btn-wide btn-primary preview"><?php _e('Pratinjau templat ini','cftp_admin'); ?></button>
							<?php
								$message = __("Sebelum mencoba fungsi ini, harap simpan perubahan Anda untuk melihatnya tercermin pada pratinjau.",'cftp_admin');
								echo system_message('info', $message);
							?>
						</div>
				<?php
					}
				?>

				<div class="after_form_buttons">
					<button type="submit" name="submit" class="btn btn-wide btn-primary empty"><?php _e('Simpan opsi','cftp_admin'); ?></button>
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(e) {
		$('.load_default').click(function(e) {
			e.preventDefault();
			var file		= jQuery(this).data('file');
			var textarea	= '#'+jQuery(this).data('textarea');
			var accept		= confirm('<?php _e('Harap konfirmasi: ganti teks templat khusus dengan yang default?','cftp_admin'); ?>');
			if ( accept ) {
				$.ajax({
					url: "emails/"+file,
					async: false,
					cache: false,
					success: function (data){
						$(textarea).text(data);
					},
					error: function() {
						alert("<?php _e('Kesalahan: konten tidak dapat dimuat','cftp_admin'); ?>");
					}
				});
			}
		});

		$('.preview').click(function(e) {
			e.preventDefault();
			var type	= jQuery(this).data('preview');
			var url		= '<?php echo BASE_URI; ?>email-preview.php?t=' + type;
		    window.open(url, "previewWindow", "width=800,height=600,scrollbars=yes");
		});
	});
</script>

<?php
	include('footer.php');
