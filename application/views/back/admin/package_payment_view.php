<div>
    <?php
        echo form_open(base_url() . 'admin/package_payment/confirm/', array(
            'class' => 'form-horizontal',
            'method' => 'post',
            'id' => 'package_payment_view',
            'enctype' => 'multipart/form-data'
        ));
    ?>
        <div class="panel-body">
            <?php if($package_data['payment_details'] == '' || $package_data['payment_details'] == '[]' || $package_data['payment_status'] == 'due'){ ?>
                <div class="form-group">
                    <!-- <label class="col-sm-2 control-label" for="demo-hor-1"> </label> -->
                    <div class="col-sm-12 text-center">
                        <h1 style="color:red;"><?php echo translate("no_payment_info_provided"); ?></h1>
                    </div>
                </div>
            <?php } else { ?>
                
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <td><?php echo translate('user');?></td>
                                <td><?php echo $package_data['username']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('amount');?></td>
                                <td><?php echo currency('','def').$package_data['amount']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('datetime');?> </td>
                                <td><?php echo date('d M,Y',$package_data['purchase_datetime']); ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('method');?></td>
                                <td>
                                    <?php 
                                        if($package_data['payment_type'] == 'c2'){
                                            echo 'Twocheckout';
                                        }
                                        else echo $package_data['payment_type']; 
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?php echo translate('details');?></td>
                                <td><?php echo $package_data['payment_details']; ?></td>
                            </tr>
                            <tr>
                                <td><?php echo translate('status');?></td>
                                <td><?php echo $package_data['payment_status']; ?></td>
                            </tr>
                        </table>
                    </div>
                    <?php if($package_data['payment_status'] != 'paid'){?>
                    <div class="form-group btm_border">
                        <label class="col-md-5 control-label" for="page_name"><?php echo translate('status');?></label>
                        <div class="col-md-7">
                            <label class="checkbox-inline"><input type="checkbox" name="package_payment[payment_status]" value="paid"><?php echo translate('paid');?></label>
                            <input type="hidden" name="package_payment[package_payment_id]" value="<?php echo $package_data['package_payment_id']; ?>">
                            <input type="hidden" name="user[user_id]" value="<?php echo $package_data['user_id']; ?>">
                        </div>
                    </div>
                    <?php } ?>

                </div>
            <?php } ?>
        </div>
    </form>
</div>
<div id="reserve"></div>
<script type="text/javascript">

    $(document).ready(function() {
        set_switchery();
    });


    $(document).ready(function() {
        $("form").submit(function(e){
            return false;
        });
    });
</script>
<div id="reserve"></div>
<script type="text/javascript">
    $(document).ready(function(){
        var attr = {name:'payment_status', value:'paid'};
        $('.enterer').attr(attr);
        $('.enterer').text('<?php echo translate('confirm_payment'); ?>');
        // $('.enterer').hide();
    });
    function num(id){
       id.value = id.value.replace(/[^0-9]/g, '');
    }
</script>
