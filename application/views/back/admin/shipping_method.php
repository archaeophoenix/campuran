<?php
    $shipping_type = $this->db->get_where('business_settings', array('type' => 'rajaongkir_type'))->row()->value;
    $shipping_api = $this->db->get_where('business_settings', array('type' => 'rajaongkir_api'))->row()->value; 
?>
<div id="content-container">
    <div id="page-title">
        <center>
        	<h1 class="page-header text-overflow">
				<?php echo translate('manage_payment_methods_&_shipment')?>
            </h1>
        </center>
    </div>
    <?php
		echo form_open(base_url() . 'admin/business_settings/set1/', array(
			'class'     => 'form-horizontal',
			'method'    => 'post',
			'id'        => 'gen_set',
			'enctype'   => 'multipart/form-data'
		));
	?>
    
        <div class="col-md-12" style="display: none;">
            <div class="panel panel-bordered panel-dark">
                <div class="panel-heading">
                    <center>
                        <h3 class="panel-title"><?php echo translate('payment_methods_settings')?></h3>
                    </center>
                </div>
                <div class="panel-body" style="background:#fffffb;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('paypal_settings')?></h4>
                                </div>       
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('paypal_email');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="paypal_email" value="<?php echo (isset($paypal) || !empty($paypal)) ? $paypal : ''; ?>" class="form-control">
                                        </div>
                                    </div>    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">
                                            <?php echo translate('paypal_account_type');?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?php
                                                $from = array('sandbox','original');
                                                $paypal_type = (isset($paypal_type) || !empty($paypal_type)) ? $paypal_type : '' ;
                                                echo $this->crud_model->select_html($from,'paypal_type','','edit','demo-chosen-select',$paypal_type);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('stripe_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('stripe_secret_key');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="stripe_secret" value="<?php echo (isset($stripe_secret) || !empty($stripe_secret)) ? $stripe_secret : ''; ?>" class="form-control">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('stripe_publishable_key');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="stripe_publishable" value="<?php echo (isset($stripe_publishable) || !empty($stripe_publishable)) ? $stripe_publishable : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('twocheckout_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('user_id');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="c2_user" value="<?php echo (isset($c2_user) || !empty($c2_user)) ? $c2_user : ''; ?>" class="form-control">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('secret_key');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="c2_secret" value="<?php echo (isset($c2_secret) || !empty($c2_secret)) ? $c2_secret : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">
                                            <?php echo translate('account_type');?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?php
                                                $from = array('demo','original');
                                                $c2_type = (isset($c2_type) || !empty($c2_type)) ? $c2_type : '' ;
                                                echo $this->crud_model->select_html($from,'c2_type','','edit','demo-chosen-select',$c2_type);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('pay_u_money_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('merchant_key');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="merchant_key" value="<?php echo (isset($merchant_key) || !empty($merchant_key)) ? $merchant_key : ''; ?>" class="form-control">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('merchant_salt');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="merchant_salt" value="<?php echo (isset($merchant_salt) || !empty($merchant_salt)) ? $merchant_salt : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">
                                            <?php echo translate('account_type');?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?php
                                                $from = array('sandbox','original');
                                                $pum_type = (isset($pum_type) || !empty($pum_type)) ? $pum_type : '' ;
                                                echo $this->crud_model->select_html($from,'pum_account_type','','edit','demo-chosen-select',$pum_type);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="display:none;">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('sslcommerz_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('ssl_store_id');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="ssl_store_id" value="<?php echo (isset($ssl_store_id) || !empty($ssl_store_id)) ? $ssl_store_id : ''; ?>" class="form-control">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('ssl_store_password');?></label>
                                        <div class="col-sm-8">
                                            <input type="password" name="ssl_store_passwd" value="<?php echo (isset($ssl_store_passwd) || !empty($ssl_store_passwd)) ? $ssl_store_passwd : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">
                                            <?php echo translate('account_type');?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?php
                                                $from = array('sandbox','original');
                                                $ssl_type = (isset($ssl_type) || !empty($ssl_type)) ? $ssl_type : '' ;
                                                echo $this->crud_model->select_html($from,'ssl_type','','edit','demo-chosen-select',$ssl_type);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('voguePay_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('merchant_id');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="vp_merchant_id" value="<?php echo (isset($vp_merchant_id) || !empty($vp_merchant_id)) ? $vp_merchant_id : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel">
                                <div class="panel-heading bg-white">
                                    <h4 class="panel-title"><?php echo translate('duitku_settings')?></h4>
                                </div>
                    
                                <!--Panel body-->
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('merchant_code');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="duitku_code" value="<?php echo (isset($duitku_code) || !empty($duitku_code)) ? $duitku_code : ''; ?>" class="form-control">
                                        </div>
                                    </div>
        
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('merchant_key');?></label>
                                        <div class="col-sm-8">
                                            <input type="text" name="duitku_key" value="<?php echo (isset($duitku_key) || !empty($duitku_key)) ? $duitku_key : ''; ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-inputemail">
                                            <?php echo translate('account_type');?>
                                        </label>
                                        <div class="col-sm-8">
                                            <?php
                                                $from = array('sandbox','original');
                                                $duitku_type = (isset($duitku_type) || !empty($duitku_type)) ? $duitku_type : '' ;
                                                echo $this->crud_model->select_html($from,'duitku_type','','edit','demo-chosen-select',$duitku_type);
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="panel panel-bordered panel-dark">
                        <div class="panel-heading">
                            <center>
                                <h3 class="panel-title"><?php echo translate('shipment_settings')?></h3>
                            </center>
                        </div>
                        <div class="panel-body" style="background:#fffffb;">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" ><?php echo translate('shipping_cost_type');?></label>
                                <div class="col-sm-6">
                                    <?php $sp_type = (isset($sp_type) || !empty($sp_type)) ? $sp_type : '' ; ?>
                                    <select name="shipping_cost_type" class="demo-cs-multiselect">
                                        <option value="product_wise" 
                                            <?php if($sp_type == 'product_wise'){ echo 'selected'; } ?> >
                                                <?php echo translate('product_wise');?>
                                                    </option>
                                        <option value="fixed" 
                                            <?php if($sp_type == 'fixed'){ echo 'selected'; } ?> >
                                                <?php echo translate('fixed');?>
                                                    </option>
                                    </select>
                                </div>
                            </div>
                                    
                            <div class="form-group">
                                <label class="col-sm-3 control-label" ><?php echo translate('shipping_cost_(If_fixed)');?> (<?php echo currency('','def'); ?>)</label>
                                <div class="col-sm-6">
                                    <input type="text" name="shipping_cost" 
                                        value="<?php echo (isset($sp_val) || !empty($sp_val)) ? $sp_val : ''; ?>" 
                                            class="form-control">
                                </div>
                            </div>
                                    
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo translate('shipment_info');?></label>
                                <div class="col-sm-6">
                                    <textarea class="summernotes" data-height="200" data-name="shipment_info" ><?php echo (isset($shipment_info) || !empty($shipment_info)) ? $shipment_info : ''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    	<div class="panel-footer text-right">
            <span class="btn btn-info submitter enterer" 
                data-ing='<?php echo translate('saving'); ?>' data-msg='<?php echo translate('settings_updated!'); ?>' >
                    <?php echo translate('save');?>
            </span>
        </div>
    </form>
</div>
<style>
.bg-white{
	background:#ffffff !important;
	color:#000 !important;
}
</style>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var user_type = 'admin';
	var module = 'business_settings';
	var list_cont_func = '';
	var dlt_cont_func = '';
</script>

<script src="<?php echo base_url(); ?>template/back/js/custom/business.js"></script>
