<h1 style="display:none;"><?php echo translate('product_stock'); ?></h1>
		<table id="export-table" data-name='product_stock' data-orientation='p' style="display:none;">
    									<thead>
    												<tr>
    													<th>No.</th>
    													<th>Product</th>
    													<th>Pabrikan / Vendor</th>
    													<th>Kategori</th>
    													<th>Subkategori</th>
    													<th>Total Stok</th>
    												</tr>
    											</thead>
    									<tbody>
									    
										<?php 
										$no = 1;
										foreach($product_list as $row){ ?>
											<tr>
												<td><?php echo $no++;?></td>
												<td><?php echo $row['title'];?></td>
												<td>
												    <?php $add_by = json_decode($row['added_by'],true);
												    echo $add_by['type'] == 'manufacture' ? $this->crud_model->get_type_name_by_id('manufacture',$add_by['id'],'name')."(Manufacture)" : $this->crud_model->get_type_name_by_id('vendor',$add_by['id'],'name')."(Seller)";
												    ?>
												</td>
												<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name');?></td>
												<td><?php echo $this->crud_model->get_type_name_by_id('sub_category',$row['sub_category'],'sub_category_name');?></td>
												<td>
												    <?php if($row['downlad']=='ok'){ echo "1";
												    }elseif($row['is_bundle'] == 'yes'){
												    echo empty($row['current_stock']) ? 0 : $row['current_stock'];
												    }else{
												    
												    $this->db->select('sum(current_stock) as total_stock');
												    $this->db->where('manufacture_product_id',$row['manufacture_product_id']);
												    $this->db->group_by('manufacture_product_id');
												    $total_stock = $this->db->get('product')->row_array();
												    
												    echo empty($total_stock['total_stock']) ? 0 : $total_stock['total_stock'];
												    }?></td>
											</tr>
										<?php } ?>
									
									
									</tbody>
    								</table>