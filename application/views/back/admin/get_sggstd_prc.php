<div class="form-group btm_border">
  <label for="demo-ho-20" class="col-sm-4 control-label"><?php echo  translate('recommended_selling_price'); ?></label>
  <div class="col-sm-4">
    <input type="text" disabled value="<?php echo $suggested_price; ?>" class="form-control">
  </div>
  <span class="btn"><?php echo currency('', 'def'); ?> / </span>
  <span class="btn"><?php echo  $unit; ?></span>
</div>
<div class="form-group btm_border">
  <label for="demo-ho-20" class="col-sm-4 control-label"><?php echo  translate('manufacture_selling_price'); ?></label>
  <div class="col-sm-4">
    <input type="text" disabled value="<?php echo $sale_price; ?>" class="form-control">
  </div>
  <span class="btn"><?php echo currency('', 'def'); ?> / </span>
  <span class="btn"><?php echo  $unit; ?></span>
</div>
<div class="form-group btm_border">
  <label for="demo-ho-20" class="col-sm-4 control-label"><?php echo  translate('manufacture_discount_products'); ?></label>
  <div class="col-sm-4">
    <input type="text" disabled value="<?php echo $discount; ?>" class="form-control">
  </div>
  <span class="btn"><?php echo ($discount_type == 'percent') ? '%' : currency('', 'def'); ?></span>
</div>

<div class="form-group btm_border <?php echo ('ok' == $high_price_lock) ? 'has-error' : 'has-warning'; ?>">
  <label for="demo-ho-20" class="col-sm-4 control-label"><?php echo  translate('high_price'); ?></label>
  <div class="col-sm-4">
    <input type="text" disabled id="high_price" value="<?php echo $high_price; ?>" class="form-control">
    <input type="hidden" id="high_price_lock" value="<?php echo $high_price_lock; ?>">
  </div>
  <span class="btn"><?php echo currency('', 'def'); ?> / </span>
  <span class="btn"><?php echo  $unit; ?></span>
</div>
<div class="form-group btm_border <?php echo ('ok' == $low_price_lock) ? 'has-error' : 'has-warning'; ?>">
  <label for="demo-ho-20" class="col-sm-4 control-label"><?php echo  translate('low_price'); ?></label>
  <div class="col-sm-4">
    <input type="text" disabled id="low_price" value="<?php echo $low_price; ?>" class="form-control">
    <input type="hidden" id="low_price_lock" value="<?php echo $low_price_lock; ?>">
  </div>
  <span class="btn"><?php echo currency('', 'def'); ?> / </span>
  <span class="btn"><?php echo  $unit; ?></span>
</div>

<script>
let high = true;
let low = true;

const price = document.querySelector('#sale_price');
const hlock = document.querySelector('#high_price_lock').value;
const llock = document.querySelector('#low_price_lock').value;
const hprice = parseFloat(document.querySelector('#high_price').value);
const lprice = parseFloat(document.querySelector('#low_price').value);
const submit = document.querySelector('.enterer');
const sapri = document.querySelector('#sapri');

price.addEventListener('input', event => {
  let has = 'form-group btm_border ';
  high = (parseFloat(price.value) > hprice && hlock == 'ok') ? false : true;
  low = (parseFloat(price.value) < lprice && llock == 'ok') ? false : true;


  has += (high == low && low == true) ? 'has-success' : 'has-error';

  sapri.setAttribute('class', has);

  if (high == low) {
    submit.removeAttribute('disabled');
  } else {
    submit.setAttribute('disabled', 'disabled');
  }

});

price.dispatchEvent(new Event('input'));
</script>