<form class="form-horizontal" action="<?php echo base_url() . 'admin/subscription_payment/approval_payment'; ?>" method="post" id="ad_payment_approval" enctype="multipart/form-data">
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped">
        <?php $nyetatus = ['pending' => 'Menunggu Konfirmasi', 'paid' => 'Lunas', 'due' => 'Belum Lunas']; ?>
        <tr>
          <td><?php echo translate('user');?></td>
          <td><?php echo $data['firstname']; ?></td>
        </tr>
        <tr>
          <td><?php echo translate('amount');?></td>
          <td>Rp <?php echo $data['amount']; ?></td>
        </tr>

        <tr>
          <td><?php echo translate('purchase_datetime');?> </td>
          <td><?php echo date('d M,Y',$data['purchase_datetime']); ?></td>
        </tr>

        <tr>
          <td><?php echo translate('payment_datetime');?> </td>
          <td><?php echo date('d M,Y',$data['payment_timestamp']); ?></td>
        </tr>

        <tr>
          <td><?php echo translate('payment_type');?></td>
          <td><?php echo $duit[$data['payment_type']]; ?></td>
        </tr>
        <tr>
          <td><?php echo translate('details');?></td>
          <td><?php echo $data['payment_details']; ?></td>
        </tr>
      </table>
      <?php if($data['payment_status'] != 'paid'){?>
      <div>
          <label class="col-md-5 control-label" for="page_name"><?php echo translate('status');?></label>
          <div class="col-md-7">
              <input type="hidden" name="subscription_payment_id" value="<?php echo $data['subscription_payment_id']; ?>">
              <label class="checkbox-inline"><input type="checkbox" name="payment_status" id="payment_status" value="paid"><?php echo translate('paid');?></label>
          </div>
      </div>
      <?php } ?>
    </div>
  </div>
</form>

<script>
$(document).ready(function(){
  if (!$('#payment_status').length) {
    $('.enterer').hide();
  }

  $('.enterer').click(function(){
    $('#ad_payment_approval').submit();
  });
});
</script>