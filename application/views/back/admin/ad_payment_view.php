<form class="form-horizontal" action="<?php echo base_url() . 'admin/payments/approval_payment'; ?>" method="post" id="ad_payment_approval" enctype="multipart/form-data">
  <div class="panel-body">
    <div class="table-responsive">
      <table class="table table-striped">
        <?php $nyetatus = ['pending' => 'Menunggu Konfirmasi', 'paid' => 'Lunas', 'due' => 'Belum Lunas']; ?>
        <tr>
          <td><?php echo translate('user');?></td>
          <td><?php echo $data['firstname']; ?></td>
        </tr>
        <tr>
          <td><?php echo translate('amount');?></td>
          <td>Rp <?php echo $data['amount']; ?></td>
        </tr>

        <tr>
          <td><?php echo translate('purchase_datetime');?> </td>
          <td><?php echo date('d M,Y',$data['purchase_datetime']); ?></td>
        </tr>

        <tr>
          <td><?php echo translate('payment_datetime');?> </td>
          <td><?php echo date('d M,Y',$data['payment_timestamp']); ?></td>
        </tr>

        <tr>
          <td><?php echo translate('package_time_duration');?> </td>
          <td>
            <?php 
              switch ($data['package_id']) {
                case '1':
                  $duration_timestamp = $data['purchase_datetime'] + (7*24*60*60);
                  break;

                case '2':
                  $duration_timestamp = $data['purchase_datetime'] + (30*24*60*60);
                  break;

                case '3':
                  $duration_timestamp = $data['purchase_datetime'] + (180*24*60*60);
                  break;

                case '4':
                  $duration_timestamp = $data['purchase_datetime'] + (365*24*60*60);
                  break;
                
                default:
                  # code...
                  break;
              }
              echo date('d M,Y',$duration_timestamp); 
            ?>
          </td>
        </tr>
        <tr>
          <td><?php echo translate('payment_type');?></td>
          <td><?php echo $duit[$data['payment_type']]; ?></td>
        </tr>
        <tr>
          <td><?php echo translate('details');?></td>
          <td><?php echo $data['payment_details']; ?></td>
        </tr>
      </table>
      <?php if($data['payment_status'] != 'paid'){?>
      <div>
          <label class="col-md-5 control-label" for="page_name"><?php echo translate('status');?></label>
          <div class="col-md-7">
              <input type="hidden" name="advertisement_id" value="<?php echo $data['advertisement_id']; ?>">
              <input type="hidden" name="expire_timestamp" value="<?php echo $duration_timestamp; ?>">
              <input type="hidden" name="advertisement_payment_id" value="<?php echo $data['advertisement_payment_id']; ?>">
              <label class="checkbox-inline"><input type="checkbox" name="payment_status" id="payment_status" value="paid"><?php echo translate('paid');?></label>
          </div>
      </div>
      <?php } ?>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  if (!$('#payment_status').length) {
    $('.enterer').hide();
  }

  $('.enterer').click(function(){
    $('#ad_payment_approval').submit();
  });
});
</script>