<script src="https://maps.google.com/maps/api/js?v=3.exp&signed_in=true&callback=MapApiLoaded&key=<?php echo $this->db->get_where('general_settings',array('type' => 'google_api_key'))->row()->value; ?>"></script>
<div id="content-container">
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo translate('manage_profile');?></h1>
    </div>
    <div class="tab-base">
        <div class="panel">
            <div class="panel-body">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="" style="border:1px solid #ebebeb; border-radius:4px;">
                        <?php
							$row = $this->db->get_where('manufacture', array(
								'manufacture_id' => $this->session->userdata('manufacture_id')
							))->row_array();
							// foreach ($manufacture_data as $row) {
                                $location = $this->crud_model->selected('subdistrict_id', $row['rajaongkir_subdistrict']);
						?>
                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo translate('manage_details');?></h3>
                        </div>
						<?php
                            echo form_open(base_url() . 'manufacture/manage_manufacture/update_profile/', array(
                                'class' => 'form-horizontal',
                                'method' => 'post'
                            ));
                        ?>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="name">
                                    	<?php echo translate('name');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="name" value="<?php echo $row['name']; ?>" id="name" class="form-control required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-1">
                                    	<?php echo translate('company');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="company" value="<?php echo $row['company']; ?>" id="demo-hor-1" class="form-control required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-1">
                                    	<?php echo translate('display_name');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="display_name" value="<?php echo $row['display_name']; ?>" id="demo-hor-1" class="form-control required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-2">
										<?php echo translate('email');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="email" name="email" value="<?php echo $row['email']; ?>" id="demo-hor-2" class="form-control required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-3">
										<?php echo translate('phone');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="phone" value="<?php echo $row['phone']; ?>" id="demo-hor-3" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('address_line_1');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="address1" value="<?php echo $row['address1']; ?>" id="demo-hor-4" class="form-control address" onblur="set_cart_map('iio');">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('address_line_2');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="address2" value="<?php echo $row['address2']; ?>" id="demo-hor-4" class="form-control address" onblur="set_cart_map('iio');">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('subdistrict');?>
                                    </label>
                                    <div class="col-sm-6">
                                       <input type="text" value="<?php echo $location['subdistrict_name'] . ' - ' . $location['type'] . ' ' . $location['city_name']; ?>" id="demo-hor-4" class="form-control subdistrict" onblur="set_cart_map('iio');">
                                       <input type="hidden" name="rajaongkir_subdistrict" id="subdistrict_id" value="<?php echo $row['rajaongkir_subdistrict']; ?>">
                                       <input type="hidden" name="rajaongkir_city" id="city_id" value="<?php echo $row['rajaongkir_city']; ?>">
                                       <input type="hidden" name="rajaongkir_province" id="province_id" value="<?php echo $row['rajaongkir_province']; ?>">
                                       <input type="hidden" name="subdistrict" id="subdistrict_name" value="<?php echo $row['subdistrict']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="city_name">
                                        <?php echo translate('city');?>
                                    </label>
                                    <div class="col-sm-6">
                                        <input type="text" name="city" readonly value="<?php echo $row['city']; ?>" id="city_name" class="form-control address" onblur="set_cart_map('iio');">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="province">
										<?php echo translate('state');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="state" readonly value="<?php echo $row['state']; ?>" id="province" class="form-control address" onblur="set_cart_map('iio');">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('country');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="country" value="<?php echo $row['country']; ?>" id="demo-hor-4" class="form-control address" onblur="set_cart_map('iio');">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('zip');?>
                                	</label>
                                    <div class="col-sm-6">
                                        <input type="text" type="show" placeholder="Isi Kode Pos " name="zip" value="<?php echo $row['zip']; ?>" id="demo-hor-4" class="form-control address" onblur="set_cart_map('iio');">
                                        </br>
                                        <input id="lat_lang" type="hidden" placeholder="Isi Langitude - Latitude Peta Lokasi Kantor contoh  [-6.2245908,106.8023311,15] " name="lat_lang" value="<?php echo $row['lat_lang']; ?>" class="form-control"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4"><?php echo translate('locate_location');?></label>
                                    <div class="col-sm-6">
                                        <div class="" id="map" style="height:400px;" >
                                            <!-- <div id="map-canvas" style="height:400px;"></div> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="demo-hor-4">
										<?php echo translate('about');?>
                                        	</label>
                                    <div class="col-sm-6">
                                        <textarea name="details" rows="10" class="form-control"><?php echo $row['details']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <span class="btn btn-info submitter enterer" data-ing='<?php echo translate('updating..'); ?>' data-msg='<?php echo translate('profile_updated!'); ?>'>
									<?php echo translate('update_profile');?>
                                    	</span>
                            </div>
                        </form>

                        <div class="panel-heading">
                            <h3 class="panel-title"><?php echo translate('change_password');?></h3>
                        </div>
							<?php
                                echo form_open(base_url() . 'manufacture/manage_manufacture/update_password/', array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post'
                                ));
                            ?>
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-5">
                                        	<?php echo translate('current_password');?>
                                            	</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password" value="" id="demo-hor-5" class="form-control required">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-6">
                                        	<?php echo translate('new_password*');?>
                                            	</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password1" value="" id="demo-hor-6" class="form-control pass pass1 required">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="demo-hor-7">
                                        	<?php echo translate('confirm_password');?>
                                            	</label>
                                        <div class="col-sm-6">
                                            <input type="password" name="password2" value="" id="demo-hor-7" class="form-control pass pass2 required">
                                        </div>
                                        <div id="pass_note">
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-right">
                                    <span class="btn btn-info pass_chng disabled enterer" disabled='disabled' data-ing='<?php echo translate('updating..'); ?>' data-msg='<?php echo translate('password_updated!'); ?>'>
                                    	<?php echo translate('update_password');?>
                                    		</span>
                                </div>
                        	</form>
                        <?php
                            //}
                        ?>
                    </div>
                </div>
            </div>
        <!--Panel body-->
        </div>
    </div>
</div>
<?php $coo = json_decode($row['lat_lang'], true); ?>
<input type="hidden" id="lat" value="<?php echo $coo[0]; ?>">
<input type="hidden" id="lng" value="<?php echo $coo[1]; ?>">
<script type="text/javascript">
	$(".pass").blur(function() {
		var pass1 = $(".pass1").val();
		var pass2 = $(".pass2").val();
		if (pass1 !== pass2) {
			$("#pass_note").html('' + '  <span class="require_alert" >' + '      <?php echo translate('password_mismatched'); ?>' + '  </span>');
			$(".pass_chng").attr("disabled", "disabled");
			$(".pass_chng").addClass("disabled");
		} else if (pass1 == pass2) {
			$("#pass_note").html('');
			$(".pass_chng").removeAttr("disabled");
			$(".pass_chng").removeClass("disabled");
		}
	});
	
	$('.pass_chng').on('click', function() {
	
		//alert('vdv');
		var here = $(this); // alert div for show alert message
		var form = here.closest('form');
		var can = '';
		var ing = here.data('ing');
		var msg = here.data('msg');
		var prv = here.html();
	
		//var form = $(this);
		var formdata = false;
		if (window.FormData) {
			formdata = new FormData(form[0]);
		}
	
		var a = 0;
		var take = '';
		form.find(".required").each(function() {
			var txt = '*<?php echo translate('required'); ?>';
			a++;
			if (a == 1) {
				take = 'scroll';
			}
			var here = $(this);
			if (here.val() == '') {
				if (!here.is('select')) {
					here.css({
						borderColor: 'red'
					});
	
					if (here.closest('div').find('.require_alert').length) {
	
					} else {
						sound('form_submit_problem');
						here.closest('div').append('' + '  <span id="' + take + '" class="label label-danger require_alert" >' + '      ' + txt + '  </span>');
					}
				}
				var topp = 100;
	
				$('html, body').animate({
					scrollTop: $("#scroll").offset().top - topp
				}, 500);
				can = 'no';
			}
	
			take = '';
		});
	
		if (can !== 'no') {
			$.ajax({
				url: form.attr('action'), // form action url
				type: 'POST', // form submit method get/post
				dataType: 'html', // request type html/json/xml
				data: formdata ? formdata : form.serialize(), // serialize form data 
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					here.html(ing); // change submit button text
				},
				success: function(data) {
					here.fadeIn();
					here.html(prv);
					if (data == 'updated') {
						$.activeitNoty({
							type: 'success',
							icon: 'fa fa-check',
							message: msg,
							container: 'floating',
							timer: 3000
						});
					} else if (data == 'pass_prb') {
						$.activeitNoty({
							type: 'danger',
							icon: 'fa fa-check',
							message: '<?php echo translate('incorrect_password!'); ?>',
							container: 'floating',
							timer: 3000
						});
					}
				},
				error: function(e) {
					console.log(e)
				}
			});
		} else {
			sound('form_submit_problem');
			return false;
		}
	});

	var base_url = '<?php echo base_url(); ?>';
	var user_type = 'manufacture';
	var module = 'manage_admin';
	var list_cont_func = '';
	var dlt_cont_func = '';

</script>

<!-- <script>
    
    $(document).ready(function(){
      set_cart_map();
    });

    function set_cart_map(tty){
        //$('#maps').animate({ height: '400px' }, 'easeInOutCubic', function(){});
        initialize();
        var address = [];
        //$('#pos').show('fast');
        //$('#lnlat').show('fast');
        $('.address').each(function(index, value){
            if(this.value !== ''){
                address.push(this.value);
            }
        });
        address = address.toString();
        deleteMarkers();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if($('#langlat').val().indexOf(',')  == -1 || $('#first').val() == 'no' || tty == 'iio'){
                    deleteMarkers();
                    var location = results[0].geometry.location; 
                    var marker = addMarker(location);
                    map.setCenter(location);
                    $('#langlat').val(location);
                } else if($('#langlat').val().indexOf(',')  >= 0){
                    deleteMarkers();
                    var loca = $('#langlat').val();
                    loca = loca.split(',');
                    var lat = loca[0].replace('(','');
                    var lon = loca[1].replace(')','');
                    var marker = addMarker(new google.maps.LatLng(lat, lon));
                    map.setCenter(new google.maps.LatLng(lat, lon));
                }
                if($('#first').val() == 'yes'){
                    $('#first').val('no');
                }
                // Add dragging event listeners.
                google.maps.event.addListener(marker, 'drag', function() {
                    $('#langlat').val(marker.getPosition());
                });
            }
        }); 
    }

    var geocoder;
    var map;
    var markers = [];
    function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
            zoom: 14,
            center: latlng
        }
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        google.maps.event.addListener(map, 'click', function(event) {
            deleteMarkers();
            var marker = addMarker(event.latLng);
            $('#langlat').val(event.latLng);    
            // Add dragging event listeners.
            google.maps.event.addListener(marker, 'drag', function() {
                $('#langlat').val(marker.getPosition());
            });
            
        });     
    }
        

    /*
        var address = [];
        $('#maps').show('fast');
        $('#pos').show('fast');
        $('#lnlat').show('fast');
        $(".address").each(
        address.push(this.value);
        );
    */

    $('.address').on('blur', function(){
		$('#langlat').val('');
        set_cart_map();
    });

    // Add a marker to the map and push to the array.
    function addMarker(location) {
        var image = {
            url: base_url+'uploads/others/marker.png',
            size: new google.maps.Size(40, 60),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(20, 62)
        };

        var shape = {
            coords: [1, 5, 15, 62, 62, 62, 15 , 5, 1],
            type: 'poly'
        };

        var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable:true,
            icon: image,
            shape: shape,
            animation: google.maps.Animation.DROP
        });
        markers.push(marker);
        return marker;
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    // Sets the map on all markers in the array.
    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setAllMap(null);
    }
    //google.maps.event.addDomListener(window, 'load', initialize);
</script> -->

<script>
    var map;
    var marker;
    var InforObj = [];
    var centerCords = {
      lat: parseFloat($('#lat').val()),
      lng: parseFloat($('#lng').val())
    };
    var markersOnMap = [{
        placeName: $('#name').val(),
        LatLng: [{lat: parseFloat($('#lat').val()), lng: parseFloat($('#lng').val())}]
      }
    ];

    window.onload = function () {
      initMap();
    };

    function taruhMarker(peta, posisiTitik){
    
        if( marker ){
          // pindahkan marker
          marker.setPosition(posisiTitik);
        } else {
          // buat marker baru
          marker = new google.maps.Marker({
            position: posisiTitik,
            map: peta
          });
        }
      
         // isi nilai koordinat ke form
        document.getElementById('lat_lang').value = '[' + posisiTitik.lat() + ',' + posisiTitik.lng() + ']';
        
    }

    function addMarkerInfo() {
      for (var i = 0; i < markersOnMap.length; i++) {
        var contentString = '<div id="content"><h5>' + $('#name').val() + '</h5><p><a href="https://www.google.com/maps/place/' + parseFloat($('#lat').val()) + ',' + parseFloat($('#lng').val()) + '" target="_blank">map</a></p></div>';

        const marker = new google.maps.Marker({
          position: markersOnMap[i].LatLng[0],
          map: map,
          icon:'https://promedis.id/uploads/logo_image/logo_59.png'
        });

        const infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 200
        });

        marker.addListener('click', function () {
          closeOtherInfo();
          infowindow.open(marker.get('map'), marker);
          InforObj[0] = infowindow;
        });
        // marker.addListener('mouseover', function () {
        //   closeOtherInfo();
        //   infowindow.open(marker.get('map'), marker);
        //   InforObj[0] = infowindow;
        // });
        // marker.addListener('mouseout', function () {
        //   closeOtherInfo();
        //   infowindow.close();
        //   InforObj[0] = infowindow;
        // });
      }
    }

    function closeOtherInfo() {
      if (InforObj.length > 0) {
        /* detach the info-window from the marker ... undocumented in the API docs */
        InforObj[0].set("marker", null);
        /* and close it */
        InforObj[0].close();
        /* blank the array */
        InforObj.length = 0;
      }
    }

    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 9,
        center: centerCords,
        mapTypeId:google.maps.MapTypeId.ROADMAP
      });
      google.maps.event.addListener(map, 'click', function(event) {
        taruhMarker(this, event.latLng);
      });
      addMarkerInfo();
    }
</script>