<?php
$physical_check = $this->crud_model->get_type_name_by_id('general_settings','68','value');
$digital_check = $this->crud_model->get_type_name_by_id('general_settings','69','value');
?>
<nav id="mainnav-container">
    <div id="mainnav">
        <!--Menu-->
        <div id="mainnav-menu-wrap">
            <div class="nano">
                <div class="nano-content">
                    <ul id="mainnav-menu" class="list-group">
                        <!--Category name-->
                        <li class="list-header"></li>
            
                        <!--Menu list item-->
                        <li <?php if($page_name=="dashboard"){?> class="active-link" <?php } ?> 
                        	style="border-top:1px solid rgba(69, 74, 84, 0.7);">
                            <a href="<?php echo base_url(); ?>manufacture/">
                                <i class="fa fa-dashboard"></i>
                                <span class="menu-title">
									<?php echo translate('dashboard');?>
                                </span>
                            </a>
                        </li>
                        
            			<?php
						if($physical_check == 'ok' && $digital_check == 'ok'){
                            if(  $this->crud_model->manufacture_permission('product') ||
                                        $this->crud_model->manufacture_permission('sub_category') ||
                                        	$this->crud_model->manufacture_permission('stock') || 
                                                   $this->crud_model->manufacture_permission('digital') ||
                                                        $this->crud_model->manufacture_permission('brand')) {
						?>
                        <!--Menu list item-->
                        <li <?php if($page_name=="product" || 
                                         	$page_name=="stock" ||
													$page_name=="digital" ){?>
                                                     			class="active-sub" 
                                                       				<?php } ?> >
                            <a href="#">
                                <i class="fa fa-shopping-cart"></i>
                                    <span class="menu-title">
                                        <?php echo translate('products');?>
                                    </span>
                                	<i class="fa arrow"></i>
                            </a>
            
                            <!--PRODUCT------------------>
                            <ul class="collapse <?php if($page_name=="product" ||
                                                            $page_name=="sub_category_digital"||
															    $page_name=="stock" || 
                                                                    $page_name=="digital" ||
                                                                        $page_name=="brand" ){?>
                                                                             		in
                                                                                		<?php } ?> >" >
							<?php
                                if( $this->crud_model->manufacture_permission('product') || 
                                        $this->crud_model->manufacture_permission('sub_category') ||
                                            $this->crud_model->manufacture_permission('stock') ||
                                                $this->crud_model->manufacture_permission('brand')){
                            ?>
                            <!--Menu list item-->
                                <li <?php if($page_name=="product" || 
                                                $page_name=="sub_category" ||
                                                    $page_name=="stock"  ||
                                                        $page_name=="brand"){?>
                                                                 class="active-sub" 
                                                                <?php } ?> >
                                    <a href="#">
                                        <i class="fa fa-list"></i>
                                            <span class="menu-title">
                                                <?php echo translate('physical_products');?>
                                            </span>
                                            <i class="fa arrow"></i>
                                    </a>
                    
                                    <!--PRODUCT------------------>
                                    <ul class="collapse <?php if($page_name=="product" || 
                                                                    $page_name=="sub_category" ||
                                                                        $page_name=="stock" ||
                                                                            $page_name=="brand"){?>
                                                                		in
                                                                     		<?php } ?> " >
                                        <?php if($this->crud_model->manufacture_permission('brand')){
                                        ?>
                                            <li <?php if($page_name=="brand"){?> class="active-link" <?php } ?> >
                                                <a href="<?php echo base_url(); ?>manufacture/brand">
                                                    <i class="fa fa-circle fs_i"></i>
                                                        <?php echo translate('brands');?>
                                                </a>
                                            </li>
                                        <?php 
                                            } if($this->crud_model->manufacture_permission('product')){
                                        ?>
                                            <li <?php if($page_name=="product"){?> class="active-link" <?php } ?> >
                                                <a href="<?php echo base_url(); ?>manufacture/product">
                                                    <i class="fa fa-circle fs_i"></i>
                                                        <?php echo translate('all_products');?>
                                                </a>
                                            </li>
                                        <?php
                                            } if($this->crud_model->manufacture_permission('sub_category')){
                                                ?>
                                                    <li <?php if($page_name=="sub_category"){?> class="active-link" <?php } ?> >
                                                        <a href="<?php echo base_url(); ?>manufacture/sub_category">
                                                            <i class="fa fa-circle fs_i"></i>
                                                                <?php echo translate('sub-category');?>
                                                        </a>
                                                    </li>
                                                <?php
                                            }
                                        ?>
                                    </ul>
                                </li>
                          
                            <?php
                                }
                            ?>
                        
                            </ul>
                        </li>
            			<?php
								}
							}
						?>
                        <?php
						if($physical_check == 'ok' && $digital_check !== 'ok'){  
                        	if( $this->crud_model->manufacture_permission('product') || 
                           			$this->crud_model->manufacture_permission('stock') ){
						?>
						<!--Menu list item-->
							<li <?php if($page_name=="product" || 
											$page_name=="stock" ){?>
														 class="active-sub" 
															<?php } ?> >
								<a href="#">
									<i class="fa fa-list"></i>
										<span class="menu-title">
											<?php echo translate('products');?>
										</span>
										<i class="fa arrow"></i>
								</a>
				
								<!--PRODUCT------------------>
								<ul class="collapse <?php if($page_name=="product" || 
																$page_name=="stock" ){?>
																	in
																		<?php } ?> " >
									
									<?php if($this->crud_model->manufacture_permission('product')){
									?>
										<li <?php if($page_name=="product"){?> class="active-link" <?php } ?> >
											<a href="<?php echo base_url(); ?>manufacture/product">
												<i class="fa fa-circle fs_i"></i>
													<?php echo translate('all_products');?>
											</a>
										</li>
									<?php
										} if($this->crud_model->manufacture_permission('stock')){
									?>
										<li <?php if($page_name=="stock"){?> class="active-link" <?php } ?> >
											<a href="<?php echo base_url(); ?>manufacture/stock">
												<i class="fa fa-circle fs_i"></i>
													<?php echo translate('product_stock');?>
											</a>
										</li>
									<?php
										}
									?>
								</ul>
							</li>
						<?php
							}
						}
						?>
                        <?php
						if($physical_check !== 'ok' && $digital_check == 'ok'){
							if($this->crud_model->manufacture_permission('digital') ){
                            ?>
                            <!--Menu list item-->
                            <li <?php if($page_name=="digital"){?> class="active-link" <?php } ?> >
                                <a href="<?php echo base_url(); ?>manufacture/digital">
                                    <i class="fa fa-list"></i>
                                        <?php echo translate('products');?>
                                </a>
                            </li>
						<?php
							}
						}
						?>  
						
                        <?php
							if($this->crud_model->manufacture_permission('report')){
						?>
                        <!--Menu list item-->
                        <li <?php if($page_name=="report" || 
                                        $page_name=="report_stock" ||
                                            $page_name=="report_wish" ){?>
                                                     class="active-sub" 
                                                        <?php } ?>>
                            <a href="#">
                                <i class="fa fa-file-text"></i>
                                    <span class="menu-title">
                                		<?php echo translate('reports');?>
                                    </span>
                                		<i class="fa arrow"></i>
                            </a>
                            
                            <!--REPORT-------------------->
                            <ul class="collapse <?php if($page_name=="report" || 
                                                            $page_name=="report_stock" ||
                                                                $page_name=="report_wish" ){?>
                                                                             in
                                                                                <?php } ?> ">
                                <li <?php if($page_name=="report"){?> class="active-link" <?php } ?> >
                                	<a href="<?php echo base_url(); ?>manufacture/report/">
                                    	<i class="fa fa-circle fs_i"></i>
                                            <?php echo translate('product_compare');?>
                                    </a>
                                </li>
                                <?php
                                if($physical_check=='ok'){
								?>
                                <li <?php if($page_name=="report_stock"){?> class="active-link" <?php } ?> >
                                    <a href="<?php echo base_url(); ?>manufacture/report_stock/">
                                    	<i class="fa fa-circle fs_i"></i>
                                        	<?php echo translate('product_stock');?>
                                    </a>
                                </li>
                                <?php
								}
								?>
                                <li <?php if($page_name=="report_wish"){?> class="active-link" <?php } ?> >
                                    <a href="<?php echo base_url(); ?>manufacture/report_wish/">
                                    	<i class="fa fa-circle fs_i"></i>
                                        	<?php echo translate('product_wishes');?>
                                    </a>
                                </li>
                                <li <?php if($page_name=="report_product"){?> class="active-link" <?php } ?> >
                                    <a href="<?php echo base_url(); ?>manufacture/report_product/">
                                    	<i class="fa fa-circle fs_i"></i>
                                        	<?php echo translate('table_product_stock');?>
                                    </a>
                                </li>
                                <li <?php if($page_name=="report_product_seller"){?> class="active-link" <?php } ?> >
                                    <a href="<?php echo base_url(); ?>manufacture/report_product_seller/">
                                    	<i class="fa fa-circle fs_i"></i>
                                        	<?php echo translate('product_seller');?>
                                    </a>
                                </li>
                                <li <?php if($page_name=="report_product_wishlist"){?> class="active-link" <?php } ?> >
                                    <a href="<?php echo base_url(); ?>manufacture/report_product_wishlist/">
                                    	<i class="fa fa-circle fs_i"></i>
                                        	<?php echo translate('table_product_wishlist');?>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php
							}
						?>        
                        <?php
                            if($this->crud_model->manufacture_permission('business_settings')){
                                //if ($this->db->get_where('business_settings',array('type' => 'commission_set'))->row()->value == 'no') {
                        ?>
                        <li <?php if($page_name=="package"){?> class="active-link" <?php } ?> >
                            <a href="<?php echo base_url(); ?>manufacture/package/">
                                <i class="fa fa-gift"></i>
                                <span class="menu-title">
                                    <?php echo translate('upgrade_package');?>
                                </span>
                            </a>
                        </li>
                        <?php
                                //}
                            }
                        ?>

                        <?php
                            if($this->crud_model->manufacture_permission('site_settings')){
                        ?>
                        <!--Menu list item-->
                        <li <?php if($page_name=="site_settings"){?> class="active-link" <?php } ?> >
                            <a href="<?php echo base_url(); ?>manufacture/site_settings/">
                                <i class="fa fa-wrench"></i>
                                    <span class="menu-title">
                                        <?php echo translate('settings');?>
                                    </span>
                            </a>
                        </li>
                        <!--Menu list item-->
                        <?php
                            }
                        ?>
                        
                        <li <?php if($page_name=="manage_manufacture"){?> class="active-link" <?php } ?> >
                            <a href="<?php echo base_url(); ?>manufacture/manage_manufacture/">
                                <i class="fa fa-lock"></i>
                                <span class="menu-title">
                                	<?php echo translate('manage_profile');?>
                                </span>
                            </a>
                        </li>
                </div>
            </div>
        </div>
    </div>
</nav>

<style>
	ul ul ul li a{
		padding-left:80px !important;
	}
	ul ul ul li a:hover{
		background:#2f343b !important;
	}
</style>