 <link rel="stylesheet" href="<?php echo base_url(); ?>template/back/amcharts/style.css"	type="text/css">
 <link rel="stylesheet" href="<?php echo base_url(); ?>template/back/plugins/bootstrap-datepicker/bootstrap-datepicker.css"	type="text/css">
<script src="<?php echo base_url(); ?>template/back/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/back/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/back/amcharts/themes/light.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/back/amcharts/amstock.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/back/plugins/bootstrap-datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<div id="content-container">
	<div id="page-title">
		<h1 class="page-header text-overflow"><?php echo translate('report_product_seller');?></h1>
	</div>
    <div class="tab-base">
        <div class="panel">
            <div class="panel-body">
                <div class="tab-content">
                <!-- LIST -->
                    <div class="tab-pane fade active in" id="" style="border:1px solid #ebebeb; border-radius:4px;">
						<div class="panel-body">
							<div class="row">
							
							
							<div class="col-xs-12">
							    <div class="col-xs-12">
							        <div class="col-sm-2 col-sm-offset-7">
							            <input id="from_date" type="text" class="form-control date-picker" placeholder="Dari Tanggal">
							        </div>
							        <div class="col-sm-2">
							            <input id="to_date" type="text" class="form-control date-picker" placeholder="Sampai Tanggal">
							        </div>
							        <div class="col-sm-1">
							            <button type="button" class="btn btn-default btn-labeled fa fa-filter" onclick="filter_tbl();">Filter</button>
							        </div>
							    </div>
								<div class="col-xs-12" id="tbl-wrapper">
    								<table id="table1" data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" data-show-export="false" class="table table-bordered">
    									<thead>
    												<tr>
    													<th>No.</th>
    													<th>Produk</th>
    													<th>Pabrikan / Seller</th>
    													<th>Kategori</th>
    													<th>Subkategori</th>
    													<th>Jumlah Seller</th>
    												</tr>
    											</thead>
    									<tbody>
    									    
    										<?php 
    										$no = 1;
    										foreach($product_list as $row){ ?>
    											<tr>
    												<td><?php echo $no++;?></td>
    												<td><?php echo $row['title'];?></td>
    												<td>
												    <?php $add_by = json_decode($row['added_by'],true);
												    echo $add_by['type'] == 'manufacture' ? $this->crud_model->get_type_name_by_id('manufacture',$add_by['id'],'name')."(Manufacture)" : $this->crud_model->get_type_name_by_id('vendor',$add_by['id'],'name')."(Seller)";
												    ?>
												</td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name');?></td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('sub_category',$row['sub_category'],'sub_category_name');?></td>
    												
    												    <td>
												    <?php if($row['downlad']=='ok'){ echo "1";
												    }elseif($row['is_bundle'] == 'yes'){
												    echo "1";
												    }else{
												    
												    $this->db->where('manufacture_product_id',$row['manufacture_product_id']);
    												    $this->db->where('product_by_manufacture !=',NULL);
    												    
    												    $total_seller = $this->db->get_where('product')->num_rows();
    												 echo $total_seller; 
												    
												    }?></td>
    											</tr>
    										<?php } ?>
    								
    									</tbody>
    								</table>
    							</div>
							</div>
					
						</div>
						</div>
						
						
                    </div>
                </div>
            </div>
            <!--Panel body-->
        </div>
    </div>
</div>

<div id='export-div'>
		<h1 style="display:none;"><?php echo translate('product_per_seller'); ?></h1>
		<table id="export-table" data-name='product_per_seller' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th>No.</th>
    													<th>Produk</th>
    													<th>Pabrikan / Seller</th>
    													<th>Kategori</th>
    													<th>Subkategori</th>
    													<th>Jumlah Seller</th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					$i = 0;
	            	foreach($product_list as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
    												<td><?php echo $row['title'];?></td>
    												<td>
												    <?php $add_by = json_decode($row['added_by'],true);
												    echo $add_by['type'] == 'manufacture' ? $this->crud_model->get_type_name_by_id('manufacture',$add_by['id'],'name')."(Manufacture)" : $this->crud_model->get_type_name_by_id('vendor',$add_by['id'],'name')."(Seller)";
												    ?>
												</td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name');?></td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('sub_category',$row['sub_category'],'sub_category_name');?></td>
    												<td>
												    <?php if($row['downlad']=='ok'){ echo "1";
												    }elseif($row['is_bundle'] == 'yes'){
												    echo "1";
												    }else{
												    
												    $this->db->where('manufacture_product_id',$row['manufacture_product_id']);
    												    $this->db->where('product_by_manufacture !=',NULL);
    												    
    												    $total_seller = $this->db->get_where('product')->num_rows();
    												 echo $total_seller; 
												    
												    }?></td>
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>


<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'manufacture';
	var module = 'report_product_seller';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
	var loading = '<div>loading...<div>';


    $(document).ready(function() {
        other();
    });

    function other(){
        $('.demo-chosen-select').chosen();
        $('.demo-cs-multiselect').chosen({width:'100%'});
        $('.date-picker').datepicker({
            format: 'dd/mm/yyyy',
        });
    }
   function get_cat(id){
        $('#sub').hide('slow');
		$('#pro').hide('slow');
        ajax_load(base_url+'manufacture/stock/sub_by_cat/'+id,'sub_cat','other');
        $('#sub').show('slow');
        total();
    }
	function get_product(id){
        $('#pro').hide('slow');
        ajax_load(base_url+'manufacture/stock/pro_by_sub/'+id,'product','other');
        $('#pro').show('slow');
        total();
    }

    function get_pro_res(id){

    }
    
     $(document).ready(function(){
        $('#table1').bootstrapTable({});
    });
    
    function filter_tbl()
    {
        $.ajax({
           url: base_url+'manufacture/report_product_seller/filter',
           type: 'get',
           data: {
               from_date: $("#from_date").val(),
               to_date: $("#to_date").val()
           },
           dataType: 'html',
           success: function(res){
               $("#tbl-wrapper").html(res);
               filter_tbl_export();
           },
           error: function(err, jqXHR, errorThrown){
               
           }
        }).done(function(){
            $('#table1').bootstrapTable({});
        }); 
    }
    
    function filter_tbl_export()
    {
        $.ajax({
           url: base_url+'manufacture/report_product_seller/filter_export',
           type: 'get',
           data: {
               from_date: $("#from_date").val(),
               to_date: $("#to_date").val()
           },
           dataType: 'html',
           success: function(res){
               $("#export-div").html(res);
           },
           error: function(err, jqXHR, errorThrown){
               
           }
        }); 
    }
</script>
