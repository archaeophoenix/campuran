<h1 style="display:none;"><?php echo translate('product_per_seller'); ?></h1>
		<table id="export-table" data-name='product_per_seller' data-orientation='p' style="display:none;">
    									<thead>
    												<tr>
    													<th>No.</th>
    													<th>Product</th>
    													<th>Pabrikan / Seller</th>
    													<th>Kategori</th>
    													<th>Subkategori</th>
    													<th>Total Jumlah Seller</th>
    												</tr>
    											</thead>
    									<tbody>
    									    
    										<?php 
    										$no = 1;
    										foreach($product_list as $row){ ?>
    											<tr>
    												<td><?php echo $no++;?></td>
    												<td><?php echo $row['title'];?></td>
    												<td>
												    <?php $add_by = json_decode($row['added_by'],true);
												    echo $add_by['type'] == 'manufacture' ? $this->crud_model->get_type_name_by_id('manufacture',$add_by['id'],'name')."(Manufacture)" : $this->crud_model->get_type_name_by_id('vendor',$add_by['id'],'name')."(Seller)";
												    ?>
												</td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name');?></td>
    												<td><?php echo $this->crud_model->get_type_name_by_id('sub_category',$row['sub_category'],'sub_category_name');?></td>
    												<td>
												    <?php if($row['downlad']=='ok'){ echo "1";
												    }elseif($row['is_bundle'] == 'yes'){
												    echo "1";
												    }else{
												    
												    $this->db->where('manufacture_product_id',$row['manufacture_product_id']);
    												    $this->db->where('product_by_manufacture !=',NULL);
    												    
    												    $total_seller = $this->db->get_where('product')->num_rows();
    												 echo $total_seller; 
												    
												    }?></td>
    											</tr>
    										<?php } ?>
    								
    									</tbody>
    								</table>