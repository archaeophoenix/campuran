
<div id="content-container"> 
    <div id="page-title">
        <h1 class="page-header text-overflow"><?php echo translate('manage_site');?></h1>
    </div>
    <div class="tab-base">
        <div class="panel">
            <div class="tab-base tab-stacked-left">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#demo-stk-lft-tab-2"><?php echo translate('vendor_images');?></a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#demo-stk-lft-tab-4"><?php echo translate('social_media');?></a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#demo-stk-lft-tab-5"><?php echo translate('SEO');?></a>
                    </li>
                </ul>

                <div class="tab-content bg_grey">
                    <span id="genset"></span>
                    <div id="demo-stk-lft-tab-2" class="tab-pane fade active in">
                        <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo translate('select_images');?></h3>
                                </div>
                            <?php
                                echo form_open(base_url() . 'vendor/vendor_images/', array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post',
                                    'id' => '',
                                    'enctype' => 'multipart/form-data'
                                ));
                            ?>
                                <div class="form-group margin-top-10">
                                    <label class="col-sm-3 control-label margin-top-10" for="demo-hor-inputemail"><h5><?php echo translate('logo');?></h5> <br><i>(<?php echo translate('suggested_width');?>:<?php echo translate('height');?> - 300px:300px*)</i></label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-2">
                                            <?php if(file_exists('uploads/vendor_logo_image/logo_'.$this->session->userdata('vendor_id').'.png')){?>
                                            <img class="img-responsive img-md img-border" src="<?php echo base_url(); ?>uploads/vendor_logo_image/logo_<?php echo $this->session->userdata('vendor_id'); ?>.png" id="blah" style="width:auto !important;" >
                                            <?php }else{ ?>
                                            <img class="img-responsive img-md img-border" src="<?php echo base_url(); ?>uploads/vendor_logo_image/default.jpg" id="blah" style="width:auto !important;" >
                                        <?php }?>
                                        </div>
                                        <div class="col-sm-2">
                                        <span class="pull-left btn btn-default btn-file margin-top-10">
                                            <?php echo translate('select_logo');?>
                                            <input type="file" name="logo" class="form-control" id="imgInp">
                                        </span>
                                        </div>
                                        <div class="col-sm-5"></div>
                                    </div>
                                </div><hr>
                                <div class="form-group margin-top-10">
                                    <label class="col-sm-3 control-label margin-top-10" for="demo-hor-inputemail">
                                        <h5><?php echo translate('cover_photo'); ?></h5> 
                                        <br>
                                        <i>(<?php echo translate('suggested_width');?>:<?php echo translate('height');?> - 655px:125px*)</i>
                                    </label>
                                    <div class="col-sm-9">
                                        <div class="col-sm-12">
                                             <?php if(file_exists('uploads/vendor_banner_image/banner_'.$this->session->userdata('vendor_id').'.jpg')){?>
                                            <img class="img-responsive img-lg img-border" src="<?php echo base_url(); ?>uploads/vendor_banner_image/banner_<?php echo $this->session->userdata('vendor_id'); ?>.jpg" id="blahn" style="width:auto !important; height:320px;">
                                             <?php }else{ ?>
                                             <img class="img-responsive img-lg img-border" src="<?php echo base_url(); ?>uploads/vendor_banner_image/default.jpg" id="blahn" style="width:auto !important; height:320px;">
                                             <?php }?>
                                        </div>
                                        <div class="col-sm-6">
                                        <span class="pull-left btn btn-default btn-file margin-top-10">
                                            <?php echo translate('select_photo');?>
                                            <input type="file" name="banner" class="form-control" id="imgInpn">
                                        </span>
                                        </div>
                                        <div class="col-sm-5"></div>
                                    </div>
                                </div>
                                <br />
                                <div class="panel-footer text-right">
                                    <span class="btn btn-success btn-labeled fa fa-check submitter enterer"  data-ing='<?php echo translate('saving'); ?>' data-msg='<?php echo translate('settings_updated!'); ?>'>
                                        <?php echo translate('save');?>
                                    </span>
                                </div>
                            </form> 
                            </div>              
                        </div>
                    </div>
                
                    <!--UPLOAD : SOCIAL LINKS---------->
                    <div id="demo-stk-lft-tab-4" class="tab-pane fade <?php if($tab_name=="social_links") {?>active in<?php } ?>">
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo translate('social_links');?></h3>
                            </div>
							<?php
                                echo form_open(base_url() . 'vendor/social_links/set/', array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post',
                                    'id' => '',
                                    'enctype' => 'multipart/form-data'
                                ));
                            ?>
                                <div class="panel-body">
                                    <!--WEBSITE---------->
                                    <div class="form-group website">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon fb_font">
                                                    <i class="fab fa-internet-explorer fa-lg"></i>
                                                </span>
                                                <input type="text" name="website" placeholder="www.promedis.id" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'website'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!--FACEBOOK---------->
                                    <div class="form-group facebook">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon fb_font">
                                                    <i class="fab fa-facebook-square fa-lg"></i>
                                                </span>
                                                <input type="text" name="facebook" placeholder="www.facebook.com/promedis" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'facebook'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <!--LINE---------->
                                    <div class="form-group line">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon g_font">
                                                    <i class="fab fa-line fa-lg"></i>
                                                </span>
                                                <input type="text" name="google-plus" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'google_plus'); ?>" id="demo-hor-inputemail" class="form-control">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--INSTAGRAM---------->
                                    <div class="form-group instagram">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon g_font">
                                                    <i class="fab fa-instagram fa-lg"></i>
                                                </span>
                                                <input type="text" name="instagram" placeholder="www.instagram.com/promedis" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'instagram'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--TWITTER---------->
                                    <div class="form-group twitter">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon tw_font">
                                                    <i class="fab fa-twitter-square fa-lg"></i>
                                                </span>
                                                <input type="text" name="twitter" placeholder="www.twitter.com/promedis" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'twitter'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--WHATSAPP---------->
                                    <div class="form-group whatsapp">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon wa_font">
                                                    <i class="fab fa-whatsapp fa-lg"></i>
                                                </span>
                                                <input type="text" name="pinterest" placeholder="62812345678 (without +)" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'pinterest'); ?>" id="demo-hor-inputemail" class="form-control num" oninput="num(this);">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--TELEGRAM---------->
                                    <div class="form-group skype">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon skype_font">
                                                    <i class="fab fa-telegram fa-lg"></i>
                                                </span>
                                                <input type="text" name="skype" placeholder="vetbiz" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'skype'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control">
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!--YOUTUBE---------->
                                    <div class="form-group youtube">
                                        <label class="col-sm-3 control-label" ></label>
                                        <div class="col-sm-6">
                                            <div class="input-group mar-btm">
                                                <span class="input-group-addon youtube_font">
                                                    <i class="fab fa-youtube fa-lg"></i>
                                                </span>
                                                <input type="text" name="youtube" value="<?php echo $this->crud_model->get_type_name_by_id('vendor',$this->session->userdata('vendor_id'),'youtube'); ?>" id="demo-hor-inputemail" oninput="form_group(this);" class="t-socmed form-control" placeholder="https://www.youtube.com/channel/channelblabla">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--SAVE---------->
                                <div class="panel-footer text-right">
                                    <span class="btn btn-success btn-labeled fa fa-check submitter enterer"  data-ing='<?php echo translate('saving'); ?>' data-msg='<?php echo translate('settings_updated!'); ?>'>
                                        <?php echo translate('save');?>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- START : MANAGE SEO------>
                    <div id="demo-stk-lft-tab-5" class="tab-pane fade">
                         <div class="col-md-12">
                            <div class="panel">
                                <div class="panel-heading margin-bottom-20">
                                    <h3 class="panel-title">
                                        <?php echo translate('manage_search_engine_optimization');?>
                                    </h3>
                                </div>
                            <?php 
                                $description =  $this->db->get_where('vendor',array('vendor_id' => $this->session->userdata('vendor_id')))->row()->description;
                                $keywords =  $this->db->get_where('vendor',array('vendor_id' => $this->session->userdata('vendor_id')))->row()->keywords;
                            ?>
							<?php
                                echo form_open(base_url() . 'vendor/seo_settings/set', array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post',
                                    'id' => '',
                                    'enctype' => 'multipart/form-data'
                                ));
                            ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="demo-hor-inputemail">
                                        <?php echo translate('keywords'); ?>
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-">
                                            <input type="text"  data-role="tagsinput" name="keywords" value="<?php echo $keywords; ?>" class="form-control" >
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="demo-hor-inputemail">
                                        <?php echo translate('description'); ?>
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-">
                                            <textarea name="description" class="form-control" rows='8' ><?php echo $description; ?></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel-footer text-right">
                                    <span class="btn btn-success btn-labeled fa fa-check submitter enterer"  data-ing='<?php echo translate('saving'); ?>' data-msg='<?php echo translate('settings_updated!'); ?>'>
                                    <?php echo translate('save');?></span>
                                </div>
                            </form>               
                        </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display:none;" id="site"></div>
<!-- for logo settings -->
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#wrap').hide('fast');
                $('#blah').attr('src', e.target.result);
                $('#wrap').show('fast');
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });

    function readURLn(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blahn').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInpn").change(function(){
        readURLn(this);
    });


    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'vendor';
    var module = 'site_settings';
    var list_cont_func = 'show_all';
	
	$(document).ready(function() {
		$("form").submit(function(e){
			return false;
		});

	});
</script>
<script src="<?php echo base_url(); ?>template/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js">
</script>
<script type="text/javascript">

/*$('.num').on('input', (e) =>  {
  $(this).val($(this).val().replace(/[^0-9]/g, ''));
});*/

/*$('.t-socmed').on('input', (e) =>  {
    console.log( $(this).val() );
    // form_group(this);
});*/

function num(id){
   id.value = id.value.replace(/[^0-9]/g, '');
   // $(this).val($(this).val().replace(/[^0-9]/g, '')); 
}

function form_group(id){
    var pattern = {};

    pattern['twitter'] = /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i;
    pattern['facebook'] = /^(https?:\/\/)?((w{3}\.)?)facebook.com\/.*/i;
    pattern['instagram'] = /^\s*(http\:\/\/)?((w{3}\.)?)instagram\.com\/[a-z\d-_]{1,255}\s*$/i;
    // pattern['skype'] = /^[\w\.@]{6,100}$/;
    pattern['youtube'] = /(https?:\/\/)?((w{3}\.)?)youtu((\.be)|(be\..{2,5}))\/((user)|(channel))\//;
    pattern['website'] = /(http(s)?:\/\/.)?((w{3}\.)?)[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g;

    if (id.value.match(pattern[id.name])) {
        $('.' + id.name).attr('class', id.name + ' form-group has-success');
    } else {
        $('.' + id.name).attr('class', id.name + ' form-group has-error');
    }

    if($('.has-error').length > 0){
        $('.enterer').attr('disabled','disabled');
    } else {
        $('.enterer').removeAttr('disabled');
    }
}

/*var str1 = 'http://twitter.com/anypage';
var str2 = 'http://twitter.com/#!/anypage';


var str3 = 'http://facebook2.com/anypage';

var str4 = 'http://www.facebook.com/anypage'; 
var str5 = 'http://facebook.com/anypage';     
var str6 = 'https://facebook.com/anypage';    
var str7 = 'https://www.facebook.com/anypage';

var str8 = 'facebook.com/anypage';            
var str9 = 'www.facebook.com/anypage';        
var str10 = '@anypage';        
var str11 = 'https://www.youtube.com/channel/channelblabla';        
var str12 = 'https://www.youtube.com/c/channelblabla';        
var str13 = 'instagram.com/channelblabla';  */      

function validate_url(url) {
  if (/^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(url)){
     return 'twitter';    
  }

 if (/^(https?:\/\/)?((w{3}\.)?)facebook.com\/.*/i.test(url)){
     return 'facebook';
 }

 /*if (url.match(/^[\w\.@]{6,100}$/)){
   return 'skype';
 }*/

 if (url.match(/(?:(?:http|https):\/\/)?(?:www\.)?(?:instagram\.com|instagr\.am)\/([A-Za-z0-9-_\.]+)/im)) {
    return 'instagram';
 }

 if (url.match(/(https?:\/\/)?(www\.)?youtu((\.be)|(be\..{2,5}))\/((user)|(channel))\//)) {
   return 'youtube';
 }

 if (url.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
    return 'whatsApp';
 }

 return 'unknown';
}



/*var ok = 'anypage'.match(/^[\w\.@]{6,100}$/);
if (ok) {
    console.log(ok);
}

console.log(str1 + ' This link is ' + validate_url(str1));
console.log(str2 + ' This link is ' + validate_url(str2));
console.log(str3 + ' This link is ' + validate_url(str3));
console.log(str4 + ' This link is ' + validate_url(str4));
console.log(str5 + ' This link is ' + validate_url(str5));
console.log(str6 + ' This link is ' + validate_url(str6));
console.log(str7 + ' This link is ' + validate_url(str7));
console.log(str8 + ' This link is ' + validate_url(str8));
console.log(str9 + ' This link is ' + validate_url(str9));
console.log(str10 + ' This link is ' + validate_url(str10));
console.log(str11 + ' This link is ' + validate_url(str11));
console.log(str12 + ' This link is ' + validate_url(str12));
console.log('(123) 456-7890 This link is ' + validate_url('(123) 456-7890'));
console.log('(123)456-7890 This link is ' + validate_url('(123)456-7890'));
console.log('123-456-7890 This link is ' + validate_url('123-456-7890'));
console.log('123.456.7890 This link is ' + validate_url('123.456.7890'));
console.log('1234567890 This link is ' + validate_url('1234567890'));
console.log('+31636363634 This link is ' + validate_url('+31636363634'));
console.log('075-63546725 This link is ' + validate_url('075-63546725'));
console.log(str13 + 'This link is ' + validate_url(str13));*/
</script>