<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
            $deliv = json_decode($sales['delivery_status'], true);

            $form['class'] = 'form-login';
            $form['method'] = 'post';
            $form['id'] = 'product_status_form';
            echo form_open(base_url() . 'home/profile/order_confirm_save/', $form);
        ?>
            <div class="row" style="box-shadow:none;overflow-wrap: break-word; word-wrap: break-word;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div>
                        	<label style="float: left"><?php echo translate('delivery_status');?></label>
                            <label class="checkbox-inline"><input type="checkbox" name="status" value="confirmed_by_user">Konfirmasi Penerimaan Barang</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control selectpicker" name="condition" required="required" data-toggle="tooltip" title="<?php echo translate('availability_status');?>">
                            <option disabled="disabled" selected="selected" hidden="hidden">Kondisi Barang</option>
                        <?php foreach ($condition as $key => $val){ ?>
                            <option value="<?php echo $val; ?>" <?php echo (isset($deliv['condition']) && $val == $deliv['condition']) ? 'selected="selected"' : '' ; ?>><?php echo $val; ?> condition item</option>
                        <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="sale_id" value="<?php echo $sales['sale_id']; ?>">
                        <textarea class="form-control" name="note" style="height:200px;" placeholder="Isi Konfirmasi"><?php echo (isset($deliv['note'])) ? $deliv['note'] : '' ; ?></textarea>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right submit_status_form">
                        <?php echo translate('save');?>
                    </span>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function set_html(hide,show){
        $('#'+show).show('fast');
        $('#'+hide).hide('fast');
    }
    window.addEventListener("keydown", checkKeyPressed, false);
    function checkKeyPressed(e) {
        if (e.keyCode == "13") {
            $('.snbtn').click();
        }
    }
    function set_method(now){
        $('.meth').hide('fast');
        $('.meth').find('select').attr('name','method_1');
        var val = $(now).val();
        if(val !== ''){
            $('.'+val).show('fast');
            $('.'+val).find('select').attr('name','method');
        }
    }
    $(document).ready(function(){        
        $('.selectpicker').selectpicker();

        $(".submit_status_form").click(function(){
        	$("#product_status_form").submit();
        });
    });
</script>