<?php 
$id_gs = [58, 81];
$gs_id = $this->crud_model->read('general_settings', 'WHERE general_settings_id IN (' . implode(',',$id_gs) . ')', 'type, value');

foreach ($gs_id as $key => $val) {
    // create variable by $id_gs value with $gs_id values
    ${'gs_' . $id_gs[$key]} = $val['value'];
}
?>
<div class="thumbnail box-style-1 no-padding">
    <div class="media">
    	<div class="cover"></div>
        <div class="media-link image_delay" data-src="<?php echo $this->crud_model->file_view('product',$product_id,'','','thumb','src','multi','one'); ?>" style="background-image:url('<?php echo img_loading(); ?>');background-size:cover;">
        	<?php
                if($current_stock <= 0 && $download == ''){ 
            ?>
                <div class="sticker red" style="margin-right: 103px; font-size: 10px;">
                    <?php echo translate('out_of_stock'); ?>
                </div>
            <?php
                }
            ?>
            <?php 
                $is_manufacture = json_decode($added_by, true);
                $is_manufacture = $is_manufacture['type'];
                if($discount > 0 && $is_manufacture !== 'manufacture' ){ 
            ?>
                <div class="sticker green" style="font-size: 10px;">
                    <?php echo translate('discount');?> 
    				<?php 
                        if($discount_type =='amount'){
                              echo currency($discount); 
                              } else if($discount_type == 'percent'){
                                   echo $discount; 
                    ?> 
                        % 
                    <?php 
                        }
                    ?>
                </div>
            <?php } ?>
            <div class="quick-view-sm hidden-xs hidden-sm">
                <span onclick="quick_view('<?php echo $this->crud_model->product_link($product_id,'quick'); ?>')">
                    <span class="icon-view" data-toggle="tooltip" data-original-title="<?php  echo translate('quick_view'); ?>">
                        <strong><i class="fa fa-eye"></i></strong>
                    </span>
                </span>
            </div>
        </div>
    </div>
    <div class="caption text-center">
        <h4 class="caption-title">
        	<a title="<?php echo $title; ?>" style="" href="<?php echo $this->crud_model->product_link($product_id); ?>">
				<?php echo $title; ?>
            </a>
        </h4>
        <div class="price" style="height: 36px;">
        <?php if($is_manufacture !== 'manufacture'){?>
            <?php if($discount > 0){ ?> 
                <ins><?php echo currency($this->crud_model->get_product_price2($sale_price, $discount, $discount_type)); ?> </ins> 
                <br><del><?php echo currency($sale_price); ?></del>
            <?php } else { ?>
                <ins><?php echo currency($sale_price); ?></ins> 
                <br><del>&nbsp;</del>
            <?php }?>
        <?php } else {
            /*$brand = $this->db->get_where('product', array('product_id' => $product_id))->row()->brand;
            $category = $this->db->get_where('product', array('product_id' => $product_id))->row()->category;*/
            echo translate('product_by') . ' ' . $name . '<br>';
            echo $category_name;
        } ?>
        </div>
        <div class="price" style="height: 36px;">
        <?php echo ($recipe == 'ok') ? translate('recipe') : '&nbsp;' ; ?>
        </div>
        <div class="price" style="height: 36px;">
        <?php echo (empty($ecatalog)) ? '&nbsp;' : '<a href="' . $ecatalog . '"><img src="' . ECAT . '" style="margin-right:auto;margin-left:auto;max-height:35px;max-width:100px;height:auto;width:auto;"></a>' ; ?>
        </div>
        <?php if ($gs_58 == 'ok' && $gs_81 == 'ok'){ ?>
        <div class="vendor" style="height: 47px;overflow: hidden;">
            <?php echo $this->crud_model->product_by2($added_by,'with_link'); ?>
        </div>
        <?php } ?>
        <div class="button">
            <span class="icon-view left" onclick="do_compare(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
            	data-original-title="<?php if($this->crud_model->is_compared($product_id)=="yes"){ echo translate('compared'); } else { echo translate('compare'); } ?>">
                <strong><i class="fa fa-exchange-alt"></i></strong>
            </span>
            <span class="icon-view middle" onclick="to_wishlist(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
            	data-original-title="<?php if($this->crud_model->is_wished($product_id)=="yes"){ echo translate('added_to_wishlist'); } else { echo translate('add_to_wishlist'); } ?>">
                <strong><i class="fa fa-heart"></i></strong>
            </span>
            <?php if($is_manufacture !== 'manufacture') {?>
            <span class="icon-view right " onclick="to_cart(<?php echo $product_id; ?>,event)" data-toggle="tooltip" 
            	data-original-title="<?php if($this->crud_model->is_added_to_cart($product_id)){ echo translate('added_to_cart'); } else { echo translate('add_to_cart'); } ?>">
                <strong><i class="fa fa-shopping-cart"></i></strong>
            </span>
            <?php } ?>
        </div>
    </div>
</div>