<input type="hidden" value="<?php echo $manufacture_id; ?>" id="manufacture" />
<!-- PAGE WITH SIDEBAR -->
<section class="page-section with-sidebar">
    <div class="container">
        <div class="row">
            <!-- SIDEBAR -->
            <?php 
                include 'sidebar.php';
            ?>
            <!-- /SIDEBAR -->
            <!-- CONTENT -->
            <div class="col-md-9 content" id="content">
                <div id="featured-content">
                </div>
            </div>
            <!-- /CONTENT -->
        </div>
    </div>
</section>
<!-- /PAGE WITH SIDEBAR -->
<script>
	function get_featured_by_manufacture(id){	
		$("#featured-content").load("<?php echo base_url()?>home/manufacture_featured/get_list/"+id);
	}
	$(document).ready(function(){
		var manufacture=$('#manufacture').val();
		get_featured_by_manufacture(manufacture);
    });
</script>
<script>
    function set_search_by_cat(now){
        var cat         = $(now).data('cat');
        var min         = Number($(now).find(':selected').data('min'));
        var max         = Number($(now).find(':selected').data('max'));
        var brands      = $(now).find(':selected').data('brands');
        var subdets     = $(now).find(':selected').data('subdets');
        
        brands = brands.split(';;;;;;');
        var select_brand_options = '';
        for(var i=0, len=brands.length; i < len; i++){
            brand = brands[i].split(':::');
            if(brand.length == 2){      
                select_brand_options = select_brand_options
                                       +'        <option value="'+brand[0]+'" >'+brand[1]+'</option>'
            }
        }
        
        var select_brand_html =  '<select class="selectpicker input-price " name="brand" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" >'
                                +'      <option value="0"><?php echo translate('all_brands'); ?></option>'
                                +       select_brand_options
                                +'</select>';
        $('.search_brands').html(select_brand_html);
        
        
        var select_sub_options = '';
        $.each(subdets, function (i, v) {
            var min = v.min;
            var max = v.max;
            var brands = v.brands;
            var sub_id = v.sub_id;
            var sub_name = v.sub_name;
            select_sub_options = select_sub_options
                                   +'        <option value="'+sub_id+'" data-subcat="'+sub_id+'"  data-min="'+min+'"  data-max="'+max+'" data-brands="'+brands+'" >'+sub_name+'</option>';
        });
        
        var select_sub_html =  '<select class="selectpicker input-price " name="sub_category" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" onchange="set_search_by_scat(this)" >'
                                +'      <option value="0"><?php echo translate('all_sub_categories'); ?></option>'
                                +       select_sub_options
                                +'</select>';
        $('.search_sub').html(select_sub_html);
        $('.selectpicker').selectpicker();
        set_price_slider(min,max,min,max);
    }
    
    
    function set_search_by_scat(now){
        var scat        = $(now).data('subcat');
        var min         = Number($(now).find(':selected').data('min'));
        var max         = Number($(now).find(':selected').data('max'));
        var brands      = $(now).find(':selected').data('brands');                      
        
        brands = brands.split(';;;;;;');
        var select_brand_options = '';
        for(var i=0, len=brands.length; i < len; i++){
            brand = brands[i].split(':::');
            if(brand.length == 2){      
                select_brand_options = select_brand_options
                                       +'        <option value="'+brand[0]+'" >'+brand[1]+'</option>'
            }
        }
        
        var select_brand_html =  '<select class="selectpicker input-price " name="brand" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" >'
                                +'      <option value="0"><?php echo translate('all_brands'); ?></option>'
                                +       select_brand_options
                                +'</select>';
        $('.search_brands').html(select_brand_html);
        
        $('.selectpicker').selectpicker();
        set_price_slider(min,max,min,max);
        
    }
    
    function set_price_slider(min,max,univ_min,univ_max){ 
        var priceSliderRange = $('#slider-range');
        if ($.ui) {
            /**/
            if ($(priceSliderRange).length) {
                $(priceSliderRange).slider({
                    range: true,
                    min: univ_min,
                    max: univ_max,
                    values: [min, max],
                    slide: function (event, ui){
                        $("#amount").val(currency + (Number(ui.values[0])*exchange) + " - " + currency + (Number(ui.values[1])*exchange));
                        $("#rangeaa").val(ui.values[0] + ";" + ui.values[1]);
                    },
                    stop: function( event, ui ) {
                        do_product_search();
                    }
                });
                $("#amount").val(
                    currency + Number($("#slider-range").slider("values", 0))*exchange + " - " + currency + Number($("#slider-range").slider("values", 1))*exchange
                );
                $("#rangeaa").val($("#slider-range").slider("values", 0) + ";" + $("#slider-range").slider("values", 1));
            }
            
        }
    }
    
    $(document).ready(function(e) {
        var univ_max = $('#univ_max').val(); 
        set_price_slider(0,univ_max,0,univ_max);
        setTimeout(function(){ $('.selectpicker').selectpicker(); }, 3000);
    });
</script>