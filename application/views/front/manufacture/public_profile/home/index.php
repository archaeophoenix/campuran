<input type="hidden" id="manufacture_id" value="<?php echo $manufacture_id ?>"/>

<section class="page-section">
    <div class="container">
    	<div class="row">
            <div class="col-md-12">
                <div id="slider">
                
                </div>
            </div>
       	</div>             
    </div>
</section>
<section class="page-section brands">
 <div class="container">
        <h2 class="section-title">
            <span><?php echo translate('about').' '.$this->crud_model->get_type_name_by_id('manufacture',$manufacture_id,'display_name');?></span>
        </h2>
        <div class="partners-carousel">
             <p>
                 <?php echo $this->crud_model->get_type_name_by_id('manufacture',$manufacture_id,'details');?>
             </p>
             
        </div>
    </div>
</section>
<!-- PAGE WITH SIDEBAR -->
<section class="page-section">
	<div class="container">
		<div class="row">
			<!-- CONTENT -->
			<div class="content" id="content">
				<?php 
					include 'featured_product.php';
				?>
				<!-- /shop-sorting -->
				<?php include 'category.php'; ?>
				<?php include 'brands.php'; ?>
			</div>
			<!-- /CONTENT -->
		</div>
	</div>
</section>
<!-- /PAGE WITH SIDEBAR -->
<script>
$(document).ready(function(){
	get_slider();
});
function get_slider(){
	var id = $('#manufacture_id').val();
	$('#slider').load(
		"<?php echo base_url()?>home/manufacture_profile/get_slider/"+id,
		function(){
			var mainSliderSizeNew = $('#main-slider').find('.item').size();
            $('#main-slider').owlCarousel({
					//items: 1,
					autoplay: true,
					autoplayHoverPause: false,
					loop: mainSliderSizeNew > 1 ? true : false,
					margin: 0,
					dots: true,
					nav: true,
					navText: [
						"<i class='fa fa-angle-left'></i>",
						"<i class='fa fa-angle-right'></i>"
					],
					responsiveRefreshRate: 100,
					responsive: {
						0: {items: 1},
						479: {items: 1},
						768: {items: 1},
						991: {items: 1},
						1024: {items: 1}
					}
            });
		}
	);
}
</script>
<style>
	.social-icons a.facebook:hover {
	    background-color: #3b5998 !important;
	    color: #ffffff;
	}
	.social-icons a.twitter:hover {
	    background-color: #1da1f2 !important;
	    color: #ffffff;
	}
	.social-icons a.google:hover {
	    background-color: #dd4c40 !important;
	    color: #ffffff;
	}
	.social-icons a.pinterest:hover {
	    background-color: #bd081c !important;
	    color: #ffffff;
	}
	.social-icons a.youtube:hover {
	    background-color: #ff0000 !important;
	    color: #ffffff;
	}
	.social-icons a.skype:hover {
	    background-color: #1686D9 !important;
	    color: #ffffff;
	}
</style>
<script>
    function set_search_by_cat(now){
        var cat         = $(now).data('cat');
        var min         = Number($(now).find(':selected').data('min'));
        var max         = Number($(now).find(':selected').data('max'));
        var brands      = $(now).find(':selected').data('brands');
        var subdets     = $(now).find(':selected').data('subdets');
        
        brands = brands.split(';;;;;;');
        var select_brand_options = '';
        for(var i=0, len=brands.length; i < len; i++){
            brand = brands[i].split(':::');
            if(brand.length == 2){      
                select_brand_options = select_brand_options
                                       +'        <option value="'+brand[0]+'" >'+brand[1]+'</option>'
            }
        }
        
        var select_brand_html =  '<select class="selectpicker input-price " name="brand" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" >'
                                +'      <option value="0"><?php echo translate('all_brands'); ?></option>'
                                +       select_brand_options
                                +'</select>';
        $('.search_brands').html(select_brand_html);
        
        
        var select_sub_options = '';
        $.each(subdets, function (i, v) {
            var min = v.min;
            var max = v.max;
            var brands = v.brands;
            var sub_id = v.sub_id;
            var sub_name = v.sub_name;
            select_sub_options = select_sub_options
                                   +'        <option value="'+sub_id+'" data-subcat="'+sub_id+'"  data-min="'+min+'"  data-max="'+max+'" data-brands="'+brands+'" >'+sub_name+'</option>';
        });
        
        var select_sub_html =  '<select class="selectpicker input-price " name="sub_category" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" onchange="set_search_by_scat(this)" >'
                                +'      <option value="0"><?php echo translate('all_sub_categories'); ?></option>'
                                +       select_sub_options
                                +'</select>';
        $('.search_sub').html(select_sub_html);
        $('.selectpicker').selectpicker();
        set_price_slider(min,max,min,max);
    }
    
    
    function set_search_by_scat(now){
        var scat    = $(now).data('subcat');
        var min     = Number($(now).find(':selected').data('min'));
        var max     = Number($(now).find(':selected').data('max'));
        var brands    = $(now).find(':selected').data('brands');            
        
        brands = brands.split(';;;;;;');
        var select_brand_options = '';
        for(var i=0, len=brands.length; i < len; i++){
            brand = brands[i].split(':::');
            if(brand.length == 2){    
                select_brand_options = select_brand_options
                                       +'        <option value="'+brand[0]+'" >'+brand[1]+'</option>'
            }
        }
        
        var select_brand_html =  '<select class="selectpicker input-price " name="brand" data-live-search="true" '
                                +'  data-width="100%" data-toggle="tooltip" title="Select" >'
                                +'    <option value="0"><?php echo translate('all_brands'); ?></option>'
                                +   select_brand_options
                                +'</select>';
        $('.search_brands').html(select_brand_html);
        
        $('.selectpicker').selectpicker();
        set_price_slider(min,max,min,max);
        
    }
    
    function set_price_slider(min,max,univ_min,univ_max){ 
        var priceSliderRange = $('#slider-range');
        if ($.ui) {
            /**/
            if ($(priceSliderRange).length) {
                $(priceSliderRange).slider({
                    range: true,
                    min: univ_min,
                    max: univ_max,
                    values: [min, max],
                    slide: function (event, ui){
                        $("#amount").val(currency + (Number(ui.values[0])*exchange) + " - " + currency + (Number(ui.values[1])*exchange));
                        $("#rangeaa").val(ui.values[0] + ";" + ui.values[1]);
                    },
                    stop: function( event, ui ) {
                        do_product_search();
                    }
                });
                $("#amount").val(
                    currency + Number($("#slider-range").slider("values", 0))*exchange + " - " + currency + Number($("#slider-range").slider("values", 1))*exchange
                );
                $("#rangeaa").val($("#slider-range").slider("values", 0) + ";" + $("#slider-range").slider("values", 1));
            }
            
        }
    }
    
    $(document).ready(function(e) {
        var univ_max = $('#univ_max').val(); 
        set_price_slider(0,univ_max,0,univ_max);
        setTimeout(function(){ $('.selectpicker').selectpicker(); }, 3000);
    });
</script>