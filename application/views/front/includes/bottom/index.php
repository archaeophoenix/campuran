    		<script>
            var base_url = "<?php echo base_url(); ?>";
        </script>
        <script src="<?php echo base_url(); ?>template/front/js/ajax_method.js"></script>
        <script src="<?php echo base_url(); ?>template/front/js/bootstrap-notify.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/js/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>
        <!-- JS Global -->
        <script src="<?php echo base_url(); ?>template/front/plugins/superfish/js/superfish.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/jquery.sticky.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/jquery.easing.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/jquery.smoothscroll.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/smooth-scrollbar.min.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/jquery.cookie.js"></script>
        
        <script src="<?php echo base_url(); ?>template/front/plugins/modernizr.custom.js"></script>
        <script src="<?php echo base_url(); ?>template/front/modal/js/jquery.active-modals.js"></script>
        <script src="<?php echo base_url(); ?>template/front/js/theme.js"></script>
        <script src="<?php echo base_url(); ?>template/front/plugins/owl-carousel2/owl.carousel.min.js"></script>
        <script type="text/javascript">
            $(function () {
              $(".subdistrict").autocomplete({
                source:base_url + 'home/subdistrict',
                minLength:3,
                select: function (event, ui) {
                  $('#subdistrict').val(ui.item.label);
                  $('#subdistrict_id').val(ui.item.subdistrict_id);
                  $('#city_id').val(ui.item.city_id);
                  $('#province_id').val(ui.item.province_id);
                  $('#subdistrict_name').val(ui.item.subdistrict_name);
                  $('#city_name').val(ui.item.city_name);
                  $('#province').val(ui.item.province);
                }
              });

              $('#files').change(() => {
                let selif = document.querySelector('#files').files;
                $('#selif').text(selif.length + ' File')
              });
            });
        </script>
        
        <?php
            if (file_exists($asset_page.'.php')) {
              include $asset_page.'.php';
            }
		    ?>


        <form id="cart_form_singl">
                <input type="hidden" name="color" value="">
                <input type="hidden" name="qty" value="1">
        </form>
        <?php require 'shopping_cart.php'; ?>