<!-- PAGE -->
<link rel="stylesheet" href="<?php echo base_url(); ?>template/front/js/share/jquery.share.css">
<script src="<?php echo base_url(); ?>template/front/js/share/jquery.share.js"></script>

<?php  
	foreach($product_details as $row){
	$thumbs = $this->crud_model->file_view('product',$row['product_id'],'','','thumb','src','multi','all');
	$mains = $this->crud_model->file_view('product',$row['product_id'],'','','no','src','multi','all'); 
?>
<?php 
$is_manufacture = $this->db->get_where('product', array('product_id' => $row['product_id']))->row()->added_by;
$is_manufacture = json_decode($is_manufacture, true);
$is_manufacture = $is_manufacture['type'];
?>
<section class="page-section">
    <div class="row product-single" style="margin-top: 0px;">
        <div class="col-md-5 hidden-xs hidden-sm">
            <div class="row">
                <div class="col-md-2 others-img">
                    <?php
                        $i=1;
                        foreach ($thumbs as $id=>$row1) {
                    ?>
                    <div class="related-product" id="main<?php echo $i; ?>">
                        <img class="img-responsive img" data-src="<?php echo $thumbs[$id]; ?>" src="<?php echo $row1; ?>" alt=""/>
                    </div>
                    <?php
                        $i++;
                        }
                    ?>
                </div>
                <div class="col-md-10">
                    <img class="img-responsive main-img" id="set_image" src="" alt=""/>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <h3 class="product-title"><?php echo $row['title'];?></h3>
            <?php
            if ($this->db->get_where('product', array('product_id' => $row['product_id']))->row()->is_bundle == 'no') {
            ?>
                <div class="product-info">
                    <p>
                        <a href="<?php echo base_url(); ?>home/category/<?php echo $row['category']; ?>">
                            <?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name');?>
                        </a>
                    </p>
                    ||
                    <p>
                        <a href="<?php echo base_url(); ?>home/category/<?php echo $row['category']; ?>/<?php echo $row['sub_category']; ?>">
                            <?php echo $this->crud_model->get_type_name_by_id('sub_category',$row['sub_category'],'sub_category_name');?>
                        </a>
                    </p>
                    ||
                    <p>
                        <a href="<?php echo base_url(); ?>home/category/<?php echo $row['category']; ?>/<?php echo $row['sub_category']; ?>-<?php echo $row['brand']; ?>">
                        <?php echo $this->crud_model->get_type_name_by_id('brand',$row['brand'],'name');?>
                        </a>
                    </p>
                </div>
            <?php
            }
            ?>
            <?php
            if ($this->db->get_where('product', array('product_id' => $row['product_id']))->row()->is_bundle == 'yes') {
            ?>
            <div style="padding: 5px">
                <?php echo translate('products_package_included:');?> <br>
                <?php
                    $products = json_decode($row['products'], true);
                    foreach ($products as $product) { ?>
                        <!-- echo $product['product_id'].'<br>'; -->
                        <a style="text-decoration:underline; font-size: 12px; padding-left: 15px;" href="<?php echo $this->crud_model->product_link($product['product_id']); ?>">
                            <?php echo $this->db->get_where('product', array('product_id' => $product['product_id']))->row()->title . '<br>';?>
                        </a>
                <?php
                    }
                ?>
            </div>
            <?php
            }
            ?>
            <?php if ($this->db->get_where('general_settings', array('general_settings_id' => '58'))->row()->value == 'ok'and $this->db->get_where('general_settings', array('general_settings_id' => '81'))->row()->value == 'ok'){ ?>
            <div class="added_by">
                <?php echo translate('sold_by_:');?>
                <p>
                    <?php echo $this->crud_model->product_by($row['product_id'],'with_link');?>
                </p>
            </div>
            <?php } ?>
            <div class="product-rating clearfix">
                <div class="rating ratings_show" data-original-title="<?php echo $rating = $this->crud_model->rating($row['product_id']); ?>"	
                    data-toggle="tooltip" data-placement="left">
                    <?php
                        $r = $rating;
                        $i = 6;
                        while($i>1){
                            $i--;
                    ?>
                        <span class="star <?php if($i<=$rating){ echo 'active'; } $r++; ?>"></span>
                    <?php
                        }
                    ?>
                </div>
                
                <div class="rating inp_rev list-inline" style="display:none;" data-pid='<?php echo $row['product_id']; ?>'>
                    <span class="star rate_it" id="rating_5" data-rate="5"></span>
                    <span class="star rate_it" id="rating_4" data-rate="4"></span>
                    <span class="star rate_it" id="rating_3" data-rate="3"></span>
                    <span class="star rate_it" id="rating_2" data-rate="2"></span>
                    <span class="star rate_it" id="rating_1" data-rate="1"></span>
                </div>
                <a class="reviews ratings_show" href="#">
                    <?php echo $row['rating_num']; ?>
                    <?php echo translate('review(s)'); ?> 
                </a>  
                <?php  
                    if($this->session->userdata('user_login') == 'yes'){
                        $user_id = $this->session->userdata('user_id');
                        $user_products = $this->db->select('product_details')->from('sale')->where('buyer', $user_id)->get()->result_array();

                            foreach ($user_products as $user_prod) {
                                foreach($user_prod as $prods){
                                    $prods = json_decode($prods, true);
                                    foreach($prods as $prod){
                                        //echo $prod['id'];
                                        if($prod['id'] == $row['product_id']){
                                            //echo $prod['id'];
                                            $is_review = 'yes';
                                        }
                                    }
                                }
                            }
                        $rating_user = json_decode($row['rating_user'],true);
                        if(!in_array($user_id,$rating_user)){
                            if ($is_review == 'yes') {
                ?>
                <a class="add-review rev_show ratings_show" href="#">
                    | <?php echo translate('add_your_review');?>
                </a>
                <?php 
                            }
                        }
                    }
                ?>
            </div>
            <hr class="page-divider"/>
            <?php if($is_manufacture !== 'manufacture'){?>
            <div class="product-price">
                <?php echo translate('price_:');?>
                <?php if($row['discount'] > 0){ ?> 
                    <ins>
                        <?php echo currency($this->crud_model->get_product_price($row['product_id'])); ?>
                        <unit><?php echo ' /'.$row['unit'];?></unit>
                    </ins> 
                    <del><?php echo currency($row['sale_price']); ?></del>
                    <span class="label label-success">
                    <?php 
                        echo translate('discount:_').$row['discount'];
                        if($row['discount_type']=='percent'){
                            echo '%';
                        }
                        else{
                            echo currency();
                        }
                    ?>
                    </span>
                <?php } else { ?>
                    <ins>
                        <?php echo currency($row['sale_price']); ?>
                        <unit><?php echo ' / '.$row['unit'];?>  </unit>
                    </ins> 
            </div>
            <?php }?>
                <?php } ?>
            <?php
                include 'order_option.php';?>
            <h4>
                <a style="text-decoration:underline;" href="<?php echo $this->crud_model->product_link($row['product_id']); ?>">
                    <?php echo translate('view_details');?>
                </a>
            </h4>
            <div class="row">
                    <?php if (!empty($row['ecatalog'])) {?>
                    <div class="col-md-12 text-left">
                    <span class="text-left">
                        <?php echo '<a href="' . $row['ecatalog'] . '"><img src="' . ECAT . '" style="margin-right:auto;margin-left:auto;max-height: 50px;max-width: 135px;height:auto;width:auto;"></a>'; ?>
                    </span>
                    </div>
                     <?php } ?>
                    <?php if ($row['recipe'] == 'ok') {?>
                    <div class="col-md-5 buttons">
                        <span class=" btn btn-add-to wishlist">
                            <i class="fa fa-file-text-o"></i>
                            <span><?php echo translate('medic_recipe_front'); ?></span>
                        </span>
                    </div>
                    <?php } ?>

                    <?php if (!empty($row['demo'])) {?>
                    <div class="col-md-6 buttons">
                        <span class="btn btn-add-to compare btn_compare">
                            <i class="fa fa-play"></i>
                            <span><a target="_blank" href="<?php echo $row['demo']; ?>" title="Preview"><?php echo translate('detail_information'); ?></a></span>
                        </span>
                    </div>
                    <?php } ?>
                    <div class="col-md-12 buttons">
                        <span class=" btn btn-add-to wishlist">
                            <i class="fa fa-file-text-o"></i>
                            <span><?php echo translate('provider_information'); ?></span>
                        </span>
                        </br>
                        </br>
                        <div class="text-left">
                            <?php if($add_by[0]['website'] != ''){ ?>
                              <a href="https://<?php echo $add_by[0]['website'];?>" title="Website" target="_blank" class="btn btn-default" style="margin: 1px; display: inline-block;"><i class="fab fa-internet-explorer fa-lg"></i></a>
                            <?php } if($add_by[0]['twitter'] != ''){ ?>
                              <a href="https://<?php echo $add_by[0]['twitter'];?>" title="Twitter" target="_blank" class="btn btn-info" style="margin: 1px; display: inline-block;"><i class="fab fa-twitter"></i></a>
                            <?php } if($add_by[0]['youtube'] != ''){ ?>
                              <a href="https://<?php echo $add_by[0]['youtube'];?>" title="Youtube" target="_blank" class="btn btn-danger" style="margin: 1px; display: inline-block;"><i class="fab fa-youtube"></i></a>
                            <?php } if($add_by[0]['facebook'] != ''){ ?>
                              <a href="https://<?php echo $add_by[0]['facebook'];?>" title="Facebook" target="_blank" class="btn btn-primary" style="margin: 1px; display: inline-block;"><i class="fab fa-facebook"></i></a>
                            <?php } if($add_by[0]['instagram'] != ''){ ?>
                              <a href="https://<?php echo $add_by[0]['instagram'];?>" title="Instagram" target="_blank" class="btn btn-danger" style="margin: 1px; display: inline-block;"><i class="fab fa-instagram"></i></a>  
                            <?php } if($add_by[0]['pinterest'] != ''){ ?>
                              <a href="https://wa.me/<?php echo $add_by[0]['pinterest'];?>" title="Whatsapp" target="_blank" class="btn btn-success" style="margin: 1px; display: inline-block;"><i class="fab fa-whatsapp"></i></a>
                            <?php } if($add_by[0]['skype'] != ''){ ?>
                              <a href="https://t.me/<?php echo $add_by[0]['skype'];?>" title="Telegram" target="_blank" class="btn btn-primary" style="margin: 1px; display: inline-block;"><i class="fab fa-telegram"></i></a>
                            <?php } if($add_by[0]['google_plus'] != ''){ ?><a href="https://line.me/ti/p/~<?php echo $add_by[0]['google_plus'];?>" title="" target="_blank" class="btn btn-success" style="margin: 1px; display: inline-block;"><i class="fab fa-line"></i></a>
                            <?php } ?><a href="mailto:<?php echo $add_by[0]['email']; ?>" title="Email" class="btn btn-default" style="margin: 1px; display: inline-block;"><i class="fa fa-envelope"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
	}
?>

<!-- /PAGE -->
                
<script>
	$(".img").click(function(){
		var src = $(this).data('src');
		$("#set_image").attr("src", src);
		$(".related-product").removeClass("selected");
		$(this).closest(".related-product").addClass("selected");
	});
	$(document).ready(function() {
		$("#main1").addClass("selected");
		var src=$("#main1").find(".img").data('src');
		$("#set_image").attr("src", src);
	});
	
	$(function(){
		if($('#main1').length > 0){
			$('#main1').click();
		}
	});
</script>
<script>
	$('body').on('click', '.rev_show', function(){
		$('.ratings_show').hide('fast');
		$('.inp_rev').show('slow');
	});
</script>
<style>
	.rate_it{
		display:none;	
	}
	.product-single .fix-length{
		height:225px; 
		overflow:auto;
	}
</style>