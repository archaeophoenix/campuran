<?php
    $discus_id = $this->db->get_where('general_settings',array('type'=>'discus_id'))->row()->value;
    $fb_id = $this->db->get_where('general_settings',array('type'=>'fb_comment_api'))->row()->value;
    $comment_type = $this->db->get_where('general_settings',array('type'=>'comment_type'))->row()->value;
    $images_thumbs = $this->crud_model->file_view('product',$row['product_id'],'','','thumb','src','multi','one');
?>
<!-- PAGE -->
<section class="page-section specification">
    <div class="container">
        <div class="tabs-wrapper content-tabs">
            <ul class="nav nav-tabs">
                <li  class="active"  ><a href="#tab1" data-toggle="tab"><?php echo translate('full_description'); ?></a></li>
                <?php if($row['is_bundle'] == 'no'){ ?>
                <li ><a href="#tab2" data-toggle="tab"><?php echo translate('additional_specification'); ?></a></li>
                <?php } ?>
                <li ><a href="#tab3" data-toggle="tab"><?php echo translate('requirements'); ?></a></li>
                <li ><a href="#tab4" data-toggle="tab"><?php echo translate('reviews'); ?></a></li>
                <?php if($row['is_bundle'] == 'no' && $is_manufacture == 'vendor'){ ?>
                <li ><a href="#tab5" data-toggle="tab"><?php echo translate('product_by_near_location');?></a></li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="tab1">
                <?php 
                echo $row['description'] . '<br><br>';
                    echo $this->db->get_where('business_settings',array('type'=>'shipment_info'))->row()->value;
                ?>
                </div>
                <div class="tab-pane fade" id="tab2">
                    <div class="panel panel-sea margin-bottom-40">
                    <?php 
                        $a = $this->crud_model->get_additional_fields($row['product_id']);
                        if(count($a)>0){
                    ?>
                        <table class="table table-bordered">
                            <tbody>
                            <?php
                                foreach ($a as $val) {
                            ?>
                                <tr>
                                    <td style="text-align:left;"><?php echo $val['name']; ?></td>
                                    <td style="text-align:left;"><?php echo $val['value']; ?></td>
                                </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    <?php 
                        }
                    ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab3">
                    <?php
                    if(!empty($row['requirements'])){
                        $req = json_decode($row['requirements'],true);
                        if(count($req)>0){
                    ?>
                        <table class="table table-bordered">
                            <tbody>
                            <?php
                                foreach ($req as $roww) {
                            ?>
                                <tr>
                                    <td style="text-align:left;"><?php echo $roww['field']; ?></td>
                                    <td style="text-align:left;"><?php echo $roww['desc']; ?></td>
                                </tr>
                            <?php
                                }
                            ?>
                            </tbody>
                        </table>
                    <?php 
                        }
                    }
                    ?>
                </div>
                <div class="tab-pane fade" id="tab4">
					<?php if($comment_type == 'disqus'){ ?>
                    <div id="disqus_thread"></div>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES * * */
                        var disqus_shortname = '<?php echo $discus_id; ?>';
                        
                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function() {
                            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                        })();
                    </script>
                    <script type="text/javascript">
                        /* * * CONFIGURATION VARIABLES * * */
                            var disqus_shortname = '<?php echo $discus_id; ?>';
                        
                        /* * * DON'T EDIT BELOW THIS LINE * * */
                        (function () {
                            var s = document.createElement('script'); s.async = true;
                            s.type = 'text/javascript';
                            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
                            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
                        }());
                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                    <?php
                        }
                        else if($comment_type == 'facebook'){
                    ?>

                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=<?php echo $fb_id; ?>";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="<?php echo $this->crud_model->product_link($row['product_id']); ?>" data-numposts="5"></div>

                    <?php
                        }
                    ?>
                </div>
                <div class="tab-pane fade" id="tab5">
                    <div class="row"></div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="city_field" id="user_province" onChange="get_city(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                                    <option value=""><?php echo translate('chose_a_province')?></option>
                                    <?php echo $this->crud_model->select_rajaongkir_area('province');?>
                                </select>
                            </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="city_field" id="user_city" onChange="get_subdistrict(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                                    <option value=""><?php echo translate('chose_a_city')?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" >
                                <select class="selectpicker required" id="user_subdistrict" name="subdistrict_field" onChange="get_courier(this.value,this); get_near_product(this.value,this)" tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip">
                                    <option value=""><?php echo translate('chose_a_subdistrict')?></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="user_courier">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="products-search-result">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- /PAGE -->

<script type="text/javascript">
    function get_city(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_city/'+id, 'user_city');
    }

    function get_subdistrict(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_subdistrict/'+id, 'user_subdistrict');
    }

    function get_courier(id){
        var qty = ($("#qty").val()) ? $("#qty").val() : 1 ;
        get_rajaonkir(base_url+'home/rajaongkir/get_couriers/'+id+'/'+qty+'/'+<?php echo $row['product_id']; ?>, 'user_courier');
    }

    function get_rajaonkir(url,id){
        $.ajax({
            url: url,
            cache: false,
            success: function(response){
                document.getElementById(id).innerHTML = response
                $('.selectpicker').selectpicker('refresh');
            }
        })
    }

    function get_near_product(id){
        $.ajax({
            url: base_url + 'home/rajaongkir/get_cheapest',
            type: 'POST',
            data: {subdistrict : id, manufacture_product_id: <?php echo $row['manufacture_product_id'];?>},
            cache: false,
            dataType: 'json',
            success: function(response){
                var result = ''
                $.each(response, function(key, value){
                    result += '<div class="col-md-3 col-sm-6 col-xs-6">'
                    result += '<div class="recommend_box_1">'
                    result += '<a class="link" href="' + value.link +'">'
                    result += '<div class="image-box" style="background-size:cover; background-position:center;"></div>'
                    result += '<h4 class="caption-title " style="height: 41px; overflow: hidden;" title="'+value.title+'"><b>'+ value.title +'</b></h4>'
                    result += '<h4 class="caption-title " style="height: 41px; overflow: hidden;" title="'+value.city+'"><b>('+value.city+')</b></h4>'
                    result += '<div class="price">'+ value.sales +'</div>'
                    result += '<div class="price"><ins><?php echo translate('shipping_price')?> '+ value.shipping+'</ins></div>'
                    result += '</a></div></div>'
                })
                $('#products-search-result').html(result)

                $('.image-box').css({
                    'background-image': 'url(<?php echo $images_thumbs ?>)'
                })
                
            }
        })
    }
</script>
<style>
@media(max-width: 768px) {
	.specification .nav-tabs>li{
		float: none;
		display: block;
		text-align: center;
	}
}
</style>