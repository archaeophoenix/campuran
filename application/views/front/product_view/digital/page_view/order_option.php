
<?php
	echo form_open('', array(
		'method' => 'post',
		'class' => 'sky-form',
	));
?>
    <div class="buttons" style="display:inline-flex;">
        <span class="btn btn-add-to cart" onclick="to_cart(<?php echo $row['product_id']; ?>,event)">
            <i class="fa fa-shopping-cart"></i>
            <?php if($this->crud_model->is_added_to_cart($row['product_id'])=="yes"){ 
                echo translate('added_to_cart');  
                } else { 
                echo translate('add_to_cart');  
                } 
            ?>
        </span>
        <?php 
            $wish = $this->crud_model->is_wished($row['product_id']); 
        ?>
        <span class="btn btn-add-to <?php if($wish == 'yes'){ echo 'wished';} else{ echo 'wishlist';} ?>" onclick="to_wishlist(<?php echo $row['product_id']; ?>,event)">
            <i class="fa fa-heart"></i>
            <span class="hidden-sm hidden-xs">
				<?php if($wish == 'yes'){ 
                    echo translate('_added_to_wishlist'); 
                    } else { 
                    echo translate('_add_to_wishlist');
                    } 
                ?>
            </span>
        </span>
        <?php 
            $compare = $this->crud_model->is_compared($row['product_id']); 
        ?>
        <span class="btn btn-add-to compare btn_compare"  onclick="do_compare(<?php echo $row['product_id']; ?>,event)">
            <i class="fa fa-exchange-alt"></i>
            <span class="hidden-sm hidden-xs">
				<?php if($compare == 'yes'){ 
                    echo translate('_compared'); 
                    } else { 
                    echo translate('_compare');
                    } 
                ?>
            </span>
        </span>
    </div>
    <div class="buttons">
        <!--Share
        <div id="share">
          <a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo base_url('home/product_view') . "/".$row['product_id'] . "/".$row['title'] ?>" title="Share On Facebook" class="pop share-square share-square-facebook" style="display: inline-block;"></a>
          <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo base_url('home/product_view') . "/".$row['product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Share On Twitter" class="pop share-square share-square-twitter" style="display: inline-block;"></a>
          <a target="_blank" href="https://t.me/share/url?url=<?php echo base_url('home/product_view') . "/".$row['product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Chat Seller On Telegram" class="pop share-square share-square-telegram" style="display: inline-block;"></a>
          <a target="_blank" href="https://api.whatsapp.com/send?phone=&text=<?php echo $row['title'];?>%20<?php echo base_url('home/product_view') . "/".$row['product_id'] . "/".$row['title'] ?>" title="Chat Seller On WhatsApp" class="pop share-square share-square-whatsapp" style="display: inline-block;"></a>
          <a target="_blank" href="https://lineit.line.me/share/ui?url=<?php echo base_url('home/product_view') . "/".$row['product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Chat Seller On Line" class="pop share-square share-square-line" style="display: inline-block;"></a>
        </div>-->
    </div>
    <hr class="page-divider small">
    <input type="hidden" name="qty" value="1" />
</form>
<div id="pnopoi"></div>
<script>
	/*$(document).ready(function() {
		$('#share').share({
			networks: ['facebook','whatsapp','telegram','line','twitter'],
			theme: 'square'
		});
	});*/
</script>
<style>
.t-row{
	display:table-row;
}
</style>