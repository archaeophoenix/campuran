
<div class="col-md-8">
    <table class="table table-bordered carter_table" style="background: #fff;">
        <thead>
            <tr>
                <th width="15%" class="hidden-sm hidden-xs"><?php echo translate('image');?></th>
                <th width="10%"><?php echo translate('seller');?></th>
                <th width="25%"><?php echo translate('product_details');?></th>
                <th width="15%"><?php echo translate('unit_price');?></th>
                <th width="15%" style="text-align:center;"><?php echo translate('quantity');?></th>
                <th width="15%"><?php echo translate('subtotal');?></th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $carted = $this->cart->contents(); 
                foreach ($carted as $key => $items){ 
            ?>
            <tr data-rowid="<?php echo $items['rowid']; ?>" >
                <td class="image hidden-sm hidden-xs" align="center">
                    <a class="media-link" href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                        <i class="fa fa-plus"></i>
                        <img src="<?php echo $items['image']; ?>" width="60" alt=""/>
                    </a>
                </td>
                <td class="description">
                    <?php echo $items['seller']; ?>
                </td>
                <td class="description">
                    <h4 style="">
                        <a href="<?php echo $this->crud_model->product_link($items['id']); ?>">
                            <?php echo $items['name']; ?>
                        </a>
                    </h4>
                    <hr class="mr0">
                    <?php 
                        $color = $this->crud_model->is_added_to_cart($items['id'],'option','color'); 
                        if($color){ 
                    ?>
                   <div style="background:<?php echo $color; ?>; height:15px; width:15px;border-radius:50%;"></div>
                    <hr class="mr0">
                    <?php
                        }
                    ?>
                    
                    <?php
                        $all_o = json_decode($items['option'],true);
                        foreach ($all_o as $l => $op) { 
                            if($l !== 'color' && $op['value'] !== '' && $op['value'] !== NULL){ 
                    ?> 
                            <span style="font-size:13px;"><?php echo $op['title']; ?></span>
                    : 
                    <?php 
                        if(is_array($va = $op['value'])){ 
					?>
                    <span style="font-size:13px !important;"><?php echo $va = join(', ',$va); ?></span>
                    <?php
                        } else {
					?>
                    <span style="font-size:13px !important;"><?php echo $va; ?></span>
                    <?php
                        }
                    ?>
                    <hr class="mr0">
                    <?php
                            }
                        }
                    ?>
                    <?php
                    if ($this->db->get_where('product', array('product_id' => $items['id']))->row()->is_bundle == 'no') {
                    ?>
                    <a href="<?php echo $this->crud_model->product_link($items['id']); ?>" class="change_choice_btn">
                        <?php echo translate('change_choices'); ?>
                    </a>
                    <?php
                    }
                    ?>
                    <?php
                    if ($this->db->get_where('product', array('product_id' => $items['id']))->row()->is_bundle == 'yes') {
                    ?>
                    <div style="padding: 5px">
                        <?php echo translate('products_:');?> <br>
                        <?php
                            $products = $this->db->get_where('product', array('product_id' => $items['id']))->row()->products;
                            $products = json_decode($products, true);
                            foreach ($products as $product) { ?>
                                echo $product['product_id'].'<br>';  
                                <a style="text-decoration:underline; font-size: 12px; padding-left: 15px;" href="<?php echo $this->crud_model->product_link($product['product_id']); ?>">
                                    
                                    <?php echo $this->db->get_where('product', array('product_id' => $product['product_id']))->row()->title . '<br>';?>
                                </a>
                        <?php
                            }
                        ?>
                    </div>
                    <?php
                    }
                    ?>

                    <?php if ($items['recipe'] == 'ok'){ ?>
                    <div class="row">
                        <div class="col-md-12 ">
                            <input type="file" class="recipes" required="required" name="recipe[<?php echo $key; ?>]" style="display: none;" id="<?php echo $key; ?>" onchange="readURL(this);" accept="image/*">
                            <button type="button" class="btn btn-sm btn-primary" onclick="document.getElementById('<?php echo $key; ?>').click();"><?php echo translate('docu_recipe'); ?></button>
                        </div>
                        <div class="col-md-12 form-group btm_border" id="div-<?php echo $key; ?>" style="display: none;">
                            <img src="<?php echo $items['image']; ?>" id="img-<?php echo $key; ?>" class="pull-center" style="max-width: 80px; max-height: 80px; width: auto; height: auto;" alt=""/>
                        </div>
                    </div>
                    <?php } ?>
                </td>
                <td class="quantity pric">
                    <?php echo currency($items['price']); ?>
                </td>
                <td class="quantity product-single" style="text-align:center;">
					<?php
                        if(!$this->crud_model->is_digital($items['id'])){
                    ?>
                    <span class="buttons">
                        <div class="quantity product-quantity">
                            <button type='button' class="btn in_xs quantity-button minus"  value='minus' >
                                <i class="fa fa-minus"></i>
                            </button>
                            <input  type="number" class="form-control qty in_xs quantity-field quantity_field" data-rowid="<?php echo $items['rowid']; ?>" data-limit='no' value="<?php echo $items['qty']; ?>" id='qty1' onblur="check_ok(this);" />
                            <button type='button' class="btn in_xs quantity-button plus"  value='plus' >
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                    </span>
                    <?php
						}
					?>
                </td>
                <td class="total">
                    <span class="sub_total">
                        <?php echo currency($items['subtotal']); ?> 
                        <input type="hidden" class="stotal" value="<?php echo $items['subtotal']; ?> ">
                    </span>
                </td>

                <td class="total">
                    <span class="close" style="color:#f00;">
                        <i class="fa fa-trash"></i>
                    </span>
                </td>
            </tr>
            <?php
                }
            ?>
        </tbody>
    </table>
</div>
<div class="col-md-4">
    <h3 class="block-title">
        <span>
            <?php echo translate('shopping_cart');?>
        </span>
    </h3>
    <div class="shopping-cart" style="background: #fff;">
        <table>
            <tr>
                <td><?php echo translate('subtotal');?>:</td>
                <td  id="total"></td>
            </tr>
            <tr>
                <td><?php echo translate('tax');?>:</td>
                <td id="tax"></td>
            </tr>
            <tr>
                <td><?php echo translate('shipping');?>:</td>
                <td id="shipping"></td>
            </tr>

            <tr class="coupon_disp" <?php if($this->cart->total_discount()<=0){ ?>style="display:none;" <?php } ?>>
                <td><?php echo translate('coupon_discount');?></td>
                <td id="disco">
                    <?php echo currency($this->cart->total_discount()); ?>
                </td>
            </tr>

            <tfoot>
                <tr>
                    <td><?php echo translate('grand_total');?>:</td>
                    <td class="grand_total" id="grand"></td>
                </tr>
            </tfoot>
        </table>


        <?php if($this->cart->total_discount() <= 0 && $this->session->userdata('couponer') !== 'done' && $this->cart->get_coupon() == 0){ ?>
            <h5>
            	<?php echo translate('enter_your_coupon_code_if_you_have_one');?>.
            </h5>
            <div class="form-group">
                <input type="text" class="form-control coupon_code" placeholder="Enter your coupon code">
            </div>
            <span class="btn btn-theme btn-block coupon_btn">
                <?php echo translate('apply_coupon');?>
            </span>
        <?php } else { ?>
            <p>
              <?php echo translate('coupon_already_activated'); ?>
            </p>
        <?php } ?>
    </div>

</div>

<div class="col-md-12">
    <span class="btn btn-theme-dark" onclick="load_address_form();">
        <?php echo translate('next');?>
    </span>
</div>


<?php
    echo form_open('', array(
    'method' =>
    'post', 'id' => 'coupon_set' )); 
?> 
<input type="hidden" id="coup_frm" name="code">
</form>

<script type="text/javascript">

function readURL(input){
  var file = input.files
  var size = file[0].size / 1024;
  if(file && file[0] && size.toFixed(2) <= 500){
    var reader = new FileReader();
    reader.onload = function(e){
      $('#img-' + input.id).attr('src', e.target.result);
      $('#img-' + input.id).show();
      $('#div-' + input.id).show();
      $('#close-' + input.id).show();
    }
    if ($('#load_payments').val() == 'ok') {
        $('#order_place_btn').removeClass('disabled');
    }
    reader.readAsDataURL(input.files[0]);
  } else {
    $(input).val('');
    notiv();
  }
}

function imgRemove(id){
  $('#' + id).val('');
  $('#div-' + id).hide();
  $('#img-' + id).hide();
  $('#close-' + id).hide();
  $('#img-' + id).removeAttr('src');
  $('#order_place_btn').attr('class', 'btn btn-theme pull-right disabled');
}

$( document ).ready(function() {
	$('body').on('click','.close', function(){
		var here = $(this);
		var rowid = here.closest('tr').data('rowid');
		var thetr = here.closest('tr');
		var list1 = $('#total');
		$.ajax({
			url: base_url+'home/cart/remove_one/'+rowid,
			beforeSend: function() {
				list1.html('...');
			},
			success: function(data) {
				list1.html(data).fadeIn();
				notify(cart_product_removed,'success','bottom','right');
				//sound('cart_product_removed');
				reload_header_cart();
				others_count();
				thetr.hide('fast');
				if(data == 0){
					location.replace('<?php echo base_url();?>');
				}
			},
			error: function(e) {
				console.log(e)
			}
		});
	});
	
    update_calc_cart();
});
</script>