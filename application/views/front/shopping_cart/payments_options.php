<?php
    $shipping = 0;
    $system_title = $this->db->get_where('general_settings',array('type' => 'system_title'))->row()->value;
    $total = $this->cart->total(); 
    if ($this->crud_model->get_type_name_by_id('business_settings', '3', 'value') == '') { 
        // $shipping = $this->crud_model->cart_total_it('shipping');
        $shipping = array_sum(array_column($carted,'shipping'));
    } elseif ($this->crud_model->get_type_name_by_id('business_settings', '3', 'value') == 'fixed') { 
        $shipping = $this->crud_model->get_type_name_by_id('business_settings', '2', 'value'); 
    } 
    // $tax = $this->crud_model->cart_total_it('tax'); 
    $tax    = array_sum(array_column($carted,'vat'));
    $grand = $total + $shipping + $tax; 
    //echo $grand; 

    $p_set = $this->db->get_where('business_settings',array('type'=>'paypal_set'))->row()->value; 
    $c_set = $this->db->get_where('business_settings',array('type'=>'cash_set'))->row()->value; 
    $s_set = $this->db->get_where('business_settings',array('type'=>'stripe_set'))->row()->value;
    $c2_set = $this->db->get_where('business_settings',array('type'=>'c2_set'))->row()->value; 
    $vp_set = $this->db->get_where('business_settings',array('type'=>'vp_set'))->row()->value;
    $du_set = $this->db->get_where('business_settings', array('type' => 'duitku_set'))->row()->value;
    $pum_set = $this->db->get_where('business_settings',array('type'=>'pum_set'))->row()->value;
    $ssl_set = $this->db->get_where('business_settings',array('type'=>'ssl_set'))->row()->value;
    $balance = $this->wallet_model->user_balance();
?>

<div class="row">
    <?php
        if($p_set == 'ok'){ 
    ?>
    <div class="cc-selector col-sm-3">
        <input id="visa" type="radio" style="display:block;" checked name="payment_type" value="paypal"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="visa" onclick="radio_check('visa')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/paypal.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('paypal');?>" />
               
        </label>
    </div>
    <script>
        $(document).ready(function(e) {
            var handler = 
        })
    </script>
    <?php
        } if($du_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="duitku_cc" style="display:block" type="radio" name="payment_type" value="duitku_cc"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_cc" onclick="radio_check('duitku_cc')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_visamastercard.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('creditcard');?>" />
        </label>
    </div>
    <!-- <div class="cc-selector col-sm-3">
        <input id="duitku_bca" style="display:block" type="radio" name="payment_type" value="duitku_bca"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku"id="duitku"onclick="radio_check('duitku_bca')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_bcaclickpay.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('bca');?>" />
        </label>
    </div>-->
    <div class="cc-selector col-sm-3">
        <input id="duitku_mandiri" style="display:block" type="radio" name="payment_type" value="duitku_mandiri"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_mandiri" onclick="radio_check('duitku_mandiri')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_mandiri.jpg" width="70%" height="100%" style=" text-align-last:center;" alt="<?php echo translate('mandiri');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_bni" style="display:block" type="radio" name="payment_type" value="duitku_bni"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_bni" onclick="radio_check('duitku_bni')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_bni.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('bni');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_cimb" style="display:block" type="radio" name="payment_type" value="duitku_cimb"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_cimb" onclick="radio_check('duitku_cimb')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_cimbniaga.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('cimb niaga');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_may" style="display:block" type="radio" name="payment_type" value="duitku_may"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_may" onclick="radio_check('duitku_may')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_maybank.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('maybank');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_permata" style="display:block" type="radio" name="payment_type" value="duitku_permata"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_permata" onclick="radio_check('duitku_permata')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_permatabank.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('permatabank');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_trans" style="display:block" type="radio" name="payment_type" value="duitku_trans"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_trans" onclick="radio_check('duitku_trans')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_atmbersama.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('atm bersama');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_ritel" style="display:block" type="radio" name="payment_type" value="duitku_ritel"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_ritel" onclick="radio_check('duitku_ritel')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_ritel.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('retail');?>" />
        </label>
    </div>
    <div class="cc-selector col-sm-3">
        <input id="duitku_ovo" style="display:block" type="radio" name="payment_type" value="duitku_ovo"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden" for="duitku_ovo" onclick="radio_check('duitku_ovo')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/d_ovo.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('ovo');?>" />
        </label>
    </div>
    <?php
        } if($s_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercard" style="display:block;" type="radio" name="payment_type" value="stripe"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercardd" id="customButtong" onclick="radio_check('mastercardd')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/stripe.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('stripe');?>" />
               
        </label>
    </div>
    <script>
        $(document).ready(function(e) {
            //<script src="https://js.stripe.com/v2/"><script>
            //https://checkout.stripe.com/checkout.js
            var handler = StripeCheckout.configure({
                key: '<?php echo $this->db->get_where('business_settings' , array('type' => 'stripe_publishable'))->row()->value; ?>',
                image: '<?php echo base_url(); ?>template/front/img/stripe.png',
                token: function(token) {
                    // Use the token to create the charge with a server-side script.
                    // You can access the token ID with `token.id`
                    $('#cart_form').append("<input type='hidden' name='stripeToken' value='" + token.id + "' />");
                    if($( "#visa" ).length){
                        $( "#visa" ).prop( "checked", false );
                    }
                    if($( "#mastercard" ).length){
                        $( "#mastercard" ).prop( "checked", false );
                    }
                    $( "#mastercardd" ).prop( "checked", true );
                    notify('<?php echo translate('your_card_details_verified!'); ?>','success','bottom','right');
                    setTimeout(function(){
                        $('#cart_form').submit();
                    }, 500);
                }
            });
    
            $('#customButtong').on('click', function(e) {
                // Open Checkout with further options
                var total = $('#grand').html(); 
                total = total.replace("<?php echo currency(); ?>", '');
                //total = parseFloat(total.replace(",", ''));
                total = total/parseFloat(<?php echo exchange(); ?>);
                total = total*100;
                handler.open({
                    name: '<?php echo $system_title; ?>',
                    description: '<?php echo translate('pay_with_stripe'); ?>',
                    amount: total
                });
                e.preventDefault();
            });
    
            // Close Checkout on page navigation
            $(window).on('popstate', function() {
                handler.close();
            });
        });
    </script>
    <?php
        } if($c2_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercardc2" style="display:block;" type="radio" name="payment_type" value="c2"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercardc2" onclick="radio_check('mastercardc2')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/c2.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('cash_on_delivery');?>" />
               
        </label>
    </div>
    <?php
        }if($vp_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercardc3" style="display:block;" type="radio" name="payment_type" value="vp"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercardc3" onclick="radio_check('mastercardc3')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/vp.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('voguepay');?>" />
               
        </label>
    </div>
    <?php
        }if($pum_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercard_pum" style="display:block;" type="radio" name="payment_type" value="pum"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercard_pum" onclick="radio_check('mastercard_pum')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/pum.png" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('payumoney');?>" />
               
        </label>
    </div>
    <?php
        }
        /* if($ssl_set == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercardc4" style="display:block;" type="radio" name="payment_type" value="sslcommerz"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercardc4" onclick="radio_check('mastercardc4')">
                <img src="<?php echo base_url(); ?>template/front/img/preview/payments/sslcommerz.jpg" width="100%" height="100%" style=" text-align-last:center;" alt="<?php echo translate('sslcommerz');?>" />
               
        </label>
    </div>
    <?php
        } */ if($c_set == 'ok'){
            if($this->crud_model->get_type_name_by_id('general_settings','68','value') == 'ok'){
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercard" style="display:block;" type="radio" name="payment_type" value="cash_on_delivery"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; " for="mastercard" onclick="radio_check('mastercard')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/cash.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('cash_on_delivery');?>" />
               
        </label>
    </div>
    <?php 
            }
        }
    ?>
    <?php 
    if ($this->crud_model->get_type_name_by_id('general_settings','84','value') == 'ok') {
        if ($this->session->userdata('user_login') == 'yes') {
    ?>
    <div class="cc-selector col-sm-3">
        <input id="mastercarddd" style="display:block;" type="radio" name="payment_type" value="wallet"/>
        <label class="drinkcard-cc" style="margin-bottom:0px; width:100%; overflow:hidden; text-align:center;" for="mastercarddd" onclick="radio_check('mastercarddd')">
            <img src="<?php echo base_url(); ?>template/front/img/preview/payments/wallet.jpg" width="70%" height="70%" style=" text-align-last:center;" alt="<?php echo translate('wallet');  ?> : <?php echo currency($this->wallet_model->user_balance()); ?>" />
            <span style="position: absolute;margin-top: -8%;margin-left: -26px;color: #000000;"><?php echo currency($this->wallet_model->user_balance()); ?></span>
        </label>
    </div>
    <?php
        }
    }    
    ?>
</div>
<style>
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}
 
.cc-selector input:active +.drinkcard-cc
{
    opacity: 1;
    border:4px solid #169D4B;
}
.cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
    -moz-filter: none;
    filter: none;
    border:4px solid black;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    -webkit-transition: all 100ms ease-in;
    -moz-transition: all 100ms ease-in;
    transition: all 100ms ease-in;
    -webkit-filter:opacity(.5);
    -moz-filter:opacity(.5);
    filter:opacity(.5);
    transition: all .6s ease-in-out;
    border:4px solid transparent;
    border-radius:5px !important;
}
.drinkcard-cc:hover{
    -webkit-filter:opacity(1);
    -moz-filter: opacity(1);
    filter:opacity(1);
    transition: all .6s ease-in-out;
    border:4px solid #8400C5;
            
}

</style>