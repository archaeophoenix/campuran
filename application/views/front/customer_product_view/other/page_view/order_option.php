<div id="pnopoi"></div>
<div class="buttons">
    <div id="share">
      <a target="_blank" href="https://www.facebook.com/sharer.php?u=<?php echo base_url('home/product_view') . "/".$row['customer_product_id'] . "/".$row['title'] ?>" title="Share On Facebook" class="pop share-square share-square-facebook" style="display: inline-block;"></a>
      <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo base_url('home/product_view') . "/".$row['customer_product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Share On Twitter" class="pop share-square share-square-twitter" style="display: inline-block;"></a>
      <a target="_blank" href="https://t.me/share/url?url=<?php echo base_url('home/product_view') . "/".$row['customer_product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Chat Seller On Telegram" class="pop share-square share-square-telegram" style="display: inline-block;"></a>
      <a target="_blank" href="https://api.whatsapp.com/send?phone=&text=<?php echo $row['title'];?>%20<?php echo base_url('home/product_view') . "/".$row['customer_product_id'] . "/".$row['title'] ?>" title="Chat Seller On WhatsApp" class="pop share-square share-square-whatsapp" style="display: inline-block;"></a>
      <a target="_blank" href="https://lineit.line.me/share/ui?url=<?php echo base_url('home/product_view') . "/".$row['customer_product_id'] . "/".$row['title'] ?>&text=<?php echo $row['title'];?>" title="Chat Seller On Line" class="pop share-square share-square-line" style="display: inline-block;"></a>
    </div>
      <?php if (!empty($row['video'])) { $video = json_decode($row['video'], true); ?>
      <div class="panel panel-default" style="margin-bottom:0px">
			  <div class="panel-heading">
			  	<h3 class="panel-title"><?php echo translate('ads_video');?></h3>
			  </div>
			  <div class="panel-body">
	        <iframe controls="2" width="100%" height="330" src="<?php echo $video['video_src']; ?> " frameborder="0"></iframe>
				</div>
			</div>
              
      <?php } ?>
      </div>
</div>
<script>
	/*$(document).ready(function() {
		$('#share').share({
			networks: ['facebook','whatsapp','telegram','line','twitter'],
			theme: 'square'
		});
	});*/
</script>
<script>
$(document).ready(function() {
	check_checkbox();
});
function check_checkbox(){
	$('.checkbox input[type="checkbox"]').each(function(){
        if($(this).prop('checked') == true){
			$(this).closest('label').find('.cr-icon').addClass('add');
		}else{
			$(this).closest('label').find('.cr-icon').addClass('remove');
		}
    });
}
</script>