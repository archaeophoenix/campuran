<!--https://kapeli.com/cheat_sheets/Font_Awesome.docset/Contents/Resources/Documents/css/all.css-->
<style>
.model-4 .whatsapp {
    color: #1bd741;
}
.model-4 .line {
    color: #00b900;
}
</style>
<?php
    $facebook	=  $this->db->get_where('social_links',array('type' => 'facebook'))->row()->value;
	$twitter 	=  $this->db->get_where('social_links',array('type' => 'twitter'))->row()->value;
	$whatsapp 	=  $this->db->get_where('social_links',array('type' => 'whatsapp'))->row()->value;
	$line 		=  $this->db->get_where('social_links',array('type' => 'line'))->row()->value;
	$telegram 	=  $this->db->get_where('social_links',array('type' => 'telegram'))->row()->value;
	$skype 		=  $this->db->get_where('social_links',array('type' => 'skype'))->row()->value;
?>
<ul class="list-inline">
    <li class="hidden-xs">
        <ul class="social-nav model-4">
			<?php if ( $facebook != "" ) { ?>
				<li><a href="<?php echo $facebook; ?>" class="facebook"><i class="fab fa-facebook"></i></a></li>
			<?php } ?>
			
			<?php if ( $twitter != "" ) { ?>
				<li><a href="<?php echo $twitter;?>" class="twitter"><i class="fab fa-twitter"></i></a></li>
			<?php } ?>
			
			<?php if ( $whatsapp != "" ) { ?>
				<li><a href="<?php echo $whatsapp;?>" class="whatsapp"><i class="fab fa-whatsapp"></i></a></li>
			<?php } ?>
			
			<?php if ( $line != "" ) { ?>
				<li><a href="<?php echo $line;?>" class="line"><i class="fab fa-line"></i></a></li>
			<?php } ?>
			
			<?php if ( $telegram != "" ) { ?>
				<li><a href="<?php echo $telegram;?>" class="twitter"><i class="fab fa-telegram"></i></a></li>
			<?php } ?>
			
			<?php if ( $skype != "" ) { ?>
				<li><a href="<?php echo $skype;?>" class="skype"><i class="fab fa-skype"></i></a></li>
			<?php } ?>
			<li><a href="<?php echo base_url(); ?>home/rss_links" class="rss"><i class="fa fa-rss"></i></a></li>            
        </ul>
    </li>
    <?php
        if ($this->session->userdata('user_login') !== 'yes') {
    ?>
        <li class="hidden-lg hidden-md hidden-sm">
            <a href="<?php echo base_url(); ?>home/login_set/login">
                <i class="fa fa-user-circle" style="width:auto; margin-right:5px;"></i>
                <?php echo translate('sign_in'); ?>
            </a>
        </li>
    <?php
        } else {
    ?>
        <li class="hidden-lg hidden-md hidden-sm">
            <a href="<?php echo base_url(); ?>home/profile/gp">
                <i class="fa fa-user"></i>
                <?php echo translate('profile'); ?>
            </a>
        </li>
        <li class="hidden-lg hidden-md hidden-sm">
            <a href="<?php echo base_url(); ?>home/logout">
                <i class="fa fa-sign-out" style="width:auto; margin-right:5px;"></i>
                <?php echo translate('logout'); ?>
            </a>
        </li>
    <?php
        }
    ?>
</ul>
