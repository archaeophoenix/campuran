<!DOCTYPE html>
<html lang="en">
  <head>
    <?php
      /*
      key as colom table
      value as variable name
      */
      $cols_gs = ['physical_product_activation' => 'physical_system',
                  'digital_product_activation' => 'digital_system',
                  'meta_description' => 'description',
                  'meta_keywords' => 'keywords',
                  'meta_author' => 'author',
                  'google_api_key' => 'map_api_key',
                  'vendor_system' => 'vendor_system',
                  'manufacture_system' => 'manufacture_system',
                  'system_name' => 'system_name',
                  'system_title' => 'system_title',
                  'revisit_after' => 'revisit_after',
                  'language' => 'set_lang',
                  'google_analytics_key' => 'google_analytics_key',
                  'contact_address' => 'contact_address',
                  'contact_phone' => 'contact_phone',
                  'contact_email' => 'contact_email',
                  'contact_website' => 'contact_website',
                  'contact_about' => 'contact_about',
                  'footer_text' => 'footer_text',
                  'data_all_brands' => 'data_all_brands',
                  'footer_category' => 'footer_category'];

      $cols_ui = '"fav_ext",
                 "header_color",
                 "font",
                 "home_top_logo",
                 "header_homepage_status",
                 "header_all_categories_status",
                 "header_featured_products_status",
                 "header_todays_deal_status",
                 "header_bundled_product_status",
                 "header_classifieds_status",
                 "header_latest_products_status",
                 "header_all_brands_status",
                 "header_all_vendors_status",
                 "header_blogs_status",
                 "header_store_locator_status",
                 "header_contact_status",
                 "header_more_status",
                 "home_categories"';

      $id_gs = [53, 58, 62, 68, 80, 81, 82, 83];

      $id_ui = [17, 20, 21, 23, 24, 25, 26, 27, 29, 30, 31, 35, 40, 41, 42, 43, 44];

      $ui_id = $this->crud_model->read('ui_settings', 'WHERE ui_settings_id IN (' . implode(',',$id_ui) . ')', 'type, value');

      $gs_id = $this->crud_model->read('general_settings', 'WHERE general_settings_id IN (' . implode(',',$id_gs) . ')', 'type, value');

      $ui_settings = $this->crud_model->read('ui_settings', 'WHERE type IN (' . $cols_ui . ')', 'type, value');

      $general_settings = $this->crud_model->read('general_settings', 'WHERE type IN ("' . implode('","',array_keys($cols_gs)) . '")', 'type, value');
    
      $all_category = $this->db->get('category')->result_array();
    
          foreach ($gs_id as $key => $val) {
            // create variable by $id_gs value with $gs_id values
            ${'gs_' . $id_gs[$key]} = $val['value'];
          }
    
          foreach ($ui_id as $key => $val) {
            // create variable by $id_gs value with $id_ui values
            ${'ui_' . $id_ui[$key]} = $val['value'];
          }
    
          foreach ($general_settings as $key => $val) {
            // create variable by $cols_gs value with $general_settings values
            ${$cols_gs[$val['type']]} = $val['value'];
          }
    
          foreach ($ui_settings as $key => $val) {
            // create variable by $cols_ui value with $ui_settings values
              ${$val['type']} = $val['value'];
          }
    
    
          $page_title    =  ucfirst(str_replace('_',' ',$page_title));
          $this->crud_model->check_vendor_mambership();
          if($this->router->fetch_method() == 'product_view'){
            $keywords   .= (isset($product_tags)) ? ','.$product_tags : '' ;
          }
          if($this->router->fetch_method() == 'vendor_profile'){
            $keywords   .= (isset($vendor_tags)) ? ','.$vendor_tags : '' ;
          }
          if($this->router->fetch_method() == 'manufacture_profile'){
            $keywords   .= (isset($manufacture_tags)) ? ','.$manufacture_tags : '' ;
          }

        ?>
    <title><?php echo $page_title; ?> || <?php echo $system_title; ?></title>
    <?php include 'includes/top/index.php'; ?>
    <style type="text/css">
    .ui-autocomplete {
      position: absolute;
      top: 100%;
      left: 0;
      z-index: 1000;
      display: none;
      float: left;
      min-width: 160px;
      padding: 5px 0;
      margin: 2px 0 0;
      list-style: none;
      font-size: 14px;
      text-align: left;
      background-color: #ffffff;
      border: 1px solid #cccccc;
      border: 1px solid rgba(0, 0, 0, 0.15);
      border-radius: 4px;
      -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
      box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
      background-clip: padding-box;
    }

    .ui-autocomplete > li > div {
      display: block;
      padding: 3px 20px;
      clear: both;
      font-weight: normal;
      line-height: 1.42857143;
      color: #333333;
      white-space: nowrap;
    }

    .ui-state-hover,
    .ui-state-active,
    .ui-state-focus {
      text-decoration: none;
      color: #262626;
      background-color: #f5f5f5;
      cursor: pointer;
    }

    .ui-helper-hidden-accessible {
      border: 0;
      clip: rect(0 0 0 0);
      height: 1px;
      margin: -1px;
      overflow: hidden;
      padding: 0;
      position: absolute;
      width: 1px;
    }
    </style>
  </head>
  <body id="home" class="wide">
    <!-- PRELOADER -->
    <?php
      $preloader = '1';
      //include 'preloader/preloader_'.$preloader.'.php';
      include 'preloader.php';
    ?>
    <!-- /PRELOADER -->

    <!-- WRAPPER -->
    <div class="wrapper">

      <!-- Popup: Shopping cart items -->
      <?php include 'components/cart_modal.php'; ?>
      <!-- /Popup: Shopping cart items -->

      <!-- HEADER -->
      <?php
        $header = '1';
        include 'header/header_'.$header.'.php';
      ?>      
      <!-- /HEADER -->

      <!-- CONTENT AREA -->
      <div class="content-area" page_name="<?= $page_name?>">
        <?php include $page_name.'/index.php'; ?>
      </div>
      <!-- /CONTENT AREA -->

      <!-- FOOTER -->
      <?php
        $footer = '1';
        include 'footer/footer_'.$footer.'.php';
      ?> 
      <!-- /FOOTER -->
      <div id="to-top" class="to-top"><img src="<?php echo base_url() ?>uploads/home_pages/icon_promedis_to_top.png" alt="">
      </br>
      </br>
    </div>
    <!-- /WRAPPER -->
    <?php
      include 'script_texts.php';
      include 'includes/bottom/index.php';
    ?>


    <!-- for demo only -->

    <?php if($this->config->item('demo_mode')) { ?>
    <div class="home-switch">
      <button>
        <i class="fa fa-cog fa-spin"></i>Beranda
      </button>
      <div class="preview">
        <div class="1 home-preview" url="<?php echo base_url() ?>?requested_homepage=1">
          <div>Beranda 1</div>
          <img src="<?php echo base_url() ?>uploads/home_pages/home_1.jpg" alt="">
        </div>
        <div class="1 home-preview" url="<?php echo base_url() ?>?requested_homepage=2">
          <div>Beranda 2</div>
          <img src="<?php echo base_url() ?>uploads/home_pages/home_2.jpg" alt="">
        </div>
        <div class="1 home-preview" url="<?php echo base_url() ?>?requested_homepage=3">
          <div>Beranda 3</div>
          <img src="<?php echo base_url() ?>uploads/home_pages/home_3.jpg" alt="">
        </div>
      </div>
    </div>
    <?php } ?>
    <style>
      .home-switch.active {
        right: 0px;
      }
      .home-switch {
        position: fixed;
        right: -220px;
        top: 20vh;
        z-index: 99999;
        box-shadow: 0 0 20px rgba(0,0,0,0.5);
        background: #fff;
        transition: all 0.3s;
        -webkit-transition: all 0.3s;
      }

      .home-switch .preview > div {
        height: 200px;
        width: 200px;
        overflow-y: auto;
        margin: 10px;
        border: 1px solid #ddd;
        cursor: pointer;
      }

      .home-switch .preview div img {
        width: 100%;
      }

      .home-switch button {
        position: absolute;
        background: #000000;
        border: 0;
        right: 100%;
        font-size: 18px;
        padding: 10px;
        box-shadow: -7px 0 10px rgba(0,0,0,0.3);
        border-right: 1px solid #ddd;
        color: #fff;
        font-weight: 700; 
      }
      .home-switch .preview > div div {
        padding: 5px;
        font-size: 15px;
        text-align: center;
        background: #000;
        color: #fff;
        font-weight: 700;
      }
    </style>
    <?php if($this->config->item('demo_mode')) { ?>
    <script>
      $(document).ready(function(){
        $('.home-switch button').on('click', function(){
          if ($('.home-switch').hasClass('active')) {
            $('.home-switch').removeClass('active');
          } else{
            $('.home-switch').addClass('active');
          }
        });

        $('.home-switch').on('click', function (e) {
          e.stopPropagation();
        });

        $('.home-preview').on('click', function (e) {
          window.location.href =  $(this).attr('url');
        });

        $(document).on('click', function (e) {
          if ($('.home-switch').hasClass('active')) {
            $('.home-switch').removeClass('active');
          }
        });
      });
      
    </script>
    <?php } ?>

    <!-- for demo only -->

  </body>
</html>
<html oncontextmenu="return false"> 
</html> 
 