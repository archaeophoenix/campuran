<section class="page-section color get_into">
    <div class="container">
        <div class="row margin-top-0">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="logo_top">
                    <a href="<?php echo base_url()?>">
                        <img class="img-responsive" src="<?php echo $this->crud_model->logo('home_bottom_logo'); ?>" alt="Shop" style="z-index:200">
                    </a>
                </div>
				<?php
                    echo form_open(base_url() . 'home/vendor_logup/add_info/', array(
                        'class' => 'form-login',
                        'method' => 'post',
                        'id' => 'sign_form'
                    ));
                ?>
                	<div class="row box_shape">
                        <div class="title">
                            <?php echo translate('vendor_registration');?>
                            <div class="option">
                            	<?php echo translate('already_a_vendor_?_click_to_');?>
                                <a href="<?php echo base_url(); ?>vendor" target="_blank" class="vendor_login_btn"> 
                                    <?php echo translate('login');?> <?php echo translate('as_vendor');?>
                                </a>!
                            	<?php echo translate('not_a_member_yet_?_click_to_');?>
                                <a href="<?php echo base_url(); ?>home/login_set/registration"> 
                                    <?php echo translate('sign_up');?> <?php echo translate('as_customer');?>
                                </a>!
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control required" name="name" type="text" placeholder="<?php echo translate('name');?>" data-toggle="tooltip" title="<?php echo translate('name');?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control required" name="display_name" type="text" placeholder="<?php echo translate('display_name');?>" data-toggle="tooltip" title="<?php echo translate('display_name');?>">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control emails required" name="email" type="email" placeholder="<?php echo translate('email');?>" data-toggle="tooltip" title="<?php echo translate('email');?>">
                            </div>
                            <div id='email_note'></div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control pass1 required" type="password" name="password1" placeholder="<?php echo translate('password');?>" data-toggle="tooltip" title="<?php echo translate('password');?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control pass2 required" type="password" name="password2" placeholder="<?php echo translate('confirm_password');?>" data-toggle="tooltip" title="<?php echo translate('confirm_password');?>">
                            </div>
                            <div id='pass_note'></div> 
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control required" name="company" type="text" placeholder="<?php echo translate('company');?>" data-toggle="tooltip" title="<?php echo translate('company');?>">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control required" name="address1" type="text" placeholder="<?php echo translate('address_line_1');?>" data-toggle="tooltip" title="<?php echo translate('address_line_1');?>">
                            </div>
                        </div>

                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <input class="form-control required" name="address2" type="text" placeholder="<?php echo translate('address_line_2');?>" data-toggle="tooltip" title="<?php echo translate('address_line_2');?>">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control required subdistrict" name="subdistrict_" id="subdistrict" type="text" placeholder="<?php echo translate('subdistrict');?>" data-toggle="tooltip" title="<?php echo translate('subdistrict');?>">
                                <input type="hidden" id="subdistrict_id" name="rajaongkir_subdistrict">
                                <input type="hidden" id="city_id" name="rajaongkir_city">
                                <input type="hidden" id="province_id" name="rajaongkir_province">
                                <input type="hidden" id="subdistrict_name" name="subdistrict">
                                <input type="hidden" id="city_name" name="city">
                                <input type="hidden" id="province" name="state">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control required" name="zip" type="text" placeholder="<?php echo translate('zip');?>" data-toggle="tooltip" title="<?php echo translate('zip');?>">
                            </div>
                        </div>
                        <!--<div class="col-md-6">
                            <div class="form-group">
                                <label for="files" class="btn btn-info"><?php echo translate('legal'); ?> <span id="selif">0 File</span></label>
                                <input style="display: none;" id="files" name="files[]" type="file" class="required" multiple="multiple">
                            </div>
                        </div>-->
                        <div class="col-md-6">
                            <div class="form-group">
                                <?php echo translate('seller_category');?>
                                 <select name="description" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                                    <option selected="" disabled=""><?php echo translate('description')?></option>
                                    <?php foreach ($desc as $key => $val) { ?>
                                      <option value="<?php echo $val['val']; ?>"><?php echo $val['val']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <!-- <div class="col-md-12">
                            <div class="title">
                                <?php echo translate('shop_location')?>
                            </div>
                        </div>

                        <!-- rajaongkir 

                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="city_field" id="user_province" onChange="get_city(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                                    <option value=""><?php echo translate('chose_a_province')?></option>
                                    <?php echo $this->crud_model->select_rajaongkir_area('province');?>
                                </select>
                            </div> 
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <select name="city_field" id="user_city" onChange="get_subdistrict(this.value,this)" class="selectpicker required"   tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip" >
                                    <option value=""><?php echo translate('chose_a_city')?></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group" >
                                <select class="selectpicker required" id="user_subdistrict" name="subdistrict_field" onChange="get_courier(this.value,this)" tabindex="2" data-hide-disabled="true" data-live-search="true" data-toggle="tooltip">
                                    <option value=""><?php echo translate('chose_a_subdistrict')?></option>
                                </select>
                            </div>
                        </div> -->
                        
                        <div class="col-md-12 terms">
                            <input  name="terms_check" type="checkbox" value="ok" > 
                            <?php echo translate('i_agree_with');?>
                            <a href="<?php echo base_url();?>home/legal/terms_conditions" target="_blank">
                                <?php echo translate('terms_&_conditions');?>
                            </a>
                        </div>
                        <?php
							if($this->crud_model->get_settings_value('general_settings','captcha_status','value') == 'ok'){
						?>
                        <div class="col-md-12">
                            <div class="outer required">
                                <div class="form-group">
                                    <?php echo $recaptcha_html; ?>
                                </div>
                            </div>
                        </div>
                        <?php
							}
						?>
                        <div class="col-md-12">
                            <span class="btn btn-theme-sm btn-block btn-theme-dark pull-right logup_btn" data-ing='<?php echo translate('registering..'); ?>' data-msg="">
                                <?php echo translate('register');?>
                            </span>
                        </div>
                    </div>
            	</form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function get_city(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_city/'+id, 'user_city');
    }

    function get_subdistrict(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_subdistrict/'+id, 'user_subdistrict');
    }

    function get_courier(id){
        get_rajaonkir(base_url+'home/rajaongkir/get_courier/'+id, 'user_courier');
    }

    function get_rajaonkir(url,id){
        $.ajax({
            url: url,
            cache: false,
            success: function(response){
                document.getElementById(id).innerHTML = response
                $('.selectpicker').selectpicker('refresh');
            }
        })
    }
</script>
<style>
	.get_into .terms a{
		margin:5px auto;
		font-size: 14px;
		line-height: 24px;
		font-weight: 400;
		color: #00a075;
		cursor:pointer;
		text-decoration:underline;
	}
	
	.get_into .terms input[type=checkbox] {
		margin:0px;
		width:15px;
		height:15px;
		vertical-align:middle;
	}
    
</style>