<!-- BREADCRUMBS -->
<section class="page-section" style="padding:0px">
    <div class="vendor_header">
        <!-- HEADER -->
        <div class="header header-logo-left" style="border-bottom:none;">
            <div class="header-wrapper"  style="padding: 15px 0;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <!-- Logo -->
                            <div class="logo">
                                <a href="<?php echo $this->crud_model->vendor_link($vendor_id); ?>">
                                    <?php if(file_exists('uploads/vendor_logo_image/logo_'.$vendor_id.'.png')){?>
                                    <img class="img-responsive" src="<?php echo base_url();?>uploads/vendor_logo_image/logo_<?php echo $vendor_id;?>.png"  style="height:90px; width:90px" alt="Shop"/>
                                    <?php }else{?>
                                        <img class="img-responsive" src="<?php echo base_url();?>uploads/vendor_logo_image/default.jpg" style="height:90px; width:90px" alt="Shop"/>
                                    <?php }?>
                                </a>
                            </div>
                            <!-- /Logo -->
                            <?php
                            $col = 'display_name, create_timestamp, email, address1, address2, display_name, website, twitter, youtube, skype, facebook, pinterest, instagram, google_plus, lat_lang';
                            $data = $this->crud_model->filter_one('vendor', 'vendor_id', $vendor_id, $col);
                            $data = array_pop($data);
                            $coo = json_decode($data['lat_lang'], true);
                            ?>
                            <div class="info">
                                <h3> 
                                    <?php echo $data['display_name'];?>
                                </h3>
                                <h6>
                                    <?php echo translate('member_since');?>: 
                                    <?php echo date("d M, Y", $data['create_timestamp']);?>
                                    <br>
                                <!--    <?php echo translate('email');?>:
                                	<?php echo $data['email'];?>-->
                                </h6>
                                <h6 class="hidden-sm hidden-xs">
                                    <?php echo $data['address1'];?>
                                    <br>
                                    <?php echo $data['address2'];?>
                                </h6>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <!-- new code -->
                            <?php if(file_exists('uploads/vendor_banner_image/banner_'.$vendor_id.'.jpg')){?>
                                <img class="slide-img"  src="<?php echo base_url();?>uploads/vendor_banner_image/banner_<?php echo $vendor_id;?>.jpg"/> 
                            <?php }else{?>
                                <img class="slide-img"  src="<?php echo base_url();?>uploads/vendor_banner_image/default.jpg"/> 
                            <?php }?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        	<div class="row">
                                <div class="col-md-6 author_rating" style="margin-bottom:0px;margin-top:0px;">
                                    <h6><?php echo translate('vendor_rating'); ?></h6>
                                    <div class="rating ratings_show" data-original-title="<?php echo $rating = $this->crud_model->vendor_rating($vendor_id); ?>"	
                                        data-toggle="tooltip" data-placement="left">
                                        <?php
                                            $r = $rating;
                                            $i = 6;
                                            while($i>1){
                                                $i--;
                                        ?>
                                            <span class="star <?php if($i<=$rating){ echo 'active'; } $r++; ?>"></span>
                                        <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                                
								<div class="col-md-6">
                                    <ul class="list-inline hidden-sm hidden-xs pull-right">
                                    <?php if($data['website'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://<?php echo $data['website'];?>" target="_blank" class="btn btn-icon btn-hover-purple fab fa-internet-explorer icon-lg"></a></li>
                                    <?php } if($data['twitter'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://<?php echo $data['twitter'];?>" target="_blank" class="btn btn-icon btn-hover-info fab fa-twitter icon-lg"></a></li>
                                    <?php } if($data['youtube'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://<?php echo $data['youtube'];?>" target="_blank" class="btn btn-icon btn-hover-danger fab fa-youtube icon-lg"></a></li>
                                    <?php } if($data['skype'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://t.me/<?php echo $data['skype'];?>" target="_blank" class="btn btn-icon btn-hover-info fab fa-telegram icon-lg"></a></li>
                                    <?php } if($data['facebook'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://<?php echo $data['facebook'];?>" target="_blank" class="btn btn-icon btn-hover-primary fab fa-facebook icon-lg"></a></li>
                                    <?php } if($data['pinterest'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://wa.me/<?php echo $data['pinterest'];?>" target="_blank" class="btn btn-icon btn-hover-success fab fa-whatsapp icon-lg"></a></li>
                                    <?php } if($data['instagram'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://<?php echo $data['instagram'];?>" target="_blank" class="btn btn-icon btn-hover-pink fab fa-instagram icon-lg"></a></li>
                                    <?php } if($data['google_plus'] != ''){ ?>
                                        <li style="padding: 0px;"><a href="https://line.me/ti/p/~<?php echo $data['google_plus'];?>" target="_blank" class="btn btn-icon btn-hover-success fab fa-line icon-lg"></a></li>
                                    <?php } ?>
                                        <li><a href="mailto:<?php echo $data['email']; ?>" class="btn btn-icon btn-hover-mint fa fa-envelope icon-lg"></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-6 profile_top">
                            <div class="row">
                                <div class="col-md-12" style="margin-top:0px;">
                                    <div class="top_nav">
                                        <ul>
                                            <li <?php if($content=='home'){ ?>class="active"<?php } ?> >
                                                <a href="<?php echo base_url(); ?>home/vendor_profile/<?php echo $vendor_id;?>">
                                                    <?php echo translate('about_seller');?>
                                                </a>
                                            </li>
                                            <li <?php if($content=='product_list'){ ?>class="active"<?php } ?>>
                                                <a href="<?php echo base_url(); ?>home/vendor_category/<?php echo $vendor_id;?>/0">
                                                    <?php echo translate('featured_products');?>
                                                </a>
                                            </li>
                                            <li <?php if($content=='featured'){ ?>class="active"<?php } ?>>
                                                <a href="<?php echo base_url(); ?>home/vendor_featured/<?php echo $vendor_id;?>">
                                                    <?php echo translate('vendor_products');?>
                                                </a>
                                            </li>
                                            <?php if ($coo[0] != 0 && $coo[1] != 0) { ?>
                                                <li <?php if($content=='contact'){ ?>class="active"<?php } ?>>
                                                    <a href="https://www.google.com/maps/place/<?php echo $coo[0] . ',' . $coo[1]; ?>" target="_blank">
                                                        <?php echo translate('find_location');?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /HEADER -->             
    </div>
</section>
<!-- /BREADCRUMBS -->
<style>
.social-icons-ven li {
    padding: 5px 0px 0 0 !important;
    float: right !important;
}
.social-icons a.facebook {
    color: #3b5998;
}
.social-icons a.twitter {
    color: #1da1f2;
}
.social-icons a.google {
    color: #dd4c40;
}
.social-icons a.pinterest {
    color: #bd081c;
}
.social-icons a.youtube {
    color: #ff0000;
}
.social-icons a.skype {
    color: #1686D9;
}
</style>