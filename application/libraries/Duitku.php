<?php
/**
* this payment gateway duitku build by www.psglobalmedia.co.id
* PT PS Global Media
*
*/

class Duitku{

    function Duitku(){
        $CI=& get_instance();
        $CI->load->database();
        $type = $CI->db->get_where('business_settings',array('type'=>'duitku_type'))->row()->value;
        if($type == 'sandbox') {
            $this->duitku_url = 'http://sandbox.duitku.com/webapi/api/merchant/v2/inquiry';
         } else if($type == 'original') {
            $this->duitku_url = 'https://passport.duitku.com/webapi/api/merchant/v2/inquiry';
         }
         
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
            'Content-Type: application/json',                                                                                
            'Content-Length: ' . strlen($params_string))                                                                       
        );   
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
        //execute post
        $request = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    }
}