<?php

/**
 * RajaOngkir CodeIgniter Library
 * Digunakan untuk mengkonsumsi API RajaOngkir dengan mudah
 * 
 * @author Damar Riyadi <damar@tahutek.net>
 */
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'rajaongkir/endpoints.php';

class RajaOngkir extends Endpoints {

    private $api_key;
    private $account_type;

    public function __construct() {
        // Pastikan bahwa PHP mendukung cURL
        if (!function_exists('curl_init')) {
            log_message('error', 'cURL Class - PHP was not built with cURL enabled. Rebuild PHP with --with-curl to use cURL.');
        }

        $CI=& get_instance();
        $CI->load->database();
        // Pastikan Anda sudah memasukkan API Key di application/config/rajaongkir.php
        if ($CI->db->get_where('business_settings', array('type'=>'rajaongkir_api'))->row()->value == "") {
            log_message("error", "Harap masukkan API KEY Anda di config.");
        } else {
            $this->api_key = $CI->db->get_where('business_settings', array('type'=>'rajaongkir_api'))->row()->value;
            $this->account_type = $CI->db->get_where('business_settings', array('type'=>'rajaongkir_type'))->row()->value;
        }
        parent::__construct($this->api_key, $this->account_type);
    }
}
