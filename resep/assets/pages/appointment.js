/*
 Template Name: Promedis - Patient Managament System
 Author: PS Global Media
 File: Apppointment
 */
	$(document).ready(function(){
        
        $('.dt').on('change',function(){
        	//alert();
        	$("#btn_create").removeAttr('disabled');
        }); 

        $('#btn_create').on('click',function(e){
        	e.preventDefault();
        	var p_id = $('#myselect2').val();
        	var date = $('#datepicker-autoclose').val();
        	var time = $('#timepicker1').val();
          var base_url = $('#base_url').val();
        	//alert(time);

        	$.ajax({
				type:'POST',
				url:base_url + '/user_operation/chkappointment',
				dataType:'json',
				data:{p_id:p_id,
					  date:date,
					  time:time
					 },
				success: function(data){
						if(data.status == 1)
						{
							$(".status").css('color','red');
							$(".status").text("Telah Reservasi Appointment Hari Ini");
							$("#btn_create").prop('disabled','true');
							//console.log('success');
						}
						else if(data.status == 2)
						{
							$(".status").css('color','red');
							$(".status").text("Slot Waktu Dialokasikan Untuk Pasien Lainnya");
						}
						else
						{
							$(".status").css('color','green');
							$(".status").text("Reservasi Appointment Telah Berhasil");	
						}
				},
				error:function(data)
				{
					alert('oops! Mohon maaf ada kesalahan, silahkan coba lagi!!!');
				}
			});
        });

    });
