<?php $data = $this->user_mo->get_user(); ?>

<!-- Footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <?php echo date('Y'); ?> &copy; <?php echo $data[0]['title']; ?> <span class="text-green hidden-xs-down pull-right">Promedis Resep<i class="mdi bike text-danger"></i>-PS Global Media</span>
      </div>
    </div>
  </div>
</footer>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
<!-- End Footer -->

<!-- jQuery  -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/tether.min.js"></script><!-- Tether for Bootstrap -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/waves.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>