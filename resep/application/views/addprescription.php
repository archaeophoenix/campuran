<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php') 
?>

    <!-- Select2 -->
<link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<?php include_once('includes/header_end.php'); 
  $data = $this->user_mo->get_user();
  $error = $this->session->flashdata('error');
?>

    <div class="wrapper">
      <div class="container">
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <div class="page-title-box">
              <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
                  <li class="breadcrumb-item active">Tambahkan Resep Baru</li>
                </ol>
              </div>
              <h4 class="page-title">Tambahkan Resep Baru</h4>
            </div>
          </div>
        </div>
        <!-- end page title end breadcrumb -->
      </div> <!-- End Container -->
    </div><!-- End Wrapper -->

    <!-- ==================
      PAGE CONTENT START
      ================== -->
    <div class="page-content-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="m-b-20">
              <a href="<?php echo base_url('user/prescription'); ?>"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-arrow-left"></i>&nbsp; Ke Daftar Resep</button></a>
            </div>
          </div>
        </div><!-- Ends Row -->
        <div class="row">
          <div class="col-12">
            <div class="card m-b-20">
              <div class="card-block">
                <div class="alert alert-warning text-center" role="alert">
                  <strong>Pemberitahuan:</strong> Praktisi bisa meresepkan Obat-obatan atau terapi fisik atau jenis perawatan bagi pasien sesuai undang-undang yang berlaku.
                </div>
                <ul class="nav nav-pills nav-fill nav-justified" id="tabs" role="tablist">
                  <li class="nav-item">
                    <a class="text-danger nav-link active" data-toggle="tab" href="#medicine" role="tab">Klik Disini: Meresepkan Obat-obatan | Suplemen | Alat Kesehatan Di sini</a>
                  </li>
                  <li class="nav-item">
                    <a class="text-danger nav-link" data-toggle="tab" href="#test" role="tab">Klik Disini: Meresepkan Pemeriksaan Lab| Radiologi |  Terapi Fisik | Tindakan Perawatan</a>
                  </li>
                </ul>
                <form name="addprescription" id="addprescription" method="post" action="<?php echo base_url('user_operation/addprescription'); ?>">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="patient" class="col-form-label text-muted">Nama Pasien</label>
                            <div class="input-group">
                              <select class="select" id="myselect2" name="patient_id" required="">
                                <option value="">Pilih</option>
                                  <?php foreach ( $info as $data ) { ?>

                                <option value="<?php echo $data['patient_id']; ?>"><?php echo $data['p_name']; ?></option>

                                  <?php } ?>

                              </select>
                            </div>
                            <?php if(isset($error['patient_id'])){?> <span class="text-danger"><?php echo $error['patient_id']; ?></span> <?php } ?>

                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="symptoms" class="col-form-label text-muted">Gejala Klinis</label>
                            <div class="input-group">
                              <textarea class="form-control" name="symptoms" id="symptoms" placeholder="Tambahkan Gejala Klinis" rows="3" required=""></textarea>
                            </div>
                            <?php if(isset($error['symptoms'])){?> <span class="text-danger"><?php echo $error['symptoms']; ?></span> <?php } ?>

                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label for="diagnosis" class="col-form-label text-muted">Diagnosa</label>
                            <div class="input-group">
                              <textarea class="form-control" name="diagnosis" id="diagnosis" placeholder="Tambahkan Diagnosa" rows="3" required=""></textarea>                            
                            </div>
                            <?php if(isset($error['diagnosis'])){?> <span class="text-danger"><?php echo $error['diagnosis']; ?></span> <?php } ?>

                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <input type="hidden" name="type" id="type" value="#medicine">
                          <button class="btn btn-primary waves-effect waves-light" type="submit" name="save_prescription"><i class="fa fa-save"></i> &nbsp; Simpan Resep</button>
                        </div>
                      </div>  
                    </div>
                    <div class="col-md-6">
                      <div class="tab-content">
                        <div class="tab-pane active" id="medicine" role="tabpanel">
                          <div class="row">                      
                            <div class="col-md-12">
                              <div class="form-group">
                                <div id="medicine_entry">
                                  <div class="form-group">
                                    <label for="patient" class="col-form-label text-muted">Obat-obatan | Suplemen | Alat Kesehatan</label>
                                    <div class="row">
                                      <div class="col-md-3">
                                        <input type="text" class="medicine form-control" placeholder="Nama Produk/Generik" name="medicine_name[]" required="">
                                        <?php if(isset($error['medicine_name'])){?> <span class="text-danger"><?php echo $error['medicine_name']; ?></span> <?php } ?>
                                      </div>
                                      <div class="col-md-3">
                                        <input type="text" class="medicine form-control" placeholder="Jumlah Unit Obat" name="medicine_note[]" required="">
                                        <?php if(isset($error['medicine_note'])){?> <span class="text-danger"><?php echo $error['medicine_note']; ?></span> <?php } ?>
                                      </div>
                                      <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Dosis Terapi & Cara Minum" name="medicine_note2[]">
                                      </div>
                                      <div class="col-md-2">
                                        <button type="button" class="fcbtn btn btn-outline btn-danger btn-1d btn-sm" data-toggle="tooltip" data-placement="right" title="Hapus" onclick="delele_parent_element(this, 'medicine')"><i class="fa fa-times"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div id="append_holder_for_medicine_entries"></div>
                              </div>
                              <div class="form-group">
                                <div class="row">
                                  <div class="col-md-12">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" onclick="append_blank_entry('medicine')"><i class="fa fa-plus"></i> &nbsp; Obat-obatan | Suplemen | Alat Kesehatan</button>
                                  </div>
                                </div>
                              </div>
                            </div>                      
                          </div>
                        </div>
                        <div class="tab-pane" id="test" role="tabpanel">
                          <div class="row">                      
                            <div class="col-md-12">
                              <div class="form-group">
                                <div id="test_entry">
                                  <div class="form-group">
                                    <label for="patient" class="col-form-label text-muted">Jenis Pemeriksaan Lab| Radiologi |  Terapi Fisik | Tindakan Perawatan</label>
                                    <div class="row">
                                      <div class="col-md-3">
                                        <input type="text" class="test form-control" placeholder="Nama Pemeriksaan" name="test_name[]">
                                        <?php if(isset($error['test_name'])){?> <span class="text-danger"><?php echo $error['test_name']; ?></span> <?php } ?>
                                      </div>
                                      <div class="col-md-3">
                                        <input type="text" class="test form-control" placeholder="Catatan Pemeriksaan" name="test_note[]">
                                        <?php if(isset($error['test_note'])){?> <span class="text-danger"><?php echo $error['test_note']; ?></span> <?php } ?>
                                      </div>
                                      <div class="col-md-4">
                                        <input type="text" class="form-control" placeholder="Catatan Tambahan" name="test_note2[]">
                                      </div>
                                      <div class="col-md-2">
                                        <button type="button" class="btn-sm fcbtn btn btn-outline btn-danger btn-1d" data-toggle="tooltip" data-placement="right" title="Remove" onclick="delele_parent_element(this, 'test')"><i class="fa fa-times"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div id="append_holder_for_test_entries"></div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <button type="button" class="btn btn-primary waves-effect waves-light" onclick="append_blank_entry('test')"><i class="fa fa-plus"></i> &nbsp; Tambahkan Pemeriksaan</button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>                   
                </form>
              </div>
            </div>
          </div>
        </div><!-- Ends Row -->
      </div><!-- Ends container -->
    </div><!-- Ends page-content-wrapper -->

<?php include_once('includes/footer_start.php'); ?>

    <!-- Select2 -->
    <script src="<?php echo base_url(); ?>assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>

    <script type="text/javascript">
      
      var blank_medicine_entry = '';
      var blank_test_entry = '';
      var number_of_medicine = 1;
      var number_of_test = 1;

      $(document).ready(function() {

        $('.select').select2();

        blank_medicine_entry = $('#medicine_entry').html();
        blank_test_entry = $('#test_entry').html();
        console.log(number_of_medicine);
        console.log(number_of_test);

        $('.nav-link').click(function(){
          var txt = $(this).attr('href');
          console.log(txt);
          if (txt == '#medicine') {
            $('.medicine').attr('required', 'required');
            $('.test').removeAttr('required');
          } else {
            $('.test').attr('required', 'required');
            $('.medicine').removeAttr('required');
          }
          $('#type').val(txt);

        });

      });

      function append_blank_entry(selector) {
        if (selector == 'medicine') {
        number_of_medicine = number_of_medicine + 1;
        $('#append_holder_for_medicine_entries').append(blank_medicine_entry);
        console.log(number_of_medicine);
        } else {
        number_of_test = number_of_test + 1;
        $('#append_holder_for_test_entries').append(blank_test_entry);
        console.log(number_of_test);
        }
      }

      function delele_parent_element(n, selector) {
        if (selector == 'medicine') {
        if (number_of_medicine > 1) {
          n.parentNode.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode.parentNode);
        }
        if (number_of_medicine != 1) {
          number_of_medicine = number_of_medicine - 1;
        }
        console.log(number_of_medicine);
        } else {
        if (number_of_test > 1) {
          n.parentNode.parentNode.parentNode.parentNode.removeChild(n.parentNode.parentNode.parentNode);
        }
        if (number_of_test != 1) {
          number_of_test = number_of_test - 1;
        }
        console.log(number_of_test);
        }
      }

    </script>

<?php include_once('includes/footer_end.php'); ?>