<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('includes/header_start.php'); 
?>
<!-- DataTables -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<!-- Responsive datatable examples -->
<link href="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

<?php
include_once('includes/header_end.php');
$data = $this->user_mo->get_user();
?>

<div class="wrapper">
  <div class="container">

    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
              <li class="breadcrumb-item active">Pengguna</li>
            </ol>
          </div>
          <h4 class="page-title">Daftar Pengguna</h4>
        </div>
      </div>
    </div>
    <!-- end page title end breadcrumb -->
  </div> <!-- End Container -->
</div>
<!-- End Wrapper -->

<!-- ================== PAGE CONTENT START ================== -->
<div class="page-content-wrapper">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="card m-b-20">
          <div class="card-block">
            <div id="DataTables_Table_0_wrapper" class="table-responsive dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
              <div class="row">
                <div class="col-sm-12 col-md-6"></div>
                <div class="col-sm-12 col-md-6">
                  <div id="DataTables_Table_0_filter" class="dataTables_filter">
                    <label>Search:<input type="search" class="form-control form-control-sm" placeholder="" id="__name" oninput="users(1);"></label>
                  </div>
                </div>
              </div>
              <div class="row table-responsive">
                <table class="table table-bordered">
                  <thead>
                  <tr>
                    <th class="text-center">No.</th>
                    <th class="text-center">Nama</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Telepon</th>
                    <th class="text-center">Alamat</th>
                    <th class="text-center">Spesialis</th>
                    <th class="text-center">SIP</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Tindakan</th>
                  </tr>
                  </thead>
                  <tbody id="users"></tbody>
                </table>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-5">
                  <div class="dataTables_info" role="status" id="entries" aria-live="polite">
                  </div>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                    <ul class="pagination" id="pagination"></ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> <!-- end col -->
      </div> <!-- end row -->
    </div> <!-- end container -->
  </div>
</div>
<!-- end page-content-wrapper -->

<?php include_once('includes/footer_start.php'); ?>

<!-- Required datatable js -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jszip.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/pdfmake.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/vfs_fonts.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.html5.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.print.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/buttons.colVis.min.js"></script>

<!-- Responsive examples -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/responsive.bootstrap4.min.js"></script>

<!-- Datatable init js -->
<script src="<?php echo base_url(); ?>assets/pages/datatables.init.js"></script>

<script type="text/javascript">
var base_url = $('#base_url').val();

$(document).ready(function() {
  users();
});

async function users(page = 1){
  var no = '';
  var tag = '';
  var sta = ['Inaktif', 'Aktif'];
  var name = $('#__name').val();
  name = (name == '') ? '' : '&name=' + name ;
  var data = await $.post(base_url + '/user_operation/users/?page=' + page + name);
  var num = ((data.page - 1) * 10) + 1;
  var users = data.user;

  for(var i in users){
    i = parseInt(i);
    data.page = parseInt(data.page);
    data.count = parseInt(data.count);
    no = ((data.page - 1) * 10) + (i + 1);

    var office = (users[i].office == '') ? '' : JSON.parse(users[i].office);
    office = (office == '') ? '' : office[0].address ;

    var first = (data.first == '') ? '' : '<li class="paginate_button page-item previous" id="DataTables_Table_0_previous"><a href="#" onclick="users(1)" aria-controls="DataTables_Table_0" class="page-link">&lsaquo;</a></li>';
    var last = (data.last == '') ? '' : '<li class="paginate_button page-item next" id="DataTables_Table_0_next"><a href="#" onclick="users(' + data.rows + ')" aria-controls="DataTables_Table_0" class="page-link">&rsaquo;</a></li>';
    var mid = '';

    for (var j = data.st1; j <= data.end; j++) {
      var selected = (data.page == j) ? 'active' : '';
      var ahref = (data.page == j) ? '<a aria-controls="DataTables_Table_0" class="page-link" href="#">' + j + '</a>' : '<a aria-controls="DataTables_Table_0" class="page-link" href="#" onclick="users(' + j + ');">' + j + '</a>';
      mid += '<li class="paginate_button page-item ' + selected + '" >' + ahref + '</li>';
    }

    var det = '<a href="' + base_url + 'user/users/' + users[i].user_id + '" title="Detail Pengguna" class="btn btn-1d btn-sm btn-outline-primary waves-effect waves-light"><i class="mdi mdi-account-card-details"></i></a>';

    tag += '<tr><td>' + no + '</td><td>' + users[i].doctor_name + '</td><td>' + users[i].email + '</td><td class="text-right">' + users[i].mobile + '</td><td>' + office + '</td><td>' + users[i].specialist + '</td><td>' + users[i].sip + '</td><td>' + sta[users[i].status] + '</td><td class="text-center">' + det + '</td></tr>';
  }

  $('#users').html(tag);
  $('#entries').text('Showing '+ num + ' to ' + no + ' of ' + data.count + ' entries');
  $('#pagination').html(first + mid + last);
}
</script>

<?php include_once('includes/footer_end.php'); ?>