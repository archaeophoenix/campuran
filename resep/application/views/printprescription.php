<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php'); 
?>
<?php include_once('includes/header_end.php');
    $sys_title = $this->user_mo->get_user();
    $id = $this->uri->segment(3);
    $user = $this->user_mo->getuser();
    $row_data = $this->user_mo->getprescriptionbyid($id);
?>

    <div class="wrapper">
      <div class="container">

        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <div class="page-title-box">
              <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $sys_title[0]['title']; ?></a></li>
                  <li class="breadcrumb-item active">Cetak Resep</li>
                </ol>
              </div>
              <h4 class="page-title">Cetak Resep</h4>
            </div>
          </div>
        </div>
        <!-- end page title end breadcrumb -->
      </div> <!-- End Container -->
    </div><!-- End Wrapper -->
    <?php
      if(!$row_data) {
        echo "<center><h3>Tidak Ditemukan!!</h3></center>";
      } else {
        $data = $row_data[0];
        $info = $this->user_mo->getp_name($data['patient_id']);
        $medicine = (empty($data['medicine'])) ? '' : json_decode($data['medicine'], true);
        $m_note = (empty($data['m_note'])) ? '' : json_decode($data['m_note'], true);
        $m_note2 = (empty($data['m_note2'])) ? '' : json_decode($data['m_note2'], true);
        $test = (empty($data['test'])) ? '' : json_decode($data['test'], true);
        $t_note = (empty($data['t_note'])) ? '' : json_decode($data['t_note'], true);
        $t_note2 = (empty($data['t_note2'])) ? '' : json_decode($data['t_note2'], true);
    ?>

    <!-- ==================
      PAGE CONTENT START
    ================== -->
    <div class="page-content-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="m-b-20">
              <a href="<?php echo base_url('user/prescription'); ?>"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-arrow-left"></i>&nbsp; Kembali Ke Daftar Resep</button></a>
              <button type="button" class="btn btn-primary waves-effect waves-light" onclick="print_prescription()"><i class="fa fa-print"></i>&nbsp; Cetak Resep</button>
            </div>
          </div>
        </div>
        <div class="prescription-print" id="prescription-print">
          <div class="row">
            <div class="col-12">
              <div class="card m-b-20">
                <div class="card-block">
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-12 text-center"><img src="<?php echo base_url() . 'assets/images/' . $user['logo']; ?>"></div>
                  </div>
                  <hr>
                  <div class="col-md-12">
                    <h3 class="text-capitalize text-center"><u><?php echo $user['doctor_name']; ?></u></h3>
                  </div>
                    <h5 class="text-capitalize text-center"><?php echo $user['specialist']; ?><br><?php echo $user['sip']; ?></h5>
                  <hr>
                  ALAMAT PRAKTEK :
                  <?php if (!empty(json_decode($user['office'], true))){ ?>
                  <div class="row">
                    <?php foreach (json_decode($user['office'], true) as $key => $val){ ?>
                      <?php if ($key < 3){ ?>
                      <div class="col-md-4">
                        <address>
                          <p><?php echo $val['address']; ?></p>
                          <table style="width: 100%;">
                            <tr><td width="20%">Telp.:</td><td width="80%"><?php echo $user['mobile']; ?></td></tr>
                            <tr><td width="20%">Hari:</td><td width="80%"><?php echo $val['day']; ?></td></tr>
                            <tr><td width="20%">Jam:</td><td width="80%"><?php echo $val['hour']; ?></td></tr>
                          </table>
                        </address>
                      </div>
                      <?php } ?>
                    <?php } ?>
                  </div>
                  <?php } ?>
                  <hr>
                  <!-- <div class="row">
                    <div class="col-md-12">
                      <div class="pull-left">
                        <address>
                          <h3><b class="text-danger"><?php echo $user['doctor_name'] ?></b></h3>
                          <p class="text-muted m-l-5">

                            <br>
                            Email - <?php echo $user['email'] ?> <br>
                            Telepon - <?php echo $user['mobile'] ?>

                          </p>
                        </address>
                      </div>
                      <div class="pull-right text-right">
                        <address>
                          <h4>Pro,</h4>
                          <h5><?php echo $info['p_name']; ?></h5>
                          <p class="text-muted m-l-30"><strong>Telepon</strong> : <?php echo $info['phone']; ?></p>
                          <p class="m-t-30"><b> Tanggal :</b> <i class="fa fa-calendar"></i>&nbsp; <?php echo date('d-m-Y') ?></p>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5>Gejala Klinis</h5>
                      <p><?php echo $data['symptoms'] ?></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <h5>Diagnosa</h5>
                      <p><?php echo $data['diagnosis'] ?></p>
                    </div>
                  </div> -->
                  <div class="row">
                    <!-- <div class="col-md-2 text-right"><h4>R/</h4></div>
                    <div class="col-md-6"><h4>&nbsp;</h4></div> -->
                    <div class="col-md-12 text-right">
                      <p style="margin-left: 80%;" class="col-md-12 pull-right"><b> Tanggal :</b> <i class="fa fa-calendar"></i>&nbsp; <?php echo date("d-m-Y", strtotime($row_data[0]['date'])); ?></p>
                    </div>
                  </div>
                  <!-- <div class="col-md-12" style="margin-top: -3%;"> -->

                  <div class="row">
                  <?php if (!empty($data['test'])){ ?>
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-8">
                      <div class="table-responsive">
                        <table style="width: 100%; border-spacing: 4px;">
                          <tbody>
                          <?php for ( $i=0; $i<count($test); $i++) { ?>
                            <tr>
                              <td width="5%">R/</td>
                              <td width="45%"><?php echo $test[$i]; ?></td>
                              <td width="50%"><?php echo $t_note[$i]; ?></td>
                            </tr>
                            <?php if (isset($t_note2[$i])){ ?>
                            <tr>
                              <td width="5%"></td>
                              <td colspan="2" class="text-danger"><?php echo $t_note2[$i]; ?></td>
                            </tr>
                            <?php } ?>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-2">&nbsp;</div>
                  <?php } ?>
                  <?php if (!empty($data['medicine'])){ ?>
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-8">
                      <div class="table-responsive">
                        <table style="width: 100%; border-spacing: 4px;">
                          <tbody>
                          <?php for ( $i=0; $i<count($medicine); $i++){ ?>
                            <tr>
                              <td width="5%">R/</td>
                              <td width="45%"><?php echo $medicine[$i]; ?></td>
                              <td width="50%"><?php echo $m_note[$i]; ?></td>
                            </tr>
                            <?php if (isset($m_note2[$i])){ ?>
                            <tr>
                              <td width="5%"></td>
                              <td colspan="2" class="text-danger"><?php echo $m_note2[$i]; ?></td>
                            </tr>
                            <?php } ?>
                          <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-2">&nbsp;</div>
                  <?php } ?>
                  </div>
                  <br>
                  <hr>
                  <!-- <div class="row">
                    <div class="col-md-12">
                      <h5><i class="fa fa-calendar"></i>&nbsp;<?php echo date("d-m-Y", strtotime($row_data[0]['date'])); ?></h5>
                      <br>
                      <?php if (!empty($data['test'])) { ?>
                        <h5>Data Pemeriksaan</h5>
                        <?php foreach ($test as $k => $val) { ?>
                        <div class="row">
                          <p class="col-md-12"><label>R/ Terapi</label> <?php echo $val; ?></p>
                          <p class="col-md-12 text-danger"><?php echo $t_note[$k]; ?></p>
                        </div>
                        <?php } ?>
                        <hr>
                      <?php } ?>
                      <?php if (!empty($data['medicine'])) { ?>
                        <h5>Data Obat-obatan</h5>
                        <?php foreach ($medicine as $k => $val) { ?>
                        <div class="row">
                          <label class="col-md-2">Resep <?php echo ($k + 1); ?></label>
                          <p class="col-md-10"><?php echo $val; ?></p>
                        </div>
                        <div class="row">
                          <label class="col-md-2">Dosis / Note</label>
                          <p class="col-md-10"><?php echo $m_note[$k]; ?></p>
                        </div>
                        <?php } ?>
                        <hr>
                      <?php } ?>
                    </div>
                  </div> -->
                  <div class="row">
                    <div class="col-md-12">
                       <div class="table-responsive">
                        <table style="width: 100%; border-spacing: 4px;">
                          <tbody>
                            <tr>
                              <td width="20%">Nama Pasien:</td>
                              <td width="80%"><?php echo $row_data[0]['p_name']; ?></td>
                            </tr>
                            <tr>
                              <td width="20%">Usia Pasien:</td>
                              <td width="80%"><?php echo $row_data[0]['age']; ?></td>
                            </tr>
                          <?php if (TYPE == 'vetbiz') { ?>
                            <tr>
                              <td width="20%">Jenis Hewan:</td>
                              <td width="80%"><?php echo $row_data[0]['type']; ?></td>
                            </tr>
                            <tr>
                              <td width="20%">Pemilik:</td>
                              <td width="80%"><?php echo $row_data[0]['owner']; ?></td>
                            </tr>
                          <?php } ?>
                            <tr>
                              <td width="20%">Telp.:</td>
                              <td width="80%"><?php echo $row_data[0]['phone']; ?></td>
                            </tr>
                            <tr>
                              <td width="20%">Alamat:</td>
                              <td width="80%"><?php echo $row_data[0]['add']; ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="col-md-12 text-center">
                      <h4 class="text-capitalize text-danger" style="margin-left: auto; margin-right: auto;"><?php echo (empty($data['medicine'])) ? 'Jenis Terapi' : 'Obat' ; ?> Tidak Boleh Diganti Tanpa Izin Penulis Resep</h4>
                    </div>
                    <div class="col-md-12 text-right">
                      <h6 class="text-capitalize text-danger" style="margin-left: auto; margin-right: auto;">®<?php echo $user['doctor_name']; ?></h6>
                    </div>
                  </div>
                  <hr>
                </div>
              </div>
            </div>
          </div>
        </div><!-- end prescription-print -->
      </div> <!-- end container -->
    </div><!-- end page-content-wrapper -->
    
<?php } include_once('includes/footer_start.php'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/print2.js"></script>
<script type="text/javascript">

function print_prescription() {
  printJS({
    printable: 'prescription-print',
    type: 'html',
    targetStyles: ['*']
 })
}

// document.getElementById('printButton').addEventListener ("click", print)

$(function() {
  //print_prescription();
});

</script>

<!-- <script src="<?php echo base_url(); ?>assets/js/printThis.js"></script> -->
<?php include_once('includes/footer_end.php'); ?>