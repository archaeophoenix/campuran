<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once('includes/header_start.php');
?>

<?php 
include_once('includes/header_end.php');
$sys_title = $this->user_mo->get_user();
$id = $this->uri->segment(3);
$user = $this->user_mo->getuser();
$row_data = $this->user_mo->getinvoicebyid($id);
?>

<div class="wrapper">
  <div class="container">
    <!-- Page-Title -->
    <div class="row">
      <div class="col-sm-12">
        <div class="page-title-box">
          <div class="btn-group pull-right">
            <ol class="breadcrumb hide-phone p-0 m-0">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $sys_title[0]['title']; ?></a></li>
              <li class="breadcrumb-item active">Cetak Faktur</li>
            </ol>
          </div>
          <h4 class="page-title">Cetak Faktur</h4>
        </div>
      </div>
    </div><!-- end page title end breadcrumb -->
  </div> <!-- End Container -->
</div><!-- End Wrapper -->
<?php
  if(!$row_data) {
    echo "<center><h3>Faktur Tidak Ditemukan !</h3></center>";
  } else{
    $data = $row_data[0];
    $info = $this->user_mo->getp_name($data['patient_id']);
    $title = json_decode($data['invoice_title']);
    $amount = json_decode($data['invoice_amount']);
?>
    
<!-- ================== PAGE CONTENT START ================== -->
<div class="page-content-wrapper">
<div class="container">
<div class="row">
  <div class="col-12">
    <div class="m-b-20">                
      <a href="<?php echo base_url('user/billing'); ?>"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-arrow-left"></i>&nbsp; Kembali ke Daftar Faktur</button></a>                       
      <button type="button" class="btn btn-primary waves-effect waves-light" onclick="print_prescription()"><i class="fa fa-print"></i>&nbsp; Cetak Faktur</button>
    </div>
  </div>
</div>
<div class="prescription-print" id="prescription-print">
  <div class="row">
    <div class="col-12">
      <div class="card m-b-20">
        <div class="card-block">

          <div class="row">
            <div class="col-md-12 text-center"><img src="<?php echo base_url() . 'assets/images/' . $user['logo']; ?>"></div>
          </div>
          <div class="row">
            <div class="col-12">
              <!-- <div class="row">
                <div class="col-6">
                  <h3 class="text-danger"><?php echo $user['doctor_name'] ?></h3>
                  <address>
                  <p class="text-muted m-l-5">
                    Alamat :<br>
                    Email  :<?php echo $user['email'] ?> <br>
                    Telepon :<?php echo $user['mobile'] ?>
                  </p>
                  </address>
                </div>
                <div class="col-6 text-right">
                  <h4>Untuk Pasien:</h4>
                  <h5><?php echo $info['p_name']; ?></h5>
                  <p class="text-muted m-l-30"><strong>Alamat</strong> : <?php echo $info['add']; ?></p>
                  <p class="text-muted"><strong>Telepon</strong> : <?php echo $info['phone']; ?></p>
                  <p class="m-t-20"><b> Tanggal :</b> <i class="fa fa-calendar"></i>&nbsp; <?php echo date('d-m-Y'); ?></p>
                </div>
              </div> -->
            </div>
          </div>
            <div class="col-md-12 text-center">
              <h3 class="text-capitalize text-center"><u><?php echo $user['doctor_name']; ?></u></h3>
              <h5 class="text-capitalize "><?php echo $user['specialist']; ?><br><?php echo $user['sip']; ?></h5>
            </div>
            <hr>
            <div class="invoice-title text-center">
                <div class="row">
                  <div class="col-md-6"><h3 class="text-left">Invoice</h3></div>
                  <div class="col-md-6"><h4 class="text-right"><strong>No.Invoice: Inv/<?php echo date('m-Y', strtotime($data['invoice_date'])) .'/'. $data['invoice_id']; ?></strong></h4></div>
                </div>
              </div>
              <hr>
            <div class="row">
              <div class="col-md-8">
                 <div class="table-responsive">
                  <table style="width: 100%; border-spacing: 4px;">
                    <tbody>
                      <tr>
                        <td width="20%">Nama Pasien:</td>
                        <td width="80%"><?php echo $info['p_name']; ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Usia Pasien:</td>
                        <td width="80%"><?php echo $info['age']; ?></td>
                      </tr>
                    <?php if (TYPE == 'vetbiz') { ?>
                      <tr>
                        <td width="20%">Jenis Hewan:</td>
                        <td width="80%"><?php echo $info['type']; ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Pemilik:</td>
                        <td width="80%"><?php echo $info['owner']; ?></td>
                      </tr>
                    <?php } ?>
                      <tr>
                        <td width="20%">Telp.:</td>
                        <td width="80%"><?php echo $info['phone']; ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Alamat:</td>
                        <td width="80%"><?php echo $info['add']; ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="col-md-4 text-right">
                <p style="margin-left: 80%;" class="col-md-12 pull-right"><b> Tanggal :</b> <i class="fa fa-calendar"></i>&nbsp; <?php echo date("d-m-Y", strtotime($row_data[0]['invoice_date'])); ?></p>
              </div>
            </div>
            <!-- ALAMAT PRAKTEK :
            <?php if (!empty(json_decode($user['office'], true))){ ?>
            <div class="row">
              <?php foreach (json_decode($user['office'], true) as $key => $val){ ?>
                <?php if ($key < 3){ ?>
                <div class="col-md-4">
                  <address>
                    <p><?php echo $val['address']; ?></p>
                    <table style="width: 100%;">
                      <tr><td width="20%">Telp.:</td><td width="80%"><?php echo $user['mobile']; ?></td></tr>
                      <tr><td width="20%">Hari:</td><td width="80%"><?php echo $val['day']; ?></td></tr>
                      <tr><td width="20%">Jam:</td><td width="80%"><?php echo $val['hour']; ?></td></tr>
                    </table>
                  </address>
                </div>
                <?php } ?>
              <?php } ?>
            </div>
            <?php } ?> -->
            <br>
            <br>
            <br>
            <div class="row">
              <?php if (!empty($data['invoice_title'])){ ?>
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table" style="width: 100%; border-spacing: 4px;">
                    <thead>
                      <tr>
                        <th width="20%">No.</th>
                        <th width="60%">Deskripsi</th>
                        <th width="20%">Nilai (Rp.)</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $total = 0; for ( $i=0; $i<count($title); $i++) { $total += $amount[$i]; ?>
                      <tr>
                        <td width="20%"><?php echo ($i + 1); ?></td>
                        <td width="60%"><?php echo $title[$i]; ?></td>
                        <td width="20%"><?php echo number_format($amount[$i],0,'','.'); ?></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td></td>
                        <td class="text-right"><strong>TOTAL</strong></td>
                        <td class="text-left"><h4><?php echo number_format($total,0,'','.'); ?></h4></td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

          <!-- <div class="row">
            <div class="col-12">
              <div class="panel panel-default">
                <div class="">
                  <div class="table-responsive">
                    <table class="table">
                      <thead>
                        <tr>
                          <td><strong>No.</strong></td>
                          <td class="text-center"><strong>Deskripsi</strong></td>
                          <td class="text-center"><strong>Nilai (Rp.)</strong></td>
                        </tr>
                      </thead>
                      <tbody>
                          <?php
                            $total = 0; 
                            for ( $i=0; $i<count($title); $i++) : 
                          ?>

                        <tr>
                          <td><?php echo $i+1; ?></td>
                          <td class="text-left"><?php echo $title[$i]; ?></td>
                          <td class="text-right">
                            <?php 
                              echo $amount[$i];
                              $total += $amount[$i]
                            ?>

                          </td>
                        </tr>
                          <?php endfor;?>

                        <tr>
                          <td class="no-line"></td>
                          <td class="no-line text-right">
                            <strong>Total</strong></td>
                          <td class="no-line text-center"><h4 class="m-0"><?php echo $total; ?></h4></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div> --> <!-- end row -->
          <div class="row">
            <div class="col-md-12 text-right">
              <div class="col-md-12 text-center">
              <h4 class="text-capitalize text-danger" style="margin-left: auto; margin-right: auto;">Terima Kasih Atas Kepercayaannya</h4>
            </div>
              <!-- <div class="col-md-12 text-right">
                <h6 class="text-capitalize text-danger" style="margin-left: auto; margin-right: auto;">®<?php echo $user['doctor_name']; ?></h6>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div> <!-- end col -->
  </div> <!-- end row -->
</div><!-- end div-print -->
</div> <!-- end container -->
</div><!-- end page-content-wrapper -->
<?php } include_once('includes/footer_start.php'); ?>

<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/print.js"></script>
<script src="<?php echo base_url(); ?>assets/js/printThis.js"></script> -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/print2.js"></script>
<script type="text/javascript">

function print_prescription() {
  printJS({
    printable: 'prescription-print',
    type: 'html',
    targetStyles: ['*']
 })
}
</script>

<?php include_once('includes/footer_end.php'); ?>