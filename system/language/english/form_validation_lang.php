<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'Kolom {field} diperlukan.';
$lang['form_validation_isset']			= 'Kolom {field} harus diisi.';
$lang['form_validation_valid_email']		= 'Kolom {field} harus diisi alamat email yang valid.';
$lang['form_validation_valid_emails']		= 'Kolom {field} harus berisi alamat email yang  valid.';
$lang['form_validation_valid_url']		= 'Kolom harus berisi {field} URL yang valid.';
$lang['form_validation_valid_ip']		= 'Kolom {field} harus berisi IP yang valid.';
$lang['form_validation_min_length']		= 'Kolom {field} harus minimal {param} panjang  karakter.';
$lang['form_validation_max_length']		= 'Kolom {field} tidak boleh lebih dari {param} panjang karakter.';
$lang['form_validation_exact_length']		= 'Kolom {field} harus tepat {param} panjang karakter.';
$lang['form_validation_alpha']			= 'Kolom {field} hanya boleh mengandung karakter alfabet.';
$lang['form_validation_alpha_numeric']		= 'Kolom {field} hanya boleh mengandung karakter alfa-numerik.';
$lang['form_validation_alpha_numeric_spaces']	= 'Kolom {field} hanya boleh mengandung karakter alaa-numerik dan spasi.';
$lang['form_validation_alpha_dash']		= ' Kolom {field} hanya  boleh mengandung karakter alfa-numerik, garis bawah, dan strip.';
$lang['form_validation_numeric']		= 'Kolom  {field} harus mengandung hanya angka.';
$lang['form_validation_is_numeric']		= 'Kolom {field} harus mengandung hanya karakter numerik.';
$lang['form_validation_integer']		= 'Kolom {field} harus mengandung sebuah integer.';
$lang['form_validation_regex_match']		= 'Kolom {field} tidak benar formatnya.';
$lang['form_validation_matches']		= 'Kolom {field} tidak sesuai dengan {param} kolom.';
$lang['form_validation_differs']		= 'Kolom {field} harus berbeda dari {param} kolom.';
$lang['form_validation_is_unique'] 		= 'Kolom {field} harus mengandung nilai unik.';
$lang['form_validation_is_natural']		= 'Kolom {field} harus mengandung angka.';
$lang['form_validation_is_natural_no_zero']	= 'Kolom {field} harus mengandung angka dan lebih besar dari nol.';
$lang['form_validation_decimal']		= 'Kolom {field} harus mengandung angka desimal.';
$lang['form_validation_less_than']		= 'Kolom {field} harus mengandung angka kurang dari {param}.';
$lang['form_validation_less_than_equal_to']	= 'Kolom {field} harus mengandung angka kurang dari atau sama dengan {param}.';
$lang['form_validation_greater_than']		= 'Kolom {field} harus mengandung angka lebih besar dari {param}.';
$lang['form_validation_greater_than_equal_to']	= 'Kolom {field} harus mengandung angka lebih besar dari atau sama dengan {param}.';
$lang['form_validation_error_message_not_set']	= 'Tidak bisa akses ke pesan korespondensi salah ke nama kolom {field}.';
$lang['form_validation_in_list']		= 'Kolom {field} harus salah satu dari: {param}.';
